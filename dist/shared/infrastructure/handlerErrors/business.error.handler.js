"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessErrorHandler = void 0;
var stackParser_handler_1 = require("./stackParser.handler");
var BusinessErrorHandler = /** @class */ (function () {
    function BusinessErrorHandler() {
        this.stackParser = new stackParser_handler_1.StackParser();
    }
    BusinessErrorHandler.prototype.handle = function (e) {
        return {
            testing: "Todo pending"
        };
    };
    return BusinessErrorHandler;
}());
exports.BusinessErrorHandler = BusinessErrorHandler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVzaW5lc3MuZXJyb3IuaGFuZGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9zaGFyZWQvaW5mcmFzdHJ1Y3R1cmUvaGFuZGxlckVycm9ycy9idXNpbmVzcy5lcnJvci5oYW5kbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDZEQUFtRDtBQUVuRDtJQUFBO1FBQ2MsZ0JBQVcsR0FBRyxJQUFJLGlDQUFXLEVBQUUsQ0FBQTtJQU83QyxDQUFDO0lBTFUscUNBQU0sR0FBYixVQUFjLENBQUM7UUFDWCxPQUFPO1lBQ0gsT0FBTyxFQUFDLGNBQWM7U0FDekIsQ0FBQTtJQUNMLENBQUM7SUFDTCwyQkFBQztBQUFELENBQUMsQUFSRCxJQVFDO0FBUlksb0RBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU3RhY2tQYXJzZXIgfSBmcm9tIFwiLi9zdGFja1BhcnNlci5oYW5kbGVyXCIgXHJcblxyXG5leHBvcnQgY2xhc3MgQnVzaW5lc3NFcnJvckhhbmRsZXIge1xyXG4gICAgcHJvdGVjdGVkIHN0YWNrUGFyc2VyID0gbmV3IFN0YWNrUGFyc2VyKClcclxuXHJcbiAgICBwdWJsaWMgaGFuZGxlKGUpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICB0ZXN0aW5nOlwiVG9kbyBwZW5kaW5nXCJcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iXX0=