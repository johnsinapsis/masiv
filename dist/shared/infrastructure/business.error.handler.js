"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessErrorHandler = void 0;
var stackParser_handler_1 = require("./stackParser.handler");
var BusinessErrorHandler = /** @class */ (function () {
    function BusinessErrorHandler() {
        this.stackParser = new stackParser_handler_1.StackParser();
    }
    BusinessErrorHandler.prototype.handle = function (e) {
        return {
            testing: "Todo pending"
        };
    };
    return BusinessErrorHandler;
}());
exports.BusinessErrorHandler = BusinessErrorHandler;
