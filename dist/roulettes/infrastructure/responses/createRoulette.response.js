"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateRouletteResponse = void 0;
var common_response_1 = require("../../../shared/infrastructure/common.response");
var CreateRouletteResponse = /** @class */ (function (_super) {
    __extends(CreateRouletteResponse, _super);
    function CreateRouletteResponse() {
        var _this = _super.call(this) || this;
        _this.type = "CreateRouletteResponse";
        _this.response.description = "Se ha Creado la ruleta correctamente";
        return _this;
    }
    return CreateRouletteResponse;
}(common_response_1.CommonResponse));
exports.CreateRouletteResponse = CreateRouletteResponse;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlUm91bGV0dGUucmVzcG9uc2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvcm91bGV0dGVzL2luZnJhc3RydWN0dXJlL3Jlc3BvbnNlcy9jcmVhdGVSb3VsZXR0ZS5yZXNwb25zZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxrRkFBZ0Y7QUFFaEY7SUFBNEMsMENBQWM7SUFDdEQ7UUFBQSxZQUNJLGlCQUFPLFNBR1Y7UUFGRyxLQUFJLENBQUMsSUFBSSxHQUFHLHdCQUF3QixDQUFBO1FBQ3BDLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLHNDQUFzQyxDQUFBOztJQUN0RSxDQUFDO0lBQ0wsNkJBQUM7QUFBRCxDQUFDLEFBTkQsQ0FBNEMsZ0NBQWMsR0FNekQ7QUFOWSx3REFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tb25SZXNwb25zZSB9IGZyb20gXCIuLi8uLi8uLi9zaGFyZWQvaW5mcmFzdHJ1Y3R1cmUvY29tbW9uLnJlc3BvbnNlXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgQ3JlYXRlUm91bGV0dGVSZXNwb25zZSBleHRlbmRzIENvbW1vblJlc3BvbnNle1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy50eXBlID0gXCJDcmVhdGVSb3VsZXR0ZVJlc3BvbnNlXCJcclxuICAgICAgICB0aGlzLnJlc3BvbnNlLmRlc2NyaXB0aW9uID0gXCJTZSBoYSBDcmVhZG8gbGEgcnVsZXRhIGNvcnJlY3RhbWVudGVcIlxyXG4gICAgfVxyXG59Il19