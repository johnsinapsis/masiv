import { BetDto } from "../../application/dto/bet.dto";
import { CommonResponse } from "../../../shared/infrastructure/common.response";
import { BetResponse } from "../responses/bet.response";
import { ResponseMapper } from "../../../shared/application/mappers/response.mapper";
import { DtoMapper } from "../../../shared/application/mappers/dto.mapper";
import { DateUtil } from "../../../helpers/date.util";

export class BetRouletteMapper implements ResponseMapper,DtoMapper{
    toDto(req: any): BetDto {
        let dto = new BetDto() 
        dto.setRouletteId(req.rouletteId)
        dto.setUserId(req.userId)
        let date = DateUtil.formatDate(new Date())
        dto.setDate(date)
        dto.setMount(req.mount)
        req.number ? dto.setNumber(req.number) : dto.setColor(req.color) 
        
        return dto
    }
    
    toResponse(dto: BetDto): CommonResponse {
        let response = new BetResponse()
        response.response["bet"] = dto

        return response
    }
}