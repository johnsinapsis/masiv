"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatusRoulettePersistence = void 0;
var base_persistence_1 = require("../../../shared/infrastructure/persistences/base.persistence");
var roulette_model_1 = require("./models/roulette.model");
var StatusRoulettePersistence = /** @class */ (function (_super) {
    __extends(StatusRoulettePersistence, _super);
    function StatusRoulettePersistence() {
        var _this = _super.call(this) || this;
        _this.model = new roulette_model_1.RouletteModel();
        return _this;
    }
    StatusRoulettePersistence.prototype.setOpenStatusRoulette = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _id, updateDocument, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _id = this.getObjectId(id);
                        if (!_id)
                            return [2 /*return*/, null];
                        return [4 /*yield*/, this.setDBAndCollection()];
                    case 1:
                        _a.sent();
                        updateDocument = { $set: { status: "Open" } };
                        return [4 /*yield*/, this.collection.updateOne({ _id: _id }, updateDocument)];
                    case 2:
                        result = _a.sent();
                        this.driver.disconnection();
                        if (!result)
                            return [2 /*return*/, false];
                        return [2 /*return*/, true];
                }
            });
        });
    };
    StatusRoulettePersistence.prototype.setCloseStatusRoulette = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _id, updateDocument, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _id = this.getObjectId(id);
                        if (!_id)
                            return [2 /*return*/, null];
                        return [4 /*yield*/, this.setDBAndCollection()];
                    case 1:
                        _a.sent();
                        updateDocument = { $set: { status: "Close" } };
                        return [4 /*yield*/, this.collection.updateOne({ _id: _id }, updateDocument)];
                    case 2:
                        result = _a.sent();
                        this.driver.disconnection();
                        if (!result)
                            return [2 /*return*/, false];
                        return [2 /*return*/, true];
                }
            });
        });
    };
    return StatusRoulettePersistence;
}(base_persistence_1.Persistence));
exports.StatusRoulettePersistence = StatusRoulettePersistence;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdHVzLnJvdWxldHRlLnBlcnNpc3RlbmNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL3JvdWxldHRlcy9pbmZyYXN0cnVjdHVyZS9wZXJzaXN0ZW5jZXMvc3RhdHVzLnJvdWxldHRlLnBlcnNpc3RlbmNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLGlHQUEyRjtBQUMzRiwwREFBd0Q7QUFFeEQ7SUFBK0MsNkNBQVc7SUFDdEQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFERyxLQUFJLENBQUMsS0FBSyxHQUFHLElBQUksOEJBQWEsRUFBRSxDQUFBOztJQUNwQyxDQUFDO0lBRUsseURBQXFCLEdBQTNCLFVBQTRCLEVBQVU7Ozs7Ozt3QkFDOUIsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUE7d0JBQzlCLElBQUcsQ0FBQyxHQUFHOzRCQUNILHNCQUFPLElBQUksRUFBQTt3QkFDZixxQkFBTSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsRUFBQTs7d0JBQS9CLFNBQStCLENBQUE7d0JBQzNCLGNBQWMsR0FBRyxFQUFDLElBQUksRUFBQyxFQUFDLE1BQU0sRUFBQyxNQUFNLEVBQUMsRUFBQyxDQUFBO3dCQUM5QixxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFDLEdBQUcsS0FBQSxFQUFDLEVBQUMsY0FBYyxDQUFDLEVBQUE7O3dCQUE5RCxNQUFNLEdBQUcsU0FBcUQ7d0JBQ2xFLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUE7d0JBQzNCLElBQUcsQ0FBQyxNQUFNOzRCQUNOLHNCQUFPLEtBQUssRUFBQTt3QkFFaEIsc0JBQU8sSUFBSSxFQUFBOzs7O0tBQ2Q7SUFFSywwREFBc0IsR0FBNUIsVUFBNkIsRUFBVTs7Ozs7O3dCQUMvQixHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQTt3QkFDOUIsSUFBRyxDQUFDLEdBQUc7NEJBQ0gsc0JBQU8sSUFBSSxFQUFBO3dCQUNmLHFCQUFNLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxFQUFBOzt3QkFBL0IsU0FBK0IsQ0FBQTt3QkFDM0IsY0FBYyxHQUFHLEVBQUMsSUFBSSxFQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxFQUFDLENBQUE7d0JBQy9CLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUMsR0FBRyxLQUFBLEVBQUMsRUFBQyxjQUFjLENBQUMsRUFBQTs7d0JBQTlELE1BQU0sR0FBRyxTQUFxRDt3QkFDbEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQTt3QkFDM0IsSUFBRyxDQUFDLE1BQU07NEJBQ04sc0JBQU8sS0FBSyxFQUFBO3dCQUVoQixzQkFBTyxJQUFJLEVBQUE7Ozs7S0FDZDtJQUNMLGdDQUFDO0FBQUQsQ0FBQyxBQWpDRCxDQUErQyw4QkFBVyxHQWlDekQ7QUFqQ1ksOERBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91bGV0dGVEdG8gfSBmcm9tIFwiLi4vLi4vYXBwbGljYXRpb24vZHRvL3JvdWxldHRlLmR0b1wiO1xyXG5pbXBvcnQgeyBTdGF0dXNSb3VsZXR0ZVJlcG9zaXRvcnkgfSBmcm9tIFwiLi4vLi4vYXBwbGljYXRpb24vcmVwb3NpdG9yaWVzL3N0YXR1cy5yb3VsZXR0ZS5yZXBvc2l0b3J5XCI7XHJcbmltcG9ydCB7IFBlcnNpc3RlbmNlIH0gZnJvbSBcIi4uLy4uLy4uL3NoYXJlZC9pbmZyYXN0cnVjdHVyZS9wZXJzaXN0ZW5jZXMvYmFzZS5wZXJzaXN0ZW5jZVwiO1xyXG5pbXBvcnQgeyBSb3VsZXR0ZU1vZGVsIH0gZnJvbSBcIi4vbW9kZWxzL3JvdWxldHRlLm1vZGVsXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgU3RhdHVzUm91bGV0dGVQZXJzaXN0ZW5jZSBleHRlbmRzIFBlcnNpc3RlbmNlIGltcGxlbWVudHMgU3RhdHVzUm91bGV0dGVSZXBvc2l0b3J5e1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy5tb2RlbCA9IG5ldyBSb3VsZXR0ZU1vZGVsKClcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBzZXRPcGVuU3RhdHVzUm91bGV0dGUoaWQ6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj4ge1xyXG4gICAgICAgIGxldCBfaWQgPSB0aGlzLmdldE9iamVjdElkKGlkKVxyXG4gICAgICAgIGlmKCFfaWQpXHJcbiAgICAgICAgICAgIHJldHVybiBudWxsXHJcbiAgICAgICAgYXdhaXQgdGhpcy5zZXREQkFuZENvbGxlY3Rpb24oKVxyXG4gICAgICAgIGxldCB1cGRhdGVEb2N1bWVudCA9IHskc2V0OntzdGF0dXM6XCJPcGVuXCJ9fVxyXG4gICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCB0aGlzLmNvbGxlY3Rpb24udXBkYXRlT25lKHtfaWR9LHVwZGF0ZURvY3VtZW50KVxyXG4gICAgICAgIHRoaXMuZHJpdmVyLmRpc2Nvbm5lY3Rpb24oKVxyXG4gICAgICAgIGlmKCFyZXN1bHQpXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZVxyXG5cclxuICAgICAgICByZXR1cm4gdHJ1ZVxyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIHNldENsb3NlU3RhdHVzUm91bGV0dGUoaWQ6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj4ge1xyXG4gICAgICAgIGxldCBfaWQgPSB0aGlzLmdldE9iamVjdElkKGlkKVxyXG4gICAgICAgIGlmKCFfaWQpXHJcbiAgICAgICAgICAgIHJldHVybiBudWxsXHJcbiAgICAgICAgYXdhaXQgdGhpcy5zZXREQkFuZENvbGxlY3Rpb24oKVxyXG4gICAgICAgIGxldCB1cGRhdGVEb2N1bWVudCA9IHskc2V0OntzdGF0dXM6XCJDbG9zZVwifX1cclxuICAgICAgICBsZXQgcmVzdWx0ID0gYXdhaXQgdGhpcy5jb2xsZWN0aW9uLnVwZGF0ZU9uZSh7X2lkfSx1cGRhdGVEb2N1bWVudClcclxuICAgICAgICB0aGlzLmRyaXZlci5kaXNjb25uZWN0aW9uKClcclxuICAgICAgICBpZighcmVzdWx0KVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2VcclxuXHJcbiAgICAgICAgcmV0dXJuIHRydWVcclxuICAgIH1cclxufSJdfQ==