import { Dto } from "../../../shared/application/base.dto";
import { StatusRoulette } from "../../domain/StatusRoulette";

export class RouletteDto extends Dto{
    private date:string
    private status:StatusRoulette

    constructor(){
        super()
    }

    public getDate(): string {
        return this.date;
    }

    public setDate(date: string): void {
        this.date = date;
    }

    public getStatus(): StatusRoulette {
        return this.status;
    }

    public setStatus(status: StatusRoulette): void {
        this.status = status;
    }
}