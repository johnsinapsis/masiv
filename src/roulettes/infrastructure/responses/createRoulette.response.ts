import { CommonResponse } from "../../../shared/infrastructure/common.response";

export class CreateRouletteResponse extends CommonResponse{
    constructor(){
        super()
        this.type = "CreateRouletteResponse"
        this.response.description = "Se ha Creado la ruleta correctamente"
    }
}