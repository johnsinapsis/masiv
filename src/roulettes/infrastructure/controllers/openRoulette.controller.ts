import { Controller } from "../../../shared/infrastructure/controllers/controller.base";
import { logger } from "../../../helpers/default.logger";
import { BaseErrorHandler } from "../../../shared/infrastructure/handlerErrors/base.error.handler";
import { ConfigControllerIdentity } from "../../../shared/infrastructure/controllers/config.controller.identity";
import { RouletteDto } from "../../application/dto/roulette.dto";
import { StatusRoulettePersistence } from "../persistences/status.roulette.persistence";
import { FieldsOpenRouletteValidation } from "../../application/validations/fieldsOpenRoulette.validation";
import { ErrorOpenRoulette } from "../handler/error.openRoulette";
import { OpenRouletteRules } from "../rules/openRoulette.rules";
import { OpenRouletteMapper } from "../mappers/openRoulette.mapper";

export class OpenRouletteController extends Controller implements ConfigControllerIdentity{
    private validateFields
    private rules
    private mapper = new OpenRouletteMapper()
    private service = new StatusRoulettePersistence()

    constructor(){
        super()
        this.validateFields = new FieldsOpenRouletteValidation()
        this.rules = new OpenRouletteRules()
    }

    getConfigId(){
        return 'openRoulette'
    }

    async putOpenRoulette(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                await this.rules.exec(req.body.id)
                let update = await this.service.setOpenStatusRoulette(req.body.id)
                if(!update)
                    return BaseErrorHandler.handle(res,"Error inesperado al abrir la ruleta")
                let response = this.mapper.toResponse(new RouletteDto())

                return res.json(response)
            }
            else{
                let error = new ErrorOpenRoulette()

                return res.json(error.buildResponse(res))
            }
        }
        catch(e){
            logger.error(e)
            
            return BaseErrorHandler.handle(res,e)
        }
    }
}