import { BusinessErrorHandler } from "./business.error.handler"
import { StackParser } from "./stackParser.handler"
import { BusinessError } from "../../application/handler/business.error"

export class BaseErrorHandler {
    static beh = new BusinessErrorHandler()

    static handle(res, e) {
        if (e instanceof BusinessError) {
            let properties = this.parseStack(e.response)
            this.clear(e)
            return res.status(200).json(e)
        }
        else{
            return res.status(400).json({error:"Excepción no controlada del sistema"})
        }
    }

    static parseStack(e) {
        let parser = new StackParser(e.stack)
        let line = parser.getTopLine()
        let file = parser.getTopFile()
        return {line,file}
    }

    static clear(e){
        delete e.code
        delete e.response.stack
    }
}
