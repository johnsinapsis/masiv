"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FieldValidator = void 0;
var constants_1 = require("../../../config/constants");
var FieldValidator = /** @class */ (function () {
    function FieldValidator(jpExpr, failMsg) {
        this.jpExpr = jpExpr;
        this.failMsg = failMsg;
    }
    FieldValidator.prototype.evaluate = function (req, res) {
        this.msg = this.failMsg;
        var isValid = this.isValid(req, res);
        if (!isValid)
            this.addErrorResponse(res, this.msg);
        return isValid;
    };
    FieldValidator.prototype.isValid = function (req, res) {
        return true;
    };
    FieldValidator.prototype.addErrorResponse = function (res, msg) {
        if (typeof res[constants_1.INV_FIELDS] === constants_1.UNDEF) {
            res.invalid_fields = [];
        }
        res.invalid_fields.push(msg);
    };
    return FieldValidator;
}());
exports.FieldValidator = FieldValidator;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQudmFsaWRhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9oZWxwZXJzL3ZhbGlkYXRpb25zL2ZpZWxkL2ZpZWxkLnZhbGlkYXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsdURBQTREO0FBRTVEO0lBS0ksd0JBQVksTUFBTSxFQUFFLE9BQU87UUFDdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUE7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUE7SUFDMUIsQ0FBQztJQUVELGlDQUFRLEdBQVIsVUFBUyxHQUFHLEVBQUUsR0FBRztRQUNiLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQTtRQUN2QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQTtRQUNwQyxJQUFHLENBQUMsT0FBTztZQUNQLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ3hDLE9BQU8sT0FBTyxDQUFBO0lBQ2xCLENBQUM7SUFFRCxnQ0FBTyxHQUFQLFVBQVEsR0FBRyxFQUFFLEdBQUc7UUFDWixPQUFPLElBQUksQ0FBQTtJQUNmLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEIsVUFBa0IsR0FBRyxFQUFHLEdBQUc7UUFDdkIsSUFBSSxPQUFPLEdBQUcsQ0FBQyxzQkFBVSxDQUFDLEtBQUssaUJBQUssRUFBRTtZQUNsQyxHQUFHLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQTtTQUMxQjtRQUNELEdBQUcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO0lBQ2hDLENBQUM7SUFDTCxxQkFBQztBQUFELENBQUMsQUE1QkQsSUE0QkM7QUE1Qlksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJTlZfRklFTERTLFVOREVGIH0gZnJvbSBcIi4uLy4uLy4uL2NvbmZpZy9jb25zdGFudHNcIiBcclxuXHJcbmV4cG9ydCBjbGFzcyBGaWVsZFZhbGlkYXRvcntcclxuICAgIHB1YmxpYyBqcEV4cHJcclxuICAgIHB1YmxpYyBmYWlsTXNnXHJcbiAgICBwdWJsaWMgbXNnXHJcblxyXG4gICAgY29uc3RydWN0b3IoanBFeHByLCBmYWlsTXNnKXtcclxuICAgICAgICB0aGlzLmpwRXhwciA9IGpwRXhwclxyXG4gICAgICAgIHRoaXMuZmFpbE1zZyA9IGZhaWxNc2dcclxuICAgIH1cclxuXHJcbiAgICBldmFsdWF0ZShyZXEsIHJlcyl7XHJcbiAgICAgICAgdGhpcy5tc2cgPSB0aGlzLmZhaWxNc2dcclxuICAgICAgICBsZXQgaXNWYWxpZCA9IHRoaXMuaXNWYWxpZChyZXEsIHJlcylcclxuICAgICAgICBpZighaXNWYWxpZClcclxuICAgICAgICAgICAgdGhpcy5hZGRFcnJvclJlc3BvbnNlKHJlcywgdGhpcy5tc2cpXHJcbiAgICAgICAgcmV0dXJuIGlzVmFsaWRcclxuICAgIH1cclxuXHJcbiAgICBpc1ZhbGlkKHJlcSwgcmVzKXtcclxuICAgICAgICByZXR1cm4gdHJ1ZVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBhZGRFcnJvclJlc3BvbnNlIChyZXMgLCBtc2cpe1xyXG4gICAgICAgIGlmKCB0eXBlb2YgcmVzW0lOVl9GSUVMRFNdID09PSBVTkRFRiApe1xyXG4gICAgICAgICAgICByZXMuaW52YWxpZF9maWVsZHMgPSBbXVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXMuaW52YWxpZF9maWVsZHMucHVzaChtc2cpXHJcbiAgICB9XHJcbn0gXHJcbiJdfQ==