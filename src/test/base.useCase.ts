import config from "../config/config"
import { ConfigReader } from "../config/ConfigReader"

export class BaseUseCase{
    protected cr 
    protected url
    constructor(){
        this.cr = ConfigReader
        this.url = 'http://localhost:'+config.port
    }

    protected getRandomInt(min:number,max:number){
        min = Math.ceil(min);
        max = Math.ceil(max);
        return Math.floor(Math.random() * (max - min) + min)
    }
}

