import { ObjectId } from "bson";
import { MongoDriver } from "../drivers/mongo.driver";
import { Utils } from "../../../helpers/utilities";

export class Persistence{
    protected driver
    protected collection
    protected model
    
    constructor(){
        this.driver = new MongoDriver()
    }

    protected getObjectId(id){
        if(!Utils.isBson(id))
            return null
        return new ObjectId(id)
    }

    async setDBAndCollection(){
        let db = await this.driver.connection()
        this.collection =  db.collection(this.model.name)
    }
}