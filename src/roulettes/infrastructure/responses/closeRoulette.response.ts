import { CommonResponse } from "../../../shared/infrastructure/common.response";

export class CloseRouletteResponse extends CommonResponse{
    constructor(){
        super()
        this.type = "CloseRouletteResponse"
        this.response.description = "Cierre de apuestas realizado correctamente"
    }
}