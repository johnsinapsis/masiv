import { EmptyFieldValidation } from "../../../helpers/validations/field/empty.field.validation" 
import { NumberFieldValidation } from "../../../helpers/validations/field/number.field.validation" 
import { ColorFieldValidation } from "../../../helpers/validations/field/color.field.validation"
import { MountFieldValidator } from "../../../helpers/validations/field/mount.field.validator"
import { RangeNumberFieldValidation } from "../../../helpers/validations/field/rangeNumber.field.validation"
import { BetTypeValidation } from "../../../helpers/validations/bet.type.validation"

export const FIELD_VALIDATORS = {
    "empty": EmptyFieldValidation,    
    "number": NumberFieldValidation,
    "color": ColorFieldValidation,
    "mount": MountFieldValidator,
    "number-bet": RangeNumberFieldValidation,
    "bet-type": BetTypeValidation
}

export class FieldValidationFactory{
    static createInstance(name, jpExpr, failMsg) {
        const fConstructor = FIELD_VALIDATORS[name]
        return fConstructor ? new fConstructor(jpExpr, failMsg) : null
    }

}