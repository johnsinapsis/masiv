import { RulesBussiness } from "../../../../shared/application/business/rules.bussiness";
import { OpenRouletteBusinessRule } from "../../../application/business.rules/openRoulette.business.rule";
import { Condition } from "../../../../shared/infrastructure/rules/base.condition";
import { Roulette } from "../../../domain/Roulette";
import { RoulettePersistence } from "../../persistences/roulette.persistence";

export class OpenRouletteCondition extends Condition implements RulesBussiness, OpenRouletteBusinessRule {
    private dao
    private error = "La ruleta ya se encuentra abierta"

    constructor(error?){
        super()
        this.type = "OpenRouletteCondition"
        this.dao = new RoulettePersistence()
        if(error)
            this.error = error
    }

    async isOpenRoulette(id:string): Promise<boolean> {
        let roulette:Roulette = await this.dao.getRouletteById(id)
        if(roulette.status==="Open")
            return true

        return false
    }
    
    public async exec(id: string) {
        if(await this.isOpenRoulette(id))
            this.throwBusiness(this.error)
    }
}