"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmptyFieldValidation = void 0;
var field_validation_1 = require("./field.validation");
var utilities_1 = require("../../utilities");
var jsonpath_1 = __importDefault(require("jsonpath"));
var constants_1 = require("../../../config/constants");
var EmptyFieldValidation = /** @class */ (function (_super) {
    __extends(EmptyFieldValidation, _super);
    function EmptyFieldValidation(jpExpr, failMsg) {
        var _this = _super.call(this, jpExpr, failMsg) || this;
        _this.jpExpr = jpExpr;
        _this.failMsg = failMsg;
        return _this;
    }
    EmptyFieldValidation.prototype.isValid = function (req, res) {
        var field = jsonpath_1.default.query(req.body, this.jpExpr)[0];
        return !utilities_1.Utils.isEmpty(field);
    };
    EmptyFieldValidation.prototype.addErrorResponse = function (res, msg) {
        if (typeof res[constants_1.INV_FIELDS] === constants_1.UNDEF) {
            res.invalid_fields = [];
        }
        res.invalid_fields.push(msg);
    };
    return EmptyFieldValidation;
}(field_validation_1.FieldValidator));
exports.EmptyFieldValidation = EmptyFieldValidation;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW1wdHkuZmllbGQudmFsaWRhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9oZWxwZXJzL3ZhbGlkYXRpb25zL2ZpZWxkL2VtcHR5LmZpZWxkLnZhbGlkYXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsdURBQW1EO0FBQ25ELDZDQUF1QztBQUN2QyxzREFBeUI7QUFDekIsdURBQTZEO0FBRTdEO0lBQTBDLHdDQUFjO0lBQ3BELDhCQUFZLE1BQU0sRUFBRSxPQUFPO1FBQTNCLFlBQ0ksa0JBQU0sTUFBTSxFQUFFLE9BQU8sQ0FBQyxTQUd6QjtRQUZHLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO1FBQ3BCLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFBOztJQUMxQixDQUFDO0lBRUQsc0NBQU8sR0FBUCxVQUFRLEdBQUcsRUFBRSxHQUFHO1FBQ1osSUFBSSxLQUFLLEdBQUcsa0JBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDOUMsT0FBTyxDQUFDLGlCQUFLLENBQUMsT0FBTyxDQUFFLEtBQUssQ0FBRSxDQUFBO0lBQ2xDLENBQUM7SUFFRCwrQ0FBZ0IsR0FBaEIsVUFBa0IsR0FBRyxFQUFFLEdBQUc7UUFDdEIsSUFBSSxPQUFPLEdBQUcsQ0FBQyxzQkFBVSxDQUFDLEtBQUssaUJBQUssRUFBRTtZQUNsQyxHQUFHLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQTtTQUMxQjtRQUNELEdBQUcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO0lBQ2hDLENBQUM7SUFDTCwyQkFBQztBQUFELENBQUMsQUFsQkQsQ0FBMEMsaUNBQWMsR0FrQnZEO0FBbEJZLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZpZWxkVmFsaWRhdG9yIH0gZnJvbSAnLi9maWVsZC52YWxpZGF0aW9uJyBcclxuaW1wb3J0IHsgVXRpbHMgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMnIFxyXG5pbXBvcnQganAgZnJvbSAnanNvbnBhdGgnXHJcbmltcG9ydCB7IElOVl9GSUVMRFMsIFVOREVGIH0gZnJvbSAnLi4vLi4vLi4vY29uZmlnL2NvbnN0YW50cydcclxuXHJcbmV4cG9ydCBjbGFzcyBFbXB0eUZpZWxkVmFsaWRhdGlvbiBleHRlbmRzIEZpZWxkVmFsaWRhdG9yIHtcclxuICAgIGNvbnN0cnVjdG9yKGpwRXhwciwgZmFpbE1zZyl7XHJcbiAgICAgICAgc3VwZXIoanBFeHByLCBmYWlsTXNnKVxyXG4gICAgICAgIHRoaXMuanBFeHByID0ganBFeHByXHJcbiAgICAgICAgdGhpcy5mYWlsTXNnID0gZmFpbE1zZ1xyXG4gICAgfVxyXG5cclxuICAgIGlzVmFsaWQocmVxLCByZXMpe1xyXG4gICAgICAgIGxldCBmaWVsZCA9IGpwLnF1ZXJ5KHJlcS5ib2R5LCB0aGlzLmpwRXhwcilbMF1cclxuICAgICAgICByZXR1cm4gIVV0aWxzLmlzRW1wdHkoIGZpZWxkIClcclxuICAgIH1cclxuXHJcbiAgICBhZGRFcnJvclJlc3BvbnNlKCByZXMsIG1zZyApeyAgICAgICAgXHJcbiAgICAgICAgaWYoIHR5cGVvZiByZXNbSU5WX0ZJRUxEU10gPT09IFVOREVGICl7XHJcbiAgICAgICAgICAgIHJlcy5pbnZhbGlkX2ZpZWxkcyA9IFtdXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJlcy5pbnZhbGlkX2ZpZWxkcy5wdXNoKG1zZylcclxuICAgIH1cclxufSJdfQ==