"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RangeDateFieldsValidation = void 0;
var field_validation_1 = require("../field/field.validation");
var utilities_1 = require("../../shared/application/utilities");
var jsonpath_1 = __importDefault(require("jsonpath"));
var constants_1 = require("../../config/constants");
/**
 * Valida el rango de fechas teniendo en cuenta que los campos son dateStart y dateEnd
 */
var RangeDateFieldsValidation = /** @class */ (function (_super) {
    __extends(RangeDateFieldsValidation, _super);
    function RangeDateFieldsValidation(jpExpr, failMsg) {
        var _this = _super.call(this, jpExpr, failMsg) || this;
        _this.jpExpr = jpExpr;
        _this.failMsg = failMsg;
        return _this;
    }
    RangeDateFieldsValidation.prototype.isValid = function (req, res) {
        var dateStart = jsonpath_1.default.query(req.body, this.jpExpr)[0].dateStart;
        var dateEnd = jsonpath_1.default.query(req.body, this.jpExpr)[0].dateEnd;
        return utilities_1.Utils.isSuccessRange(dateStart, dateEnd);
        //return true
    };
    RangeDateFieldsValidation.prototype.addErrorResponse = function (res, msg) {
        if (typeof res[constants_1.INV_FIELDS] === constants_1.UNDEF) {
            res.invalid_fields = [];
        }
        res.invalid_fields.push(msg);
    };
    return RangeDateFieldsValidation;
}(field_validation_1.FieldValidator));
exports.RangeDateFieldsValidation = RangeDateFieldsValidation;
