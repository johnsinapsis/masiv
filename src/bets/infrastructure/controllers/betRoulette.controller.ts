import { Controller } from "../../../shared/infrastructure/controllers/controller.base";
import { logger } from "../../../helpers/default.logger";
import { BaseErrorHandler } from "../../../shared/infrastructure/handlerErrors/base.error.handler";
import { ConfigControllerIdentity } from "../../../shared/infrastructure/controllers/config.controller.identity";
import { Authentication } from "../../../shared/infrastructure/auhentication";
import { BetRouletteMapper } from "../mappers/betRoulette.mapper";
import { BetDto } from "../../application/dto/bet.dto";
import { FieldsBetRouletteValidation } from "../../application/validations/fieldsBetRoulette.validation";
import { ErrorBetRoulette } from "../handler/error.betRoulette";
import { BetRouletteRules } from "../rules/betRoulette.rules";
import { BetPersistence } from "../persistences/bet.persistence";

export class BetRouletteController extends Controller implements ConfigControllerIdentity{
    private validateFields
    private auth
    private rules
    private mapper = new BetRouletteMapper()
    private service = new BetPersistence()

    constructor(){
        super()
        this.validateFields = new FieldsBetRouletteValidation
        this.auth = new Authentication()
        this.rules = new BetRouletteRules()
    }

    getConfigId(){
        return 'bet'
    }

    async postBet(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNjE2YWY3OWNhZTEzMzc0ZThjMTgyOTMwIiwibmlja25hbWUiOiJqb2huc2luYXBzaXMifSwiaWF0IjoxNjM0NTAwMzc5LCJleHAiOjE2MzcwOTIzNzl9.budIyUGG_n-VFrZJ1Xf1twIuu5gQ2l4TxD8IlMT6jT0
                let decoded = this.auth.validateToken(req.get('Authorization'),res);
                let params = {...req.body, userId:decoded.user.id}
                let dto:BetDto = this.mapper.toDto(params)
                await this.rules.exec(dto)
                dto = await this.service.createBet(dto)
                let response = this.mapper.toResponse(dto)
                
                return res.json(response)
            }
            else{
                let error = new ErrorBetRoulette()

                return res.json(error.buildResponse(res))
            }
        }
        catch(e){
            logger.error(e)

            return BaseErrorHandler.handle(res,e)
        }
    }
}