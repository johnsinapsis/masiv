import { CommonResponse } from "../../../shared/infrastructure/common.response";

export class GetRoulettesResponse extends CommonResponse{
    constructor(){
        super()
        this.type = "GetRoulettesResponse"
        this.response.description = "Obtención de ruletas realizado correctamente"
    }
}