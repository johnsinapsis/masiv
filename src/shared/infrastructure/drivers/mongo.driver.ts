import { MongoClient } from "mongodb";
import config from "../../../config/config";
import { logger } from "../../../helpers/default.logger";

export class MongoDriver{
    public db = config.db.database
    private engine = config.db.motor
    private host = config.db.host
    private port = config.db.port
    private user = config.db.user
    private password = config.db.pass
    private url:string
    public client:MongoClient

    constructor(){
        this.url = this.engine+'://'+this.user+':'+this.password+
                '@'+this.host+':'+this.port
        this.client = new MongoClient(this.url)
    }

    public async connection(){
        try{
            await this.client.connect()
            await this.client.db("admin").command({ ping: 1 });
            logger.info("Connected successfully to Mongo server");
            return this.client.db(this.db)
        }
        catch(e){
            logger.error(e)
        }
    }

    public async disconnection(){
        await this.client.close()
    }
}
