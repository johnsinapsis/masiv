"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnvConfig = void 0;
var EnvConfig = /** @class */ (function () {
    function EnvConfig() {
    }
    EnvConfig.pathSeparator = function () {
        var os = process.platform;
        if (os.startsWith("win")) {
            return "\\";
        }
        else {
            return "/";
        }
    };
    return EnvConfig;
}());
exports.EnvConfig = EnvConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRW52Q29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbmZpZy9FbnZDb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUE7SUFBQTtJQVNBLENBQUM7SUFSVSx1QkFBYSxHQUFwQjtRQUNJLElBQUksRUFBRSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUE7UUFDekIsSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3RCLE9BQU8sSUFBSSxDQUFBO1NBQ2Q7YUFBTTtZQUNILE9BQU8sR0FBRyxDQUFBO1NBQ2I7SUFDTCxDQUFDO0lBQ0wsZ0JBQUM7QUFBRCxDQUFDLEFBVEQsSUFTQztBQVRZLDhCQUFTIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEVudkNvbmZpZyB7XHJcbiAgICBzdGF0aWMgcGF0aFNlcGFyYXRvcigpIHtcclxuICAgICAgICBsZXQgb3MgPSBwcm9jZXNzLnBsYXRmb3JtXHJcbiAgICAgICAgaWYgKG9zLnN0YXJ0c1dpdGgoXCJ3aW5cIikpIHtcclxuICAgICAgICAgICAgcmV0dXJuIFwiXFxcXFwiXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIFwiL1wiXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59Il19