"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Dto = void 0;
var Dto = /** @class */ (function () {
    function Dto() {
    }
    Dto.prototype.getId = function () {
        return this.id;
    };
    Dto.prototype.setId = function (id) {
        this.id = id;
    };
    return Dto;
}());
exports.Dto = Dto;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5kdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvc2hhcmVkL2FwcGxpY2F0aW9uL2Jhc2UuZHRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBO0lBQUE7SUFRQSxDQUFDO0lBTlUsbUJBQUssR0FBWjtRQUNJLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQTtJQUNsQixDQUFDO0lBQ00sbUJBQUssR0FBWixVQUFhLEVBQVM7UUFDbEIsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUE7SUFDaEIsQ0FBQztJQUNMLFVBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQztBQVJZLGtCQUFHIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIER0b3tcclxuICAgIHByb3RlY3RlZCBpZFxyXG4gICAgcHVibGljIGdldElkKCl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaWRcclxuICAgIH1cclxuICAgIHB1YmxpYyBzZXRJZChpZDpzdHJpbmcpe1xyXG4gICAgICAgIHRoaXMuaWQgPSBpZFxyXG4gICAgfVxyXG59Il19