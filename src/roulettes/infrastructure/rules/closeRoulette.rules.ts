import { RulesBussiness } from "../../../shared/application/business/rules.bussiness";
import { ExistRouletteCondition } from "./conditions/existRoulette.condition";
import { OpenRouletteCondition } from "../../../bets/infrastructure/rules/conditions/openRoulette.condition";

export class CloseRouletteRules implements RulesBussiness {
    type: string;
    private existRoulettecondition
    private openRouletteCondition

    constructor(){
        this.type = "CloseRouletteRules"
        this.existRoulettecondition = new ExistRouletteCondition()
        this.openRouletteCondition = new OpenRouletteCondition()
    }
    
    public async exec(id: string) {
        await this.existRoulettecondition.exec(id)
        await this.openRouletteCondition.exec(id)
    }
}