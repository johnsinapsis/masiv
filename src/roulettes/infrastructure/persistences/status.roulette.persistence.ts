import { RouletteDto } from "../../application/dto/roulette.dto";
import { StatusRouletteRepository } from "../../application/repositories/status.roulette.repository";
import { Persistence } from "../../../shared/infrastructure/persistences/base.persistence";
import { RouletteModel } from "./models/roulette.model";

export class StatusRoulettePersistence extends Persistence implements StatusRouletteRepository{
    constructor(){
        super()
        this.model = new RouletteModel()
    }

    async setOpenStatusRoulette(id: string): Promise<boolean> {
        let _id = this.getObjectId(id)
        if(!_id)
            return null
        await this.setDBAndCollection()
        let updateDocument = {$set:{status:"Open"}}
        let result = await this.collection.updateOne({_id},updateDocument)
        this.driver.disconnection()
        if(!result)
            return false

        return true
    }

    async setCloseStatusRoulette(id: string): Promise<boolean> {
        let _id = this.getObjectId(id)
        if(!_id)
            return null
        await this.setDBAndCollection()
        let updateDocument = {$set:{status:"Close"}}
        let result = await this.collection.updateOne({_id},updateDocument)
        this.driver.disconnection()
        if(!result)
            return false

        return true
    }
}