"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controller = void 0;
var ConfigReader_1 = require("../../../config/ConfigReader");
var Controller = /** @class */ (function () {
    function Controller() {
        this.cr = ConfigReader_1.ConfigReader;
    }
    Controller.prototype.getPathsMap = function (controllerConfigId) {
        return this.cr.getPathsMap(controllerConfigId);
    };
    Controller.prototype.build400Response = function (res) {
        return {
            "code": 400,
            "invalid_fields": res.invalid_fields
        };
    };
    return Controller;
}());
exports.Controller = Controller;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udHJvbGxlci5iYXNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL3NoYXJlZC9pbmZyYXN0cnVjdHVyZS9jb250cm9sbGVycy9jb250cm9sbGVyLmJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsNkRBQTJEO0FBRTNEO0lBR0k7UUFDSSxJQUFJLENBQUMsRUFBRSxHQUFHLDJCQUFZLENBQUE7SUFDMUIsQ0FBQztJQUVELGdDQUFXLEdBQVgsVUFBWSxrQkFBa0I7UUFDMUIsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO0lBQ2xELENBQUM7SUFFRCxxQ0FBZ0IsR0FBaEIsVUFBaUIsR0FBRztRQUNoQixPQUFPO1lBQ0gsTUFBTSxFQUFHLEdBQUc7WUFDWixnQkFBZ0IsRUFBRSxHQUFHLENBQUMsY0FBYztTQUN2QyxDQUFBO0lBQ0wsQ0FBQztJQUNMLGlCQUFDO0FBQUQsQ0FBQyxBQWpCRCxJQWlCQztBQWpCWSxnQ0FBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbmZpZ1JlYWRlciB9IGZyb20gXCIuLi8uLi8uLi9jb25maWcvQ29uZmlnUmVhZGVyXCIgXHJcblxyXG5leHBvcnQgY2xhc3MgQ29udHJvbGxlciB7XHJcbiAgICBwcm90ZWN0ZWQgY3JcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcigpe1xyXG4gICAgICAgIHRoaXMuY3IgPSBDb25maWdSZWFkZXJcclxuICAgIH1cclxuXHJcbiAgICBnZXRQYXRoc01hcChjb250cm9sbGVyQ29uZmlnSWQpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNyLmdldFBhdGhzTWFwKGNvbnRyb2xsZXJDb25maWdJZClcclxuICAgIH0gXHJcblxyXG4gICAgYnVpbGQ0MDBSZXNwb25zZShyZXMpe1xyXG4gICAgICAgIHJldHVybiB7IFxyXG4gICAgICAgICAgICBcImNvZGVcIiA6IDQwMCxcclxuICAgICAgICAgICAgXCJpbnZhbGlkX2ZpZWxkc1wiOiByZXMuaW52YWxpZF9maWVsZHNcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iXX0=