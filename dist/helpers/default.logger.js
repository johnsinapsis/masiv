"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
var log4js_1 = require("log4js");
var config_1 = __importDefault(require("../config/config"));
(0, log4js_1.configure)({
    appenders: {
        console: { type: 'stdout', layout: { type: 'colored' } },
        dateFile: {
            type: 'dateFile',
            filename: config_1.default.logging.logDir + "/" + config_1.default.logging.logFile,
            layout: { type: 'basic' },
            compress: true,
            daysToKeep: 14,
            keepFileExt: true
        }
    },
    categories: {
        default: { appenders: ['console', 'dateFile'], level: config_1.default.logging.level }
    }
});
exports.logger = (0, log4js_1.getLogger)();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmYXVsdC5sb2dnZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvaGVscGVycy9kZWZhdWx0LmxvZ2dlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxpQ0FBOEM7QUFDOUMsNERBQXNDO0FBRXRDLElBQUEsa0JBQVMsRUFBQztJQUNOLFNBQVMsRUFBRTtRQUNULE9BQU8sRUFBRSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxFQUFFO1FBQ3hELFFBQVEsRUFBRTtZQUNSLElBQUksRUFBRSxVQUFVO1lBQ2hCLFFBQVEsRUFBSyxnQkFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLFNBQUksZ0JBQU0sQ0FBQyxPQUFPLENBQUMsT0FBUztZQUM5RCxNQUFNLEVBQUUsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFO1lBQ3pCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsVUFBVSxFQUFFLEVBQUU7WUFDZCxXQUFXLEVBQUUsSUFBSTtTQUNsQjtLQUNGO0lBQ0QsVUFBVSxFQUFFO1FBQ1YsT0FBTyxFQUFFLEVBQUUsU0FBUyxFQUFFLENBQUMsU0FBUyxFQUFFLFVBQVUsQ0FBQyxFQUFFLEtBQUssRUFBRSxnQkFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7S0FDN0U7Q0FDRixDQUFDLENBQUM7QUFDVSxRQUFBLE1BQU0sR0FBRyxJQUFBLGtCQUFTLEdBQUUsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNvbmZpZ3VyZSwgZ2V0TG9nZ2VyIH0gZnJvbSAnbG9nNGpzJztcclxuaW1wb3J0IGNvbmZpZyBmcm9tICcuLi9jb25maWcvY29uZmlnJztcclxuXHJcbmNvbmZpZ3VyZSh7XHJcbiAgICBhcHBlbmRlcnM6IHtcclxuICAgICAgY29uc29sZTogeyB0eXBlOiAnc3Rkb3V0JywgbGF5b3V0OiB7IHR5cGU6ICdjb2xvcmVkJyB9IH0sXHJcbiAgICAgIGRhdGVGaWxlOiB7XHJcbiAgICAgICAgdHlwZTogJ2RhdGVGaWxlJyxcclxuICAgICAgICBmaWxlbmFtZTogYCR7Y29uZmlnLmxvZ2dpbmcubG9nRGlyfS8ke2NvbmZpZy5sb2dnaW5nLmxvZ0ZpbGV9YCxcclxuICAgICAgICBsYXlvdXQ6IHsgdHlwZTogJ2Jhc2ljJyB9LFxyXG4gICAgICAgIGNvbXByZXNzOiB0cnVlLFxyXG4gICAgICAgIGRheXNUb0tlZXA6IDE0LFxyXG4gICAgICAgIGtlZXBGaWxlRXh0OiB0cnVlXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjYXRlZ29yaWVzOiB7XHJcbiAgICAgIGRlZmF1bHQ6IHsgYXBwZW5kZXJzOiBbJ2NvbnNvbGUnLCAnZGF0ZUZpbGUnXSwgbGV2ZWw6IGNvbmZpZy5sb2dnaW5nLmxldmVsIH1cclxuICAgIH1cclxuICB9KTtcclxuICBleHBvcnQgY29uc3QgbG9nZ2VyID0gZ2V0TG9nZ2VyKCk7Il19