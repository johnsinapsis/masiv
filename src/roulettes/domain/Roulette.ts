import { Entity } from "../../shared/domain/base.entity"
import { StatusRoulette } from "./StatusRoulette"

export class Roulette extends Entity{
    id:string
    date: string
    status: StatusRoulette
}