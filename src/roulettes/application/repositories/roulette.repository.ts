import { RouletteDto } from "../dto/roulette.dto";

export interface RouletteRepository{
    createRoulette(dto:RouletteDto): Promise <RouletteDto>
}