import jp from 'jsonpath'
import config from './config'

export class ConfigReader {
    static getPathsMap(controllerId) {
        return jp.query(config, 'ep.' + controllerId)[0]
    }
}