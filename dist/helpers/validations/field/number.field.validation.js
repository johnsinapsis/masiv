"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NumberFieldValidation = void 0;
var field_validation_1 = require("../field/field.validation");
var jsonpath_1 = __importDefault(require("jsonpath"));
var utilities_1 = require("../../utilities");
var constants_1 = require("../../../config/constants");
var NumberFieldValidation = /** @class */ (function (_super) {
    __extends(NumberFieldValidation, _super);
    function NumberFieldValidation(jpExpr, failMsg) {
        var _this = _super.call(this, jpExpr, failMsg) || this;
        _this.jpExpr = jpExpr;
        _this.failMsg = failMsg;
        return _this;
    }
    NumberFieldValidation.prototype.isValid = function (req, res) {
        var field = this.jpExpr.split('.')[1];
        var prop = req.body;
        if (req.params) {
            if (req.params.hasOwnProperty(field))
                prop = req.params;
        }
        field = jsonpath_1.default.query(prop, this.jpExpr)[0];
        if (!field)
            return true;
        return utilities_1.Utils.isNumber(field);
    };
    NumberFieldValidation.prototype.addErrorResponse = function (res, msg) {
        if (typeof res[constants_1.INV_FIELDS] === constants_1.UNDEF) {
            res.invalid_fields = [];
        }
        res.invalid_fields.push(msg);
    };
    return NumberFieldValidation;
}(field_validation_1.FieldValidator));
exports.NumberFieldValidation = NumberFieldValidation;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtYmVyLmZpZWxkLnZhbGlkYXRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvaGVscGVycy92YWxpZGF0aW9ucy9maWVsZC9udW1iZXIuZmllbGQudmFsaWRhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSw4REFBMEQ7QUFDMUQsc0RBQXlCO0FBQ3pCLDZDQUF1QztBQUN2Qyx1REFBNkQ7QUFFN0Q7SUFBMkMseUNBQWM7SUFDckQsK0JBQVksTUFBTSxFQUFFLE9BQU87UUFBM0IsWUFDSSxrQkFBTSxNQUFNLEVBQUUsT0FBTyxDQUFDLFNBR3pCO1FBRkcsS0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUE7UUFDcEIsS0FBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUE7O0lBQzFCLENBQUM7SUFFRCx1Q0FBTyxHQUFQLFVBQVEsR0FBRyxFQUFFLEdBQUc7UUFDWixJQUFJLEtBQUssR0FBVSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUM1QyxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFBO1FBQ25CLElBQUcsR0FBRyxDQUFDLE1BQU0sRUFBQztZQUNWLElBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDO2dCQUMvQixJQUFJLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQTtTQUN4QjtRQUNELEtBQUssR0FBRyxrQkFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3RDLElBQUcsQ0FBQyxLQUFLO1lBQ0wsT0FBTyxJQUFJLENBQUE7UUFFZixPQUFPLGlCQUFLLENBQUMsUUFBUSxDQUFFLEtBQUssQ0FBRSxDQUFBO0lBQ2xDLENBQUM7SUFFRCxnREFBZ0IsR0FBaEIsVUFBa0IsR0FBRyxFQUFFLEdBQUc7UUFDdEIsSUFBSSxPQUFPLEdBQUcsQ0FBQyxzQkFBVSxDQUFDLEtBQUssaUJBQUssRUFBRTtZQUNsQyxHQUFHLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQTtTQUMxQjtRQUNELEdBQUcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO0lBQ2hDLENBQUM7SUFDTCw0QkFBQztBQUFELENBQUMsQUEzQkQsQ0FBMkMsaUNBQWMsR0EyQnhEO0FBM0JZLHNEQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZpZWxkVmFsaWRhdG9yIH0gZnJvbSAnLi4vZmllbGQvZmllbGQudmFsaWRhdGlvbidcclxuaW1wb3J0IGpwIGZyb20gJ2pzb25wYXRoJ1xyXG5pbXBvcnQgeyBVdGlscyB9IGZyb20gJy4uLy4uL3V0aWxpdGllcycgXHJcbmltcG9ydCB7IElOVl9GSUVMRFMsIFVOREVGIH0gZnJvbSAnLi4vLi4vLi4vY29uZmlnL2NvbnN0YW50cydcclxuXHJcbmV4cG9ydCBjbGFzcyBOdW1iZXJGaWVsZFZhbGlkYXRpb24gZXh0ZW5kcyBGaWVsZFZhbGlkYXRvcntcclxuICAgIGNvbnN0cnVjdG9yKGpwRXhwciwgZmFpbE1zZyl7XHJcbiAgICAgICAgc3VwZXIoanBFeHByLCBmYWlsTXNnKVxyXG4gICAgICAgIHRoaXMuanBFeHByID0ganBFeHByXHJcbiAgICAgICAgdGhpcy5mYWlsTXNnID0gZmFpbE1zZ1xyXG4gICAgfVxyXG5cclxuICAgIGlzVmFsaWQocmVxLCByZXMpe1xyXG4gICAgICAgIGxldCBmaWVsZDpzdHJpbmcgPSB0aGlzLmpwRXhwci5zcGxpdCgnLicpWzFdXHJcbiAgICAgICAgbGV0IHByb3AgPSByZXEuYm9keVxyXG4gICAgICAgIGlmKHJlcS5wYXJhbXMpe1xyXG4gICAgICAgICAgICBpZihyZXEucGFyYW1zLmhhc093blByb3BlcnR5KGZpZWxkKSlcclxuICAgICAgICAgICAgICAgIHByb3AgPSByZXEucGFyYW1zXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZpZWxkID0ganAucXVlcnkocHJvcCwgdGhpcy5qcEV4cHIpWzBdXHJcbiAgICAgICAgaWYoIWZpZWxkKVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZVxyXG4gICAgICAgICAgICBcclxuICAgICAgICByZXR1cm4gVXRpbHMuaXNOdW1iZXIoIGZpZWxkIClcclxuICAgIH1cclxuXHJcbiAgICBhZGRFcnJvclJlc3BvbnNlKCByZXMsIG1zZyApeyAgICAgICAgXHJcbiAgICAgICAgaWYoIHR5cGVvZiByZXNbSU5WX0ZJRUxEU10gPT09IFVOREVGICl7XHJcbiAgICAgICAgICAgIHJlcy5pbnZhbGlkX2ZpZWxkcyA9IFtdXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJlcy5pbnZhbGlkX2ZpZWxkcy5wdXNoKG1zZylcclxuICAgIH1cclxufSJdfQ==