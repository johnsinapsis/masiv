import { BetDto } from "../../application/dto/bet.dto";
import { Bet } from "../../domain/Bet";
import { BetWriteRepository } from "../../application/repositories/betWrite.repository";
import { BetModel } from "./models/bet.model";
import { Persistence } from "../../../shared/infrastructure/persistences/base.persistence";
import { BetReadRepository } from "../../application/repositories/betRead.repository"; 

export class BetPersistence extends Persistence implements BetWriteRepository,BetReadRepository{
    constructor(){
        super()
        this.model = new BetModel()
    }

    async createBet(dto:BetDto):Promise <BetDto>{
        await this.setDBAndCollection()
        let result = await this.collection.insertOne(this.model.mapCollection(dto))
        this.driver.disconnection()
        if(result){
            if(result.insertedId)
                dto.setId(result.insertedId)
        }
        this.model.clearCollection()
        return dto
    }

    async getBetsByRouletteId(rouletteId: string): Promise<Array<Bet>> {
        await this.setDBAndCollection()
        let data = await this.collection.find({rouletteId})
        let dataList = await data.toArray()
        this.driver.disconnection()
        let betList = []
        if(!dataList)
            return null
        for(let row of dataList){
            let bet:Bet = this.model.mapEntity(row) 
            betList.push(bet)
        }
        return betList
    }
    
    
}