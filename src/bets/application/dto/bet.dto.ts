import { Dto } from "../../../shared/application/base.dto";
import { Color } from "../../domain/Color";
import { ResultType } from "../../domain/ResultType";

export class BetDto extends Dto{
    private rouletteId:string
    private color:Color
    private number:number 
    private mount:number
    private userId:string
    private date:string
    private result:ResultType
    private prize:number

    public getRouletteId(): string {
        return this.rouletteId;
    }

    public setRouletteId(rouletteId: string): void {
        this.rouletteId = rouletteId;
    }

    public getColor(): Color {
        return this.color;
    }

    public setColor(color: Color): void {
        this.color = color;
    }

    public getNumber(): number {
        return this.number;
    }

    public setNumber(number: number): void {
        this.number = number;
    }

    public getMount(): number {
        return this.mount;
    }

    public setMount(mount: number): void {
        this.mount = mount;
    }

    public getUserId(): string {
        return this.userId;
    }

    public setUserId(userId: string): void {
        this.userId = userId;
    }

    public getDate(): string {
        return this.date;
    }

    public setDate(date: string): void {
        this.date = date;
    }

    public getResult(): ResultType {
        return this.result;
    }

    public setResult(result: ResultType): void {
        this.result = result;
    }

    public getPrize(): number {
        return this.prize;
    }

    public setPrize(prize: number): void {
        this.prize = prize;
    }
}