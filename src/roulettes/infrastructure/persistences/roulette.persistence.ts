import { RouletteDto } from "../../application/dto/roulette.dto";
import { Roulette } from "../../domain/Roulette";
import { RouletteRepository } from "../../application/repositories/roulette.repository";
import { RouletteModel } from "./models/roulette.model";
import { Persistence } from "../../../shared/infrastructure/persistences/base.persistence";
import { RouletteReadRepository } from "../../application/repositories/rouletteRead.repository";

export class RoulettePersistence extends Persistence implements RouletteRepository, RouletteReadRepository{
    constructor(){
        super()
        this.model = new RouletteModel()
    }

    async createRoulette(dto: RouletteDto): Promise<RouletteDto> {
        await this.setDBAndCollection()
        let result = await this.collection.insertOne(this.model.mapCollection(dto))
        this.driver.disconnection()
        if(result){
            if(result.insertedId)
                dto.setId(result.insertedId)
        }
        this.model.clearCollection()
        
        return dto
    }

    async getRouletteById(id:string):Promise <Roulette>{
        let _id = this.getObjectId(id)
        if(!_id)
            return null
        await this.setDBAndCollection()
        let data = await this.collection.findOne({_id})
        this.driver.disconnection()
        if(!data)
            return null
        let roulette:Roulette = this.model.mapEntity(data)

        return roulette 
    }

    async getAllRoulettes(): Promise <Array<Roulette>>{
        await this.setDBAndCollection()
        let data = await this.collection.find({})
        let dataList = await data.toArray()
        this.driver.disconnection()
        let rouletteList = []
        if(!dataList)
            return null
        for(let row of dataList){
            let roulette:Roulette = this.model.mapEntity(row)
            rouletteList.push(roulette)
        }

        return rouletteList
    }
}