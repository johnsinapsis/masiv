import { FieldValidator } from './field.validation'
import jp from 'jsonpath'
import { INV_FIELDS, UNDEF,MAX_MOUNT_BET } from '../../../config/constants'

export class MountFieldValidator extends FieldValidator{
    constructor(jpExpr, failMsg){
        super(jpExpr, failMsg)
        this.jpExpr = jpExpr
        this.failMsg = failMsg
    }

    isValid(req, res){
        let field = jp.query(req.body, this.jpExpr)[0]
        if(field=== UNDEF || field === null)
            return true
        if(field < 0 || field===0 || field > MAX_MOUNT_BET)
            return false;

        return true;
    }

    addErrorResponse( res, msg ){        
        if( typeof res[INV_FIELDS] === UNDEF ){
            res.invalid_fields = []
        }
        res.invalid_fields.push(msg)
    }
}