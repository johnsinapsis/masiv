"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Condition = void 0;
var business_error_1 = require("../../application/handler/business.error");
var constants_1 = require("../../../config/constants");
var Condition = /** @class */ (function () {
    function Condition() {
    }
    Condition.prototype.throwBusiness = function (msg) {
        var response = { result: constants_1.RESULT_ERROR, description: msg };
        throw new business_error_1.BusinessError(this.type, response);
    };
    return Condition;
}());
exports.Condition = Condition;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5jb25kaXRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvc2hhcmVkL2luZnJhc3RydWN0dXJlL3J1bGVzL2Jhc2UuY29uZGl0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDJFQUF3RTtBQUN4RSx1REFBd0Q7QUFFeEQ7SUFBQTtJQU9BLENBQUM7SUFKVSxpQ0FBYSxHQUFwQixVQUFxQixHQUFHO1FBQ3BCLElBQUksUUFBUSxHQUFHLEVBQUMsTUFBTSxFQUFDLHdCQUFZLEVBQUUsV0FBVyxFQUFFLEdBQUcsRUFBQyxDQUFBO1FBQ3RELE1BQU0sSUFBSSw4QkFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUMsUUFBUSxDQUFDLENBQUE7SUFDL0MsQ0FBQztJQUNMLGdCQUFDO0FBQUQsQ0FBQyxBQVBELElBT0M7QUFQWSw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJ1c2luZXNzRXJyb3IgfSBmcm9tIFwiLi4vLi4vYXBwbGljYXRpb24vaGFuZGxlci9idXNpbmVzcy5lcnJvclwiXHJcbmltcG9ydCB7IFJFU1VMVF9FUlJPUiB9IGZyb20gXCIuLi8uLi8uLi9jb25maWcvY29uc3RhbnRzXCJcclxuXHJcbmV4cG9ydCBjbGFzcyBDb25kaXRpb24ge1xyXG4gICAgcHVibGljIHR5cGVcclxuXHJcbiAgICBwdWJsaWMgdGhyb3dCdXNpbmVzcyhtc2cpe1xyXG4gICAgICAgIGxldCByZXNwb25zZSA9IHtyZXN1bHQ6UkVTVUxUX0VSUk9SICxkZXNjcmlwdGlvbjogbXNnfVxyXG4gICAgICAgIHRocm93IG5ldyBCdXNpbmVzc0Vycm9yKHRoaXMudHlwZSxyZXNwb25zZSlcclxuICAgIH1cclxufSJdfQ==