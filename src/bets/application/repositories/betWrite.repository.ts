import { BetDto } from "../dto/bet.dto";

export interface BetWriteRepository{
    createBet(dto:BetDto): Promise <BetDto>
}