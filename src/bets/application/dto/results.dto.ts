import { Bet } from "../../domain/Bet";
import { WinningNumberDto } from "./winningNumber.dto";
import { FACTOR_PRIZE_WINNING_COLOR, FACTOR_PRIZE_WINNING_NUMBER} from "../../../config/constants";

export class ResultsDto {
    static getResults(bet:Bet,dtoWinnning:WinningNumberDto){
        let results = {
            result: 'Lost',
            prize: null
        }
        if(bet.number){
            if(bet.number===dtoWinnning.getWinnignNumber()){
                results.result = "Win"
                results.prize = bet.mount * FACTOR_PRIZE_WINNING_NUMBER
            }
        }
        if(bet.color){
            if(bet.color===dtoWinnning.getWinnigColor()){
                results.result = "Win"
                results.prize = bet.mount * FACTOR_PRIZE_WINNING_COLOR
            }
        }
        return results
    }
}