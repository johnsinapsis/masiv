"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Persistence = void 0;
var bson_1 = require("bson");
var mongo_driver_1 = require("../drivers/mongo.driver");
var utilities_1 = require("../../../helpers/utilities");
var Persistence = /** @class */ (function () {
    function Persistence() {
        this.driver = new mongo_driver_1.MongoDriver();
    }
    Persistence.prototype.getObjectId = function (id) {
        if (!utilities_1.Utils.isBson(id))
            return null;
        return new bson_1.ObjectId(id);
    };
    Persistence.prototype.setDBAndCollection = function () {
        return __awaiter(this, void 0, void 0, function () {
            var db;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.driver.connection()];
                    case 1:
                        db = _a.sent();
                        this.collection = db.collection(this.model.name);
                        return [2 /*return*/];
                }
            });
        });
    };
    return Persistence;
}());
exports.Persistence = Persistence;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5wZXJzaXN0ZW5jZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9zaGFyZWQvaW5mcmFzdHJ1Y3R1cmUvcGVyc2lzdGVuY2VzL2Jhc2UucGVyc2lzdGVuY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsNkJBQWdDO0FBQ2hDLHdEQUFzRDtBQUN0RCx3REFBbUQ7QUFFbkQ7SUFLSTtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSwwQkFBVyxFQUFFLENBQUE7SUFDbkMsQ0FBQztJQUVTLGlDQUFXLEdBQXJCLFVBQXNCLEVBQUU7UUFDcEIsSUFBRyxDQUFDLGlCQUFLLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQztZQUNoQixPQUFPLElBQUksQ0FBQTtRQUNmLE9BQU8sSUFBSSxlQUFRLENBQUMsRUFBRSxDQUFDLENBQUE7SUFDM0IsQ0FBQztJQUVLLHdDQUFrQixHQUF4Qjs7Ozs7NEJBQ2EscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsRUFBQTs7d0JBQW5DLEVBQUUsR0FBRyxTQUE4Qjt3QkFDdkMsSUFBSSxDQUFDLFVBQVUsR0FBSSxFQUFFLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7Ozs7O0tBQ3BEO0lBQ0wsa0JBQUM7QUFBRCxDQUFDLEFBbkJELElBbUJDO0FBbkJZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT2JqZWN0SWQgfSBmcm9tIFwiYnNvblwiO1xyXG5pbXBvcnQgeyBNb25nb0RyaXZlciB9IGZyb20gXCIuLi9kcml2ZXJzL21vbmdvLmRyaXZlclwiO1xyXG5pbXBvcnQgeyBVdGlscyB9IGZyb20gXCIuLi8uLi8uLi9oZWxwZXJzL3V0aWxpdGllc1wiO1xyXG5cclxuZXhwb3J0IGNsYXNzIFBlcnNpc3RlbmNle1xyXG4gICAgcHJvdGVjdGVkIGRyaXZlclxyXG4gICAgcHJvdGVjdGVkIGNvbGxlY3Rpb25cclxuICAgIHByb3RlY3RlZCBtb2RlbFxyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3Rvcigpe1xyXG4gICAgICAgIHRoaXMuZHJpdmVyID0gbmV3IE1vbmdvRHJpdmVyKClcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgZ2V0T2JqZWN0SWQoaWQpe1xyXG4gICAgICAgIGlmKCFVdGlscy5pc0Jzb24oaWQpKVxyXG4gICAgICAgICAgICByZXR1cm4gbnVsbFxyXG4gICAgICAgIHJldHVybiBuZXcgT2JqZWN0SWQoaWQpXHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgc2V0REJBbmRDb2xsZWN0aW9uKCl7XHJcbiAgICAgICAgbGV0IGRiID0gYXdhaXQgdGhpcy5kcml2ZXIuY29ubmVjdGlvbigpXHJcbiAgICAgICAgdGhpcy5jb2xsZWN0aW9uID0gIGRiLmNvbGxlY3Rpb24odGhpcy5tb2RlbC5uYW1lKVxyXG4gICAgfVxyXG59Il19