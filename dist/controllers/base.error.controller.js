"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseErrorController = void 0;
var BaseErrorController = /** @class */ (function () {
    function BaseErrorController() {
    }
    BaseErrorController.prototype.buildResponse = function (res) {
        var response = { error: '', /* line:'', file:'' */ };
        return this.toResponse(res, response);
    };
    BaseErrorController.prototype.toResponse = function (res, response) {
        var stack = new Error().stack;
        var newRes = this.map2response(response, res.invalid_fields[0], stack);
        return {
            type: this.type,
            response: newRes
        };
    };
    BaseErrorController.prototype.map2response = function (response, msg, stack) {
        response.error = msg;
        //por si se requiere detallar el error
        /* let properties = this.parseStack(stack)
        response.line = properties.line
        response.file = properties.file.substring(1) */
        return response;
    };
    return BaseErrorController;
}());
exports.BaseErrorController = BaseErrorController;
