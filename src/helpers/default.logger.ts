import { configure, getLogger } from 'log4js';
import config from '../config/config';

configure({
    appenders: {
      console: { type: 'stdout', layout: { type: 'colored' } },
      dateFile: {
        type: 'dateFile',
        filename: `${config.logging.logDir}/${config.logging.logFile}`,
        layout: { type: 'basic' },
        compress: true,
        daysToKeep: 14,
        keepFileExt: true
      }
    },
    categories: {
      default: { appenders: ['console', 'dateFile'], level: config.logging.level }
    }
  });
  export const logger = getLogger();