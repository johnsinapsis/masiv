"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RouteManager = void 0;
var controller_factory_1 = require("../shared/infrastructure/controllers/controller.factory");
var RouteManager = /** @class */ (function () {
    function RouteManager(router) {
        this.router = router;
        this.router = router;
    }
    RouteManager.prototype.bindAny = function (controlName) {
        var instance = controller_factory_1.ControllerFactory.createInstance(controlName);
        var pathsMap = instance.getPathsMap(instance.getConfigId());
        var _loop_1 = function (route) {
            if (route.startsWith("get")) {
                this_1.router.get(pathsMap[route], function (req, res) { return instance[route](req, res); });
            }
            if (route.startsWith("post")) {
                this_1.router.post(pathsMap[route], function (req, res) { return instance[route](req, res); });
            }
            if (route.startsWith("put")) {
                this_1.router.put(pathsMap[route], function (req, res) { return instance[route](req, res); });
            }
        };
        var this_1 = this;
        for (var route in pathsMap) {
            _loop_1(route);
        }
    };
    return RouteManager;
}());
exports.RouteManager = RouteManager;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvcm91dGUvcm91dGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsOEZBQTRGO0FBRTVGO0lBQ0ksc0JBQXVCLE1BQU07UUFBTixXQUFNLEdBQU4sTUFBTSxDQUFBO1FBQ3pCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO0lBQ3hCLENBQUM7SUFFRCw4QkFBTyxHQUFQLFVBQVEsV0FBVztRQUNmLElBQUksUUFBUSxHQUFHLHNDQUFpQixDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUM1RCxJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFBO2dDQUNsRCxLQUFLO1lBQ1YsSUFBSSxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFDO2dCQUN4QixPQUFLLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSyxPQUFBLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQXpCLENBQXlCLENBQUMsQ0FBQTthQUM1RTtZQUNELElBQUksS0FBSyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFFMUIsT0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFHLElBQUssT0FBQSxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUF6QixDQUF5QixDQUFDLENBQUE7YUFDN0U7WUFDRCxJQUFJLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBRXpCLE9BQUssTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUUsVUFBQyxHQUFHLEVBQUUsR0FBRyxJQUFLLE9BQUEsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFBO2FBQzVFOzs7UUFYTCxLQUFLLElBQUksS0FBSyxJQUFJLFFBQVE7b0JBQWpCLEtBQUs7U0FhYjtJQUNMLENBQUM7SUFDTCxtQkFBQztBQUFELENBQUMsQUF2QkQsSUF1QkM7QUF2Qlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250cm9sbGVyRmFjdG9yeSB9IGZyb20gXCIuLi9zaGFyZWQvaW5mcmFzdHJ1Y3R1cmUvY29udHJvbGxlcnMvY29udHJvbGxlci5mYWN0b3J5XCI7XHJcblxyXG5leHBvcnQgY2xhc3MgUm91dGVNYW5hZ2VyIHtcclxuICAgIGNvbnN0cnVjdG9yKCBwcm90ZWN0ZWQgcm91dGVyICl7XHJcbiAgICAgICAgdGhpcy5yb3V0ZXIgPSByb3V0ZXJcclxuICAgIH1cclxuXHJcbiAgICBiaW5kQW55KGNvbnRyb2xOYW1lKSB7XHJcbiAgICAgICAgbGV0IGluc3RhbmNlID0gQ29udHJvbGxlckZhY3RvcnkuY3JlYXRlSW5zdGFuY2UoY29udHJvbE5hbWUpXHJcbiAgICAgICAgbGV0IHBhdGhzTWFwID0gaW5zdGFuY2UuZ2V0UGF0aHNNYXAoaW5zdGFuY2UuZ2V0Q29uZmlnSWQoKSlcclxuICAgICAgICBmb3IgKGxldCByb3V0ZSBpbiBwYXRoc01hcCkge1xyXG4gICAgICAgICAgICBpZiAocm91dGUuc3RhcnRzV2l0aChcImdldFwiKSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5nZXQocGF0aHNNYXBbcm91dGVdLCAocmVxLCByZXMpID0+IGluc3RhbmNlW3JvdXRlXShyZXEsIHJlcykpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHJvdXRlLnN0YXJ0c1dpdGgoXCJwb3N0XCIpKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIucG9zdChwYXRoc01hcFtyb3V0ZV0sIChyZXEsIHJlcykgPT4gaW5zdGFuY2Vbcm91dGVdKHJlcSwgcmVzKSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAocm91dGUuc3RhcnRzV2l0aChcInB1dFwiKSkge1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLnB1dChwYXRoc01hcFtyb3V0ZV0sIChyZXEsIHJlcykgPT4gaW5zdGFuY2Vbcm91dGVdKHJlcSwgcmVzKSlcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iXX0=