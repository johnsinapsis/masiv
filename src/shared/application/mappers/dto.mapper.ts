import { Dto } from "../base.dto";

export interface DtoMapper{
    toDto(req:any):Dto
}