"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RouletteDto = void 0;
var RouletteDto = /** @class */ (function () {
    function RouletteDto() {
    }
    RouletteDto.prototype.getId = function () {
        return this.id;
    };
    RouletteDto.prototype.setId = function (id) {
        this.id = id;
    };
    RouletteDto.prototype.getDate = function () {
        return this.date;
    };
    RouletteDto.prototype.setDate = function (date) {
        this.date = date;
    };
    return RouletteDto;
}());
exports.RouletteDto = RouletteDto;
