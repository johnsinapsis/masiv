"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateUtil = void 0;
var DateUtil = /** @class */ (function () {
    function DateUtil() {
    }
    DateUtil.formatDate = function (date) {
        var z = date.getTimezoneOffset() * 60 * 1000;
        var tLocal = date - z;
        var localD = new Date(tLocal);
        var formated = localD.toISOString();
        return formated;
    };
    return DateUtil;
}());
exports.DateUtil = DateUtil;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS51dGlsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2hlbHBlcnMvZGF0ZS51dGlsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUVBO0lBQUE7SUFRQSxDQUFDO0lBUFUsbUJBQVUsR0FBakIsVUFBa0IsSUFBSTtRQUNsQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFBO1FBQzVDLElBQUksTUFBTSxHQUFHLElBQUksR0FBRyxDQUFDLENBQUE7UUFDckIsSUFBSSxNQUFNLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7UUFDN0IsSUFBSSxRQUFRLEdBQUcsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFBO1FBQ25DLE9BQU8sUUFBUSxDQUFBO0lBQ25CLENBQUM7SUFDTCxlQUFDO0FBQUQsQ0FBQyxBQVJELElBUUM7QUFSWSw0QkFBUSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50J1xyXG5cclxuZXhwb3J0IGNsYXNzIERhdGVVdGlse1xyXG4gICAgc3RhdGljIGZvcm1hdERhdGUoZGF0ZSkge1xyXG4gICAgICAgIGxldCB6ID0gZGF0ZS5nZXRUaW1lem9uZU9mZnNldCgpICogNjAgKiAxMDAwXHJcbiAgICAgICAgbGV0IHRMb2NhbCA9IGRhdGUgLSB6XHJcbiAgICAgICAgbGV0IGxvY2FsRCA9IG5ldyBEYXRlKHRMb2NhbClcclxuICAgICAgICBsZXQgZm9ybWF0ZWQgPSBsb2NhbEQudG9JU09TdHJpbmcoKVxyXG4gICAgICAgIHJldHVybiBmb3JtYXRlZFxyXG4gICAgfVxyXG59Il19