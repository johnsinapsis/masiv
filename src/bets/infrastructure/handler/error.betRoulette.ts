import { BaseErrorController } from "../../../shared/infrastructure/controllers/base.error.controller";

export class ErrorBetRoulette extends BaseErrorController{
    constructor(){
        super()
        this.type = "ErrorBetRoulette"
        this.stack = new Error().stack
    }
}