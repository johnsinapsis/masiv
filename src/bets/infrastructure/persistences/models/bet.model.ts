import { Model } from "../../../../shared/infrastructure/Model";
import { BetDto } from "../../../application/dto/bet.dto";
import { Bet } from "../../../domain/Bet";

export class BetModel extends Model{
    constructor(){
        super()
        this.name = 'bets'
        this.clearCollection()
    }

    mapCollection(data:BetDto){
        if(data.getId()){
            this.collection._id = data.getId()
        }
        if(data.getDate())    
            this.collection.date = data.getDate()
        this.collection.rouletteId = data.getRouletteId()
        this.collection.userId = data.getUserId()
        data.getColor() ? this.collection.color = data.getColor() :
            this.collection.number = data.getNumber()
        this.collection.mount = data.getMount()
        if(data.getResult())
            this.collection.result = data.getResult()
        if(data.getPrize())
            this.collection.prize = data.getPrize()

        return this.collection
    }

    mapEntity(data:any){
        let bet = new Bet()
        bet.id = data._id
        bet.date = data.date
        bet.rouletteId = data.rouletteId
        bet.userId = data.userId
        bet.mount = data.mount
        data.color ? bet.color = data.color : bet.number = data.number
        if(data.result) 
            bet.result = data.result
        if(data.prize)
            bet.prize = data.prize

        return bet
    }

    clearCollection(){
        this.collection = {
            _id:null,
            rouletteId:null,
            userId:null,
            date:'',
            number:null,
            color:null,
            mount:0,
            result:null,
            prize:null
        }
    }
}