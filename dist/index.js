"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var server_1 = require("./server/server");
var source_map_support_1 = __importDefault(require("source-map-support"));
source_map_support_1.default.install();
var Init = /** @class */ (function () {
    function Init() {
        this.server = new server_1.ExpressServer();
        this.rMng = this.server.getRouteManager();
    }
    Init.prototype.registerRoutes = function () {
        this.rMng.bindAny('CreateRouteController');
        this.rMng.bindAny('OpenRouletteController');
        this.rMng.bindAny('BetRouletteController');
        this.rMng.bindAny('CloseRouletteController');
        this.rMng.bindAny('GetRoulettesController');
    };
    return Init;
}());
var init = new Init();
init.registerRoutes();
init.server.startup();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSwwQ0FBZ0Q7QUFDaEQsMEVBQWlEO0FBQ2pELDRCQUFnQixDQUFDLE9BQU8sRUFBRSxDQUFBO0FBQzFCO0lBSUk7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksc0JBQWEsRUFBRSxDQUFBO1FBQ2pDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQTtJQUM3QyxDQUFDO0lBRUQsNkJBQWMsR0FBZDtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLENBQUE7UUFDMUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsQ0FBQTtRQUMzQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBO1FBQzFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUE7UUFDNUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsQ0FBQTtJQUMvQyxDQUFDO0lBQ0wsV0FBQztBQUFELENBQUMsQUFoQkQsSUFnQkM7QUFFRCxJQUFJLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFBO0FBQ3JCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQTtBQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRXhwcmVzc1NlcnZlciB9IGZyb20gXCIuL3NlcnZlci9zZXJ2ZXJcIjtcclxuaW1wb3J0IHNvdXJjZU1hcFN1cHBvcnQgZnJvbSAnc291cmNlLW1hcC1zdXBwb3J0J1xyXG5zb3VyY2VNYXBTdXBwb3J0Lmluc3RhbGwoKVxyXG5jbGFzcyBJbml0IHtcclxuICAgIHNlcnZlclxyXG4gICAgck1uZ1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCl7XHJcbiAgICAgICAgdGhpcy5zZXJ2ZXIgPSBuZXcgRXhwcmVzc1NlcnZlcigpXHJcbiAgICAgICAgdGhpcy5yTW5nID0gdGhpcy5zZXJ2ZXIuZ2V0Um91dGVNYW5hZ2VyKClcclxuICAgIH1cclxuICAgIFxyXG4gICAgcmVnaXN0ZXJSb3V0ZXMoKXtcclxuICAgICAgICB0aGlzLnJNbmcuYmluZEFueSgnQ3JlYXRlUm91dGVDb250cm9sbGVyJylcclxuICAgICAgICB0aGlzLnJNbmcuYmluZEFueSgnT3BlblJvdWxldHRlQ29udHJvbGxlcicpXHJcbiAgICAgICAgdGhpcy5yTW5nLmJpbmRBbnkoJ0JldFJvdWxldHRlQ29udHJvbGxlcicpXHJcbiAgICAgICAgdGhpcy5yTW5nLmJpbmRBbnkoJ0Nsb3NlUm91bGV0dGVDb250cm9sbGVyJylcclxuICAgICAgICB0aGlzLnJNbmcuYmluZEFueSgnR2V0Um91bGV0dGVzQ29udHJvbGxlcicpXHJcbiAgICB9XHJcbn1cclxuXHJcbmxldCBpbml0ID0gbmV3IEluaXQoKVxyXG5pbml0LnJlZ2lzdGVyUm91dGVzKClcclxuaW5pdC5zZXJ2ZXIuc3RhcnR1cCgpIl19