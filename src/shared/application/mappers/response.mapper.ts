import { CommonResponse } from "../../infrastructure/common.response";
import { Dto } from "../base.dto";

export interface ResponseMapper{
    toResponse(dto:Dto): CommonResponse
}