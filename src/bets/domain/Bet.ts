import { Entity } from "../../shared/domain/base.entity"
import { Color } from './Color'
import { ResultType } from "./ResultType"

export class Bet extends Entity{
    id:string
    rouletteId:string
    color: Color
    number:number
    mount:number
    userId: string
    date: string
    result: ResultType
    prize:number
}