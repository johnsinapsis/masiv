# Backend Ruletta
## Prueba técnica Node.js - Typescript - Arquitectura Hexagonal - Principios SOLID - CleanCode
Para lanzar en **_modo desarrollo_**
#### Dev Mode 
```shell script
npm run dev
```
_Este proyecto permite observar el desarrollo con buenas prácticas basado en node.js y typescript. Se maneja una arquitectura limpia basada en puertos y adaptadores y principios SOLID buscando que cada clase tenga una responsabilidad única, asimismo, cada clase existe dentro de un archivo excepto cuando requieren constantes que pueden ser o no exportadas.

_En cuanto al cleanCode se intenta aproximar lo mejor posible a las recomedaciones de Robert C. Martin. Solo se dejan líneas vacías entre métodos para garantizar la legibilidad del código y antes de cada ```return```. 

_El código esta altamente cohesionado y bajamente acoplado por lo que permite robustez y escalabilidad. Como todo, está susceptible a mejoras ya que aunque se intenta seguir unas normas de calidad, es posible que hayan mejores formas de incrementar dicha calidad.

### Pre-requisitos 📋

_Node.js_. Debe instalar los paquetes que se encuentran en el package.json después de haber clonado el repositorio
```
npm install
```
_El proyecto está desarrollado para funcionar con mongoDB pero como el proyecto tiene bajo acoplamiento con tan solo reemplazar el driver y algunas funciones podrá reemplazar el gestor de BD ya sea relacional o no.

## Pruebas Unitarias
Las pruebas unitarias están pendientes debido a falta de tiempo. Sin embargo, el proyecto ya tiene los prerrequisitos mínimos para hacer pruebas unitarias con mocha y chai
#### Unit Test 
```shell script
npm run test
```

## Configuración ⚙️

_En la ruta /src/config/config.ts encontrará el archivo de configuración de los endpoint. Puede hacer uso de un fichero .env para agregar las variables de entorno. puede ver un ejemplo en la raíz de proyecto en el fichero .env.sample_

## Notas importantes 📖
* Hay una línea comentada intencionalmente en la linea 34 del controller BetRouletteController para que puedan realizar las pruebas de validación del token enviado por el consumidor. Debe usarse en el Header con la key Authorization. O en su defecto comentar la línea de autenticación y asignar valor al userId. Esto se hace con el fin de evitar incluso en pruebas técnicas y ejemplos enviar datos del usuario sin encriptar.
* Este código también aplica DDD para mejorar la estructura del proyecto. Este código es susceptible de mejora por lo que se realizarán algunas mejoras a medida que sean detectadas. 
* Quedó pendiente la documentación swagger para los endpoint por falta de tiempo.

## Autor ✒️

* **John Jairo González** - *Desarrollador Backend Node.js* - [johnsinapsis](https://gitlab.com/johnsinapsis)

## Licencia 📄

Este proyecto es una prueba técnica y es libre para que sea una base para mejorar los principios de cleanCode, Clean Arquitecture así como Principios SOLID


