"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.RouletteModel = void 0;
var Model_1 = require("../../../../shared/infrastructure/Model");
var Roulette_1 = require("../../../domain/Roulette");
var RouletteModel = /** @class */ (function (_super) {
    __extends(RouletteModel, _super);
    function RouletteModel() {
        var _this = _super.call(this) || this;
        _this.name = 'roulettes';
        _this.clearCollection();
        return _this;
    }
    RouletteModel.prototype.mapCollection = function (data) {
        if (data.getId()) {
            this.collection._id = data.getId();
        }
        if (data.getDate())
            this.collection.date = data.getDate();
        if (data.getStatus())
            this.collection.status = data.getStatus();
        return this.collection;
    };
    RouletteModel.prototype.mapEntity = function (data) {
        var roulette = new Roulette_1.Roulette();
        roulette.id = data._id;
        roulette.date = data.date;
        if (data.status)
            roulette.status = data.status;
        else
            roulette.status = "Close";
        return roulette;
    };
    RouletteModel.prototype.clearCollection = function () {
        this.collection = {
            _id: null,
            date: '',
            status: null
        };
    };
    return RouletteModel;
}(Model_1.Model));
exports.RouletteModel = RouletteModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91bGV0dGUubW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvcm91bGV0dGVzL2luZnJhc3RydWN0dXJlL3BlcnNpc3RlbmNlcy9tb2RlbHMvcm91bGV0dGUubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsaUVBQStEO0FBRS9ELHFEQUFvRDtBQUVwRDtJQUFtQyxpQ0FBSztJQUNwQztRQUFBLFlBQ0ksaUJBQU8sU0FHVjtRQUZHLEtBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFBO1FBQ3ZCLEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQTs7SUFDMUIsQ0FBQztJQUVELHFDQUFhLEdBQWIsVUFBYyxJQUFnQjtRQUMxQixJQUFHLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBQztZQUNaLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtTQUNyQztRQUNELElBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNiLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQTtRQUN6QyxJQUFHLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDZixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUE7UUFDN0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFBO0lBQzFCLENBQUM7SUFFRCxpQ0FBUyxHQUFULFVBQVUsSUFBUTtRQUNkLElBQUksUUFBUSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFBO1FBQzdCLFFBQVEsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQTtRQUN0QixRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7UUFDekIsSUFBRyxJQUFJLENBQUMsTUFBTTtZQUNWLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQTs7WUFFN0IsUUFBUSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUE7UUFDN0IsT0FBTyxRQUFRLENBQUE7SUFDbkIsQ0FBQztJQUVELHVDQUFlLEdBQWY7UUFDSSxJQUFJLENBQUMsVUFBVSxHQUFHO1lBQ2QsR0FBRyxFQUFDLElBQUk7WUFDUixJQUFJLEVBQUMsRUFBRTtZQUNQLE1BQU0sRUFBQyxJQUFJO1NBQ2QsQ0FBQTtJQUNMLENBQUM7SUFDTCxvQkFBQztBQUFELENBQUMsQUFwQ0QsQ0FBbUMsYUFBSyxHQW9DdkM7QUFwQ1ksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2RlbCB9IGZyb20gXCIuLi8uLi8uLi8uLi9zaGFyZWQvaW5mcmFzdHJ1Y3R1cmUvTW9kZWxcIlxyXG5pbXBvcnQgeyBSb3VsZXR0ZUR0byB9IGZyb20gXCIuLi8uLi8uLi9hcHBsaWNhdGlvbi9kdG8vcm91bGV0dGUuZHRvXCI7XHJcbmltcG9ydCB7IFJvdWxldHRlIH0gZnJvbSBcIi4uLy4uLy4uL2RvbWFpbi9Sb3VsZXR0ZVwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIFJvdWxldHRlTW9kZWwgZXh0ZW5kcyBNb2RlbCB7XHJcbiAgICBjb25zdHJ1Y3Rvcigpe1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5uYW1lID0gJ3JvdWxldHRlcydcclxuICAgICAgICB0aGlzLmNsZWFyQ29sbGVjdGlvbigpXHJcbiAgICB9XHJcblxyXG4gICAgbWFwQ29sbGVjdGlvbihkYXRhOlJvdWxldHRlRHRvKXtcclxuICAgICAgICBpZihkYXRhLmdldElkKCkpe1xyXG4gICAgICAgICAgICB0aGlzLmNvbGxlY3Rpb24uX2lkID0gZGF0YS5nZXRJZCgpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKGRhdGEuZ2V0RGF0ZSgpKSAgICBcclxuICAgICAgICAgICAgdGhpcy5jb2xsZWN0aW9uLmRhdGUgPSBkYXRhLmdldERhdGUoKVxyXG4gICAgICAgIGlmKGRhdGEuZ2V0U3RhdHVzKCkpXHJcbiAgICAgICAgICAgIHRoaXMuY29sbGVjdGlvbi5zdGF0dXMgPSBkYXRhLmdldFN0YXR1cygpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY29sbGVjdGlvblxyXG4gICAgfVxyXG5cclxuICAgIG1hcEVudGl0eShkYXRhOmFueSl7XHJcbiAgICAgICAgbGV0IHJvdWxldHRlID0gbmV3IFJvdWxldHRlKClcclxuICAgICAgICByb3VsZXR0ZS5pZCA9IGRhdGEuX2lkXHJcbiAgICAgICAgcm91bGV0dGUuZGF0ZSA9IGRhdGEuZGF0ZVxyXG4gICAgICAgIGlmKGRhdGEuc3RhdHVzKVxyXG4gICAgICAgICAgICByb3VsZXR0ZS5zdGF0dXMgPSBkYXRhLnN0YXR1c1xyXG4gICAgICAgIGVsc2VcclxuICAgICAgICAgICAgcm91bGV0dGUuc3RhdHVzID0gXCJDbG9zZVwiXHJcbiAgICAgICAgcmV0dXJuIHJvdWxldHRlXHJcbiAgICB9XHJcblxyXG4gICAgY2xlYXJDb2xsZWN0aW9uKCl7XHJcbiAgICAgICAgdGhpcy5jb2xsZWN0aW9uID0ge1xyXG4gICAgICAgICAgICBfaWQ6bnVsbCxcclxuICAgICAgICAgICAgZGF0ZTonJyxcclxuICAgICAgICAgICAgc3RhdHVzOm51bGxcclxuICAgICAgICB9IFxyXG4gICAgfVxyXG59Il19