import { Roulette } from "../../domain/Roulette";
import { RouletteDto } from "../../application/dto/roulette.dto";
import { GetRoulettesResponse } from "../responses/getRoulettes.response";
import { DtoMapper } from "../../../shared/application/mappers/dto.mapper";

export class GetRoulettesMapper implements DtoMapper{
    toDto(req:any):RouletteDto{
        let dto = new RouletteDto()
        if(req.id)
            dto.setId(req.id)
        if(req.date)
            dto.setDate(req.date)
        if(req.status)
            dto.setStatus(req.status)

        return dto
    }

    toResponseWithListRouletteDto(roulettes:Array<Roulette>){
        let response = new GetRoulettesResponse()
        let rouletteList = []
        for(let roulette of roulettes){
            let dto = this.toDto(roulette)
            rouletteList.push(dto)
        }
        response.response["roulettes"] = rouletteList
        
        return response
    }
}