import moment from 'moment'

export class DateUtil{
    static formatDate(date) {
        let z = date.getTimezoneOffset() * 60 * 1000
        let tLocal = date - z
        let localD = new Date(tLocal)
        let formated = localD.toISOString()
        return formated
    }
}