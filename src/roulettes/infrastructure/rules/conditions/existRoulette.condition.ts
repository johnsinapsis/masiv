import { RulesBussiness } from "../../../../shared/application/business/rules.bussiness";
import { ExistRouletteBusinessRule } from "../../../application/business.rules/existRoulette.business.rule";
import { Condition } from "../../../../shared/infrastructure/rules/base.condition";
import { Roulette } from "../../../domain/Roulette";
import { RoulettePersistence } from "../../persistences/roulette.persistence";


export class ExistRouletteCondition extends Condition implements RulesBussiness, ExistRouletteBusinessRule{
    private dao

    constructor(){
        super()
        this.type = "ExistRouletteCondition"
        this.dao = new RoulettePersistence()
    }

    public async rouletteExist(id:string){
        let roulette:Roulette = await this.dao.getRouletteById(id)
        console.log(roulette);
        if(!roulette)
            return false

        return true
    }

    public async exec(id:string){
        if(!await this.rouletteExist(id))
            this.throwBusiness("El id de la ruleta no existe")
    }
}