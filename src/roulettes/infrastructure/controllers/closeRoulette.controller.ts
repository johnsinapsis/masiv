import { Controller } from "../../../shared/infrastructure/controllers/controller.base";
import { logger } from "../../../helpers/default.logger";
import { BaseErrorHandler } from "../../../shared/infrastructure/handlerErrors/base.error.handler";
import { ConfigControllerIdentity } from "../../../shared/infrastructure/controllers/config.controller.identity";
import { WinningNumberDto } from "../../../bets/application/dto/winningNumber.dto";
import { StatusRoulettePersistence } from "../persistences/status.roulette.persistence";
import { BetResultPersistence } from "../../../bets/infrastructure/persistences/betResults.persistence";
import { FieldsCloseRouletteValidation } from "../../application/validations/fieldsCloseRoulette.validation";
import { ErrorCloseRoulette } from "../handler/error.closeRoulette";
import { CloseRouletteRules } from "../rules/closeRoulette.rules";
import { CloseRouletteMapper } from "../mappers/closeRoulette.mapper";

export class CloseRouletteController extends Controller implements ConfigControllerIdentity{
    private validateFields
    private rules
    private mapper = new CloseRouletteMapper()
    private rouletteStatusService = new StatusRoulettePersistence()
    private resultService = new BetResultPersistence()

    constructor(){
        super()
        this.validateFields = new FieldsCloseRouletteValidation()
        this.rules = new CloseRouletteRules()
    }

    getConfigId(){
        return 'closeRoulette'
    }

    async putCloseRoulette(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                await this.rules.exec(req.body.id)
                this.rouletteStatusService.setCloseStatusRoulette(req.body.id)
                let winnignNumber:WinningNumberDto = this.resultService.selectWinningNumber()
                let bets = await this.resultService.updateBetsResults(req.body.id,winnignNumber)
                let response = this.mapper.toResponseWithListBetDto(bets,winnignNumber)

                return res.json(response)
            }
            else{
                let error = new ErrorCloseRoulette()

                return res.json(error.buildResponse(res))
            }
        }
        catch(e){
            logger.error(e)
            
            return BaseErrorHandler.handle(res,e)
        }
    }
}