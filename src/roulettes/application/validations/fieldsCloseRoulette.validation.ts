import { BaseValidation } from "../../../shared/application/validation/base.validation";

export class FieldsCloseRouletteValidation extends BaseValidation{
    constructor(){
        super()
        let emptyId = this.fieldValidationFactory.createInstance('empty','$.id','El id de ruleta es requerido')
        this.addValidator(emptyId)
    }
}