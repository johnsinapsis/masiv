"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseErrorHandler = void 0;
var business_error_handler_1 = require("./business.error.handler");
var stackParser_handler_1 = require("./stackParser.handler");
var business_error_1 = require("../application/handler/business.error");
var BaseErrorHandler = /** @class */ (function () {
    function BaseErrorHandler() {
    }
    BaseErrorHandler.handle = function (res, e) {
        if (e instanceof business_error_1.BusinessError) {
            var properties = this.parseStack(e.response);
            this.clear(e);
            return res.status(200).json(e);
        }
        else {
            return res.status(400).json({ error: "Excepción no controlada del sistema" });
        }
    };
    BaseErrorHandler.parseStack = function (e) {
        var parser = new stackParser_handler_1.StackParser(e.stack);
        var line = parser.getTopLine();
        var file = parser.getTopFile();
        return { line: line, file: file };
    };
    BaseErrorHandler.clear = function (e) {
        delete e.code;
        delete e.response.stack;
    };
    BaseErrorHandler.beh = new business_error_handler_1.BusinessErrorHandler();
    return BaseErrorHandler;
}());
exports.BaseErrorHandler = BaseErrorHandler;
