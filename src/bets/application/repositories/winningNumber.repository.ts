import { WinningNumberDto } from "../dto/winningNumber.dto";
import { Bet } from "../../domain/Bet"; 

export interface WinningNumberRepository{
    selectWinningNumber():WinningNumberDto
    updateBetsResults(rouletteId:string,dtoWinning:WinningNumberDto): Promise <Array<Bet>>
}