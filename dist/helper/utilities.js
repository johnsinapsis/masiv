"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Utils = void 0;
var constants_1 = require("../config/constants");
var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.isEmpty = function (text) {
        return (Utils.isUndfNull(text) || text.toString() == "");
    };
    Utils.isUndfNull = function (text) {
        return (typeof text == constants_1.UNDEF || text == null);
    };
    Utils.isNumber = function (text) {
        if (!Utils.isEmpty(text)) {
            return !isNaN(text);
        }
        return false;
    };
    Utils.isDate = function (date) {
        if (!/\d{4}-\d{2}-\d{2}/.test(date))
            return false;
        var d = new Date(date);
        if (isNaN(d.getTime()))
            return false;
        return true;
    };
    Utils.isSuccessRange = function (dateStart, dateEnd) {
        var f1 = new Date(dateStart);
        var f2 = new Date(dateEnd);
        return !(f2 < f1);
    };
    Utils.setLocalDate = function (date) {
        var z = date.getTimezoneOffset() * 60 * 1000;
        var tLocal = date - z;
        var localD = new Date(tLocal);
        return localD;
    };
    return Utils;
}());
exports.Utils = Utils;
