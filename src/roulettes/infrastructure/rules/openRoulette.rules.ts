import { RulesBussiness } from "../../../shared/application/business/rules.bussiness";
import { ExistRouletteCondition } from "./conditions/existRoulette.condition";
import { OpenRouletteCondition } from "./conditions/openRoulette.condition";

export class OpenRouletteRules implements RulesBussiness {
    type: string;
    private existRoulettecondition
    private openRouletteCondition

    constructor(){
        this.type = "OpenRouletteRules"
        this.existRoulettecondition = new ExistRouletteCondition()
        this.openRouletteCondition = new OpenRouletteCondition()
    }
    
    public async exec(id: string) {
        await this.existRoulettecondition.exec(id)
        await this.openRouletteCondition.exec(id)
    }
}