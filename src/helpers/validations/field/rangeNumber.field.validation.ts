import { FieldValidator } from './field.validation'
import jp from 'jsonpath'
import { INV_FIELDS, UNDEF } from '../../../config/constants'
import { START_NUMBER_BET,END_NUMBER_BET } from '../../../config/constants'

export class RangeNumberFieldValidation extends FieldValidator{
    constructor(jpExpr, failMsg){
        super(jpExpr, failMsg)
        this.jpExpr = jpExpr
        this.failMsg = failMsg
    }

    isValid(req, res){
        let field = jp.query(req.body, this.jpExpr)[0]
        if(!field)
            return true
        if(field < START_NUMBER_BET || field > END_NUMBER_BET)
            return false;

        return true;
    }

    addErrorResponse( res, msg ){        
        if( typeof res[INV_FIELDS] === UNDEF ){
            res.invalid_fields = []
        }
        res.invalid_fields.push(msg)
    }
}