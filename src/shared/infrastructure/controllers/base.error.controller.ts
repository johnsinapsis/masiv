import { RESULT_ERROR } from "../../../config/constants"
import { StackParser } from "../handlerErrors/stackParser.handler" 
import { logger } from "../../../helpers/default.logger"

export class BaseErrorController{
    protected type
    protected stack
    
    buildResponse(res){
        let response = {result:RESULT_ERROR ,description:'',line:'', file:'' }
        return this.toResponse(res, response)
    }

    public toResponse(res,response){
        let newRes = this.map2response(response,res.invalid_fields[0])
        let error = this.parseStack()
        newRes.line = error.line
        newRes.file = error.file.substring(1)
        logger.error(`ErrorType: ${this.type}, file: ${newRes.file}, line: ${newRes.line} `)

        return {
            type: this.type,
            response:newRes
        } 
    }

    protected map2response(response,msg){
        response.description = msg

        return response
    }
    
    private parseStack() {
        let parser = new StackParser(this.stack)
        let line = parser.getTopLine()
        let file = parser.getTopFile()

        return {line,file}
    }
}