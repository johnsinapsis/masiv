import { Model } from "../../../../shared/infrastructure/Model"
import { RouletteDto } from "../../../application/dto/roulette.dto";
import { Roulette } from "../../../domain/Roulette";

export class RouletteModel extends Model {
    constructor(){
        super();
        this.name = 'roulettes'
        this.clearCollection()
    }

    mapCollection(data:RouletteDto){
        if(data.getId()){
            this.collection._id = data.getId()
        }
        if(data.getDate())    
            this.collection.date = data.getDate()
        if(data.getStatus())
            this.collection.status = data.getStatus()
        return this.collection
    }

    mapEntity(data:any){
        let roulette = new Roulette()
        roulette.id = data._id
        roulette.date = data.date
        if(data.status)
            roulette.status = data.status
        else
            roulette.status = "Close"
        return roulette
    }

    clearCollection(){
        this.collection = {
            _id:null,
            date:'',
            status:null
        } 
    }
}