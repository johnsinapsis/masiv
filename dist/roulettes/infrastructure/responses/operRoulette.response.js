"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenRouletteResponse = void 0;
var common_response_1 = require("../../../shared/infrastructure/common.response");
var OpenRouletteResponse = /** @class */ (function (_super) {
    __extends(OpenRouletteResponse, _super);
    function OpenRouletteResponse() {
        var _this = _super.call(this) || this;
        _this.type = "OpenRouletteResponse";
        _this.response.description = "Se ha abierto la ruleta correctamente";
        return _this;
    }
    return OpenRouletteResponse;
}(common_response_1.CommonResponse));
exports.OpenRouletteResponse = OpenRouletteResponse;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlclJvdWxldHRlLnJlc3BvbnNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL3JvdWxldHRlcy9pbmZyYXN0cnVjdHVyZS9yZXNwb25zZXMvb3BlclJvdWxldHRlLnJlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGtGQUFnRjtBQUVoRjtJQUEwQyx3Q0FBYztJQUNwRDtRQUFBLFlBQ0ksaUJBQU8sU0FHVjtRQUZHLEtBQUksQ0FBQyxJQUFJLEdBQUcsc0JBQXNCLENBQUE7UUFDbEMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsdUNBQXVDLENBQUE7O0lBQ3ZFLENBQUM7SUFDTCwyQkFBQztBQUFELENBQUMsQUFORCxDQUEwQyxnQ0FBYyxHQU12RDtBQU5ZLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1vblJlc3BvbnNlIH0gZnJvbSBcIi4uLy4uLy4uL3NoYXJlZC9pbmZyYXN0cnVjdHVyZS9jb21tb24ucmVzcG9uc2VcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBPcGVuUm91bGV0dGVSZXNwb25zZSBleHRlbmRzIENvbW1vblJlc3BvbnNle1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy50eXBlID0gXCJPcGVuUm91bGV0dGVSZXNwb25zZVwiXHJcbiAgICAgICAgdGhpcy5yZXNwb25zZS5kZXNjcmlwdGlvbiA9IFwiU2UgaGEgYWJpZXJ0byBsYSBydWxldGEgY29ycmVjdGFtZW50ZVwiXHJcbiAgICB9XHJcbn0iXX0=