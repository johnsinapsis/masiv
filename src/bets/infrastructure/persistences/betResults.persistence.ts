import { Persistence } from "../../../shared/infrastructure/persistences/base.persistence";
import { Bet } from "../../domain/Bet";
import { WinningNumberRepository } from "../../application/repositories/winningNumber.repository";
import { WinningNumberDto } from "../../application/dto/winningNumber.dto";
import { START_NUMBER_BET, END_NUMBER_BET } from "../../../config/constants";
import { BetModel } from "./models/bet.model";
import { BetPersistence } from "./bet.persistence";
import { logger } from "../../../helpers/default.logger";
import { ResultsDto } from "../../application/dto/results.dto";

export class BetResultPersistence extends Persistence implements WinningNumberRepository{
    private session;
    private transactionOptions 
    constructor(){
        super()
        this.model = new BetModel()
        this.transactionOptions = {
            readPreference: 'primary',
            readConcern: { level: 'local' },
            writeConcern: { w: 'majority' }
        };
    }

    startSession(){
        this.session = this.driver.client.startSession()
    }

    selectWinningNumber(): WinningNumberDto {
        let dto = new WinningNumberDto()
        dto.setWinningNumber(Math.floor(
            Math.random() * (START_NUMBER_BET - END_NUMBER_BET) + END_NUMBER_BET
            ))

        return dto
    }

    async updateBetsResults(rouletteId:string,dtoWinnning:WinningNumberDto):Promise <Array<Bet>>{
        let betService = new BetPersistence()
        let betsList:Array<Bet> = await betService.getBetsByRouletteId(rouletteId) 
        await this.setDBAndCollection()
        this.startSession()
        try {
            const transactionResults = await this.session.withTransaction(async () => {
                for(let bet of betsList){
                    let _id = typeof bet.id === 'object' ? bet.id : this.getObjectId(bet.id)
                    let updateDocument = {$set:ResultsDto.getResults(bet,dtoWinnning)}
                    let result = await this.collection.updateOne({_id},updateDocument)
                    if(!result)
                        await this.session.abortTransaction();
                } 
            },this.transactionOptions)
        } catch(e){
            logger.error(e)
        } finally {
            await this.session.endSession();
            this.driver.disconnection()
        }
        betsList = await betService.getBetsByRouletteId(rouletteId)
        
        return betsList
    }
}