import { Roulette } from "../../domain/Roulette";
import { RouletteDto } from "../../application/dto/roulette.dto";
import { DtoMapper } from "../../../shared/application/mappers/dto.mapper";
import { ResponseMapper } from "../../../shared/application/mappers/response.mapper";
import { CreateRouletteResponse } from "../responses/createRoulette.response";
import { Dto } from "../../../shared/application/base.dto";

export class CreateRouletteMapper implements DtoMapper, ResponseMapper{
    toDto(req: any): RouletteDto {
        let dto = new RouletteDto()
        dto.setId('')
        let date = new Date()
        dto.setDate(date.toISOString())

        return dto
    }
    
    toResponse(dto:RouletteDto){
        let response = new CreateRouletteResponse()
        response.response["roulette"] = dto
        
        return response
    }
}