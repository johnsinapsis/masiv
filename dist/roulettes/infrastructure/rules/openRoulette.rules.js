"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenRouletteRules = void 0;
var existRoulette_condition_1 = require("./conditions/existRoulette.condition");
var openRoulette_condition_1 = require("./conditions/openRoulette.condition");
var OpenRouletteRules = /** @class */ (function () {
    function OpenRouletteRules() {
        this.type = "OpenRouletteRules";
        this.existRoulettecondition = new existRoulette_condition_1.ExistRouletteCondition();
        this.openRouletteCondition = new openRoulette_condition_1.OpenRouletteCondition();
    }
    OpenRouletteRules.prototype.exec = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.existRoulettecondition.exec(id)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.openRouletteCondition.exec(id)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return OpenRouletteRules;
}());
exports.OpenRouletteRules = OpenRouletteRules;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlblJvdWxldHRlLnJ1bGVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL3JvdWxldHRlcy9pbmZyYXN0cnVjdHVyZS9ydWxlcy9vcGVuUm91bGV0dGUucnVsZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0ZBQThFO0FBQzlFLDhFQUE0RTtBQUU1RTtJQUtJO1FBQ0ksSUFBSSxDQUFDLElBQUksR0FBRyxtQkFBbUIsQ0FBQTtRQUMvQixJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxnREFBc0IsRUFBRSxDQUFBO1FBQzFELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLDhDQUFxQixFQUFFLENBQUE7SUFDNUQsQ0FBQztJQUVZLGdDQUFJLEdBQWpCLFVBQWtCLEVBQVU7Ozs7NEJBQ3hCLHFCQUFNLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUE7O3dCQUExQyxTQUEwQyxDQUFBO3dCQUMxQyxxQkFBTSxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFBOzt3QkFBekMsU0FBeUMsQ0FBQTs7Ozs7S0FDNUM7SUFDTCx3QkFBQztBQUFELENBQUMsQUFmRCxJQWVDO0FBZlksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUnVsZXNCdXNzaW5lc3MgfSBmcm9tIFwiLi4vLi4vLi4vc2hhcmVkL2FwcGxpY2F0aW9uL2J1c2luZXNzL3J1bGVzLmJ1c3NpbmVzc1wiO1xyXG5pbXBvcnQgeyBFeGlzdFJvdWxldHRlQ29uZGl0aW9uIH0gZnJvbSBcIi4vY29uZGl0aW9ucy9leGlzdFJvdWxldHRlLmNvbmRpdGlvblwiO1xyXG5pbXBvcnQgeyBPcGVuUm91bGV0dGVDb25kaXRpb24gfSBmcm9tIFwiLi9jb25kaXRpb25zL29wZW5Sb3VsZXR0ZS5jb25kaXRpb25cIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBPcGVuUm91bGV0dGVSdWxlcyBpbXBsZW1lbnRzIFJ1bGVzQnVzc2luZXNzIHtcclxuICAgIHR5cGU6IHN0cmluZztcclxuICAgIHByaXZhdGUgZXhpc3RSb3VsZXR0ZWNvbmRpdGlvblxyXG4gICAgcHJpdmF0ZSBvcGVuUm91bGV0dGVDb25kaXRpb25cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcigpe1xyXG4gICAgICAgIHRoaXMudHlwZSA9IFwiT3BlblJvdWxldHRlUnVsZXNcIlxyXG4gICAgICAgIHRoaXMuZXhpc3RSb3VsZXR0ZWNvbmRpdGlvbiA9IG5ldyBFeGlzdFJvdWxldHRlQ29uZGl0aW9uKClcclxuICAgICAgICB0aGlzLm9wZW5Sb3VsZXR0ZUNvbmRpdGlvbiA9IG5ldyBPcGVuUm91bGV0dGVDb25kaXRpb24oKVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBwdWJsaWMgYXN5bmMgZXhlYyhpZDogc3RyaW5nKSB7XHJcbiAgICAgICAgYXdhaXQgdGhpcy5leGlzdFJvdWxldHRlY29uZGl0aW9uLmV4ZWMoaWQpXHJcbiAgICAgICAgYXdhaXQgdGhpcy5vcGVuUm91bGV0dGVDb25kaXRpb24uZXhlYyhpZClcclxuICAgIH1cclxufSJdfQ==