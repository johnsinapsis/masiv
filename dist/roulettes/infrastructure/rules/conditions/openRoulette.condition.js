"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenRouletteCondition = void 0;
var base_condition_1 = require("../../../../shared/infrastructure/rules/base.condition");
var roulette_persistence_1 = require("../../persistences/roulette.persistence");
var OpenRouletteCondition = /** @class */ (function (_super) {
    __extends(OpenRouletteCondition, _super);
    function OpenRouletteCondition(error) {
        var _this = _super.call(this) || this;
        _this.error = "La ruleta ya se encuentra abierta";
        _this.type = "OpenRouletteCondition";
        _this.dao = new roulette_persistence_1.RoulettePersistence();
        if (error)
            _this.error = error;
        return _this;
    }
    OpenRouletteCondition.prototype.isOpenRoulette = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var roulette;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.dao.getRouletteById(id)];
                    case 1:
                        roulette = _a.sent();
                        if (roulette.status === "Open")
                            return [2 /*return*/, true];
                        return [2 /*return*/, false];
                }
            });
        });
    };
    OpenRouletteCondition.prototype.exec = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.isOpenRoulette(id)];
                    case 1:
                        if (_a.sent())
                            this.throwBusiness(this.error);
                        return [2 /*return*/];
                }
            });
        });
    };
    return OpenRouletteCondition;
}(base_condition_1.Condition));
exports.OpenRouletteCondition = OpenRouletteCondition;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlblJvdWxldHRlLmNvbmRpdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9yb3VsZXR0ZXMvaW5mcmFzdHJ1Y3R1cmUvcnVsZXMvY29uZGl0aW9ucy9vcGVuUm91bGV0dGUuY29uZGl0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLHlGQUFtRjtBQUVuRixnRkFBOEU7QUFFOUU7SUFBMkMseUNBQVM7SUFJaEQsK0JBQVksS0FBTTtRQUFsQixZQUNJLGlCQUFPLFNBS1Y7UUFSTyxXQUFLLEdBQUcsbUNBQW1DLENBQUE7UUFJL0MsS0FBSSxDQUFDLElBQUksR0FBRyx1QkFBdUIsQ0FBQTtRQUNuQyxLQUFJLENBQUMsR0FBRyxHQUFHLElBQUksMENBQW1CLEVBQUUsQ0FBQTtRQUNwQyxJQUFHLEtBQUs7WUFDSixLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQTs7SUFDMUIsQ0FBQztJQUVLLDhDQUFjLEdBQXBCLFVBQXFCLEVBQVM7Ozs7OzRCQUNGLHFCQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxFQUFBOzt3QkFBdEQsUUFBUSxHQUFZLFNBQWtDO3dCQUMxRCxJQUFHLFFBQVEsQ0FBQyxNQUFNLEtBQUcsTUFBTTs0QkFDdkIsc0JBQU8sSUFBSSxFQUFBO3dCQUVmLHNCQUFPLEtBQUssRUFBQTs7OztLQUNmO0lBRVksb0NBQUksR0FBakIsVUFBa0IsRUFBVTs7Ozs0QkFDckIscUJBQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsRUFBQTs7d0JBQWhDLElBQUcsU0FBNkI7NEJBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBOzs7OztLQUNyQztJQUNMLDRCQUFDO0FBQUQsQ0FBQyxBQXhCRCxDQUEyQywwQkFBUyxHQXdCbkQ7QUF4Qlksc0RBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUnVsZXNCdXNzaW5lc3MgfSBmcm9tIFwiLi4vLi4vLi4vLi4vc2hhcmVkL2FwcGxpY2F0aW9uL2J1c2luZXNzL3J1bGVzLmJ1c3NpbmVzc1wiO1xyXG5pbXBvcnQgeyBPcGVuUm91bGV0dGVCdXNpbmVzc1J1bGUgfSBmcm9tIFwiLi4vLi4vLi4vYXBwbGljYXRpb24vYnVzaW5lc3MucnVsZXMvb3BlblJvdWxldHRlLmJ1c2luZXNzLnJ1bGVcIjtcclxuaW1wb3J0IHsgQ29uZGl0aW9uIH0gZnJvbSBcIi4uLy4uLy4uLy4uL3NoYXJlZC9pbmZyYXN0cnVjdHVyZS9ydWxlcy9iYXNlLmNvbmRpdGlvblwiO1xyXG5pbXBvcnQgeyBSb3VsZXR0ZSB9IGZyb20gXCIuLi8uLi8uLi9kb21haW4vUm91bGV0dGVcIjtcclxuaW1wb3J0IHsgUm91bGV0dGVQZXJzaXN0ZW5jZSB9IGZyb20gXCIuLi8uLi9wZXJzaXN0ZW5jZXMvcm91bGV0dGUucGVyc2lzdGVuY2VcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBPcGVuUm91bGV0dGVDb25kaXRpb24gZXh0ZW5kcyBDb25kaXRpb24gaW1wbGVtZW50cyBSdWxlc0J1c3NpbmVzcywgT3BlblJvdWxldHRlQnVzaW5lc3NSdWxlIHtcclxuICAgIHByaXZhdGUgZGFvXHJcbiAgICBwcml2YXRlIGVycm9yID0gXCJMYSBydWxldGEgeWEgc2UgZW5jdWVudHJhIGFiaWVydGFcIlxyXG5cclxuICAgIGNvbnN0cnVjdG9yKGVycm9yPyl7XHJcbiAgICAgICAgc3VwZXIoKVxyXG4gICAgICAgIHRoaXMudHlwZSA9IFwiT3BlblJvdWxldHRlQ29uZGl0aW9uXCJcclxuICAgICAgICB0aGlzLmRhbyA9IG5ldyBSb3VsZXR0ZVBlcnNpc3RlbmNlKClcclxuICAgICAgICBpZihlcnJvcilcclxuICAgICAgICAgICAgdGhpcy5lcnJvciA9IGVycm9yXHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgaXNPcGVuUm91bGV0dGUoaWQ6c3RyaW5nKTogUHJvbWlzZTxib29sZWFuPiB7XHJcbiAgICAgICAgbGV0IHJvdWxldHRlOlJvdWxldHRlID0gYXdhaXQgdGhpcy5kYW8uZ2V0Um91bGV0dGVCeUlkKGlkKVxyXG4gICAgICAgIGlmKHJvdWxldHRlLnN0YXR1cz09PVwiT3BlblwiKVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZVxyXG5cclxuICAgICAgICByZXR1cm4gZmFsc2VcclxuICAgIH1cclxuICAgIFxyXG4gICAgcHVibGljIGFzeW5jIGV4ZWMoaWQ6IHN0cmluZykge1xyXG4gICAgICAgIGlmKGF3YWl0IHRoaXMuaXNPcGVuUm91bGV0dGUoaWQpKVxyXG4gICAgICAgICAgICB0aGlzLnRocm93QnVzaW5lc3ModGhpcy5lcnJvcilcclxuICAgIH1cclxufSJdfQ==