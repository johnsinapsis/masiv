import { Controller } from "../../../shared/infrastructure/controllers/controller.base";
import { logger } from "../../../helpers/default.logger";
import { BaseErrorHandler } from "../../../shared/infrastructure/handlerErrors/base.error.handler";
import { ConfigControllerIdentity } from "../../../shared/infrastructure/controllers/config.controller.identity";
import { RouletteDto } from "../../application/dto/roulette.dto";
import { CreateRouletteMapper } from "../mappers/createRoulette.mapper";
import { RoulettePersistence } from "../persistences/roulette.persistence";

export class CreateRouteController extends Controller implements ConfigControllerIdentity{
    private mapper = new CreateRouletteMapper()
    private service = new RoulettePersistence()

    constructor(){
        super()
    }

    getConfigId(){
        return 'createRoulette'
    }

    async postCreateRoulette(req,res){
        try{
            let dto:RouletteDto = this.mapper.toDto(req.body)
            dto = await this.service.createRoulette(dto)
            let response = this.mapper.toResponse(dto)
            
            return res.json(response)
        }
        catch(e){
            logger.error(e)
            return BaseErrorHandler.handle(res,e)
        }
    }
}