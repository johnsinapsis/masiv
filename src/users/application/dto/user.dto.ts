import { Dto } from "../../../shared/application/base.dto";

export class UserDto extends Dto{
    private nickname:string
    
    constructor(){
        super()
    }

    public getNickname(): string {
        return this.nickname;
    }

    public setNickname(nickname: string): void {
        this.nickname = nickname;
    }
}