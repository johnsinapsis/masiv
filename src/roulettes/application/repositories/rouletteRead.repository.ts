import { Roulette } from "../../domain/Roulette";

export interface RouletteReadRepository{
    getRouletteById(id:string):Promise <Roulette>
    getAllRoulettes(): Promise <Array<Roulette>>
}