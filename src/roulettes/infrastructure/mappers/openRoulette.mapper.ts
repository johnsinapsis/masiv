import { Dto } from "../../../shared/application/base.dto";
import { CommonResponse } from "../../../shared/infrastructure/common.response";
import { OpenRouletteResponse } from "../responses/operRoulette.response";
import { ResponseMapper } from "../../../shared/application/mappers/response.mapper";

export class OpenRouletteMapper implements ResponseMapper{
    toResponse(dto: Dto): CommonResponse {
        let response = new OpenRouletteResponse()
        return response
    }
}
