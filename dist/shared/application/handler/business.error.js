"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessError = void 0;
var base_error_1 = require("./base.error");
var BusinessError = /** @class */ (function (_super) {
    __extends(BusinessError, _super);
    function BusinessError(type, response) {
        if (type === void 0) { type = ""; }
        var _this = _super.call(this, type, response) || this;
        _this.type = type;
        _this.response = response;
        return _this;
    }
    return BusinessError;
}(base_error_1.BaseError));
exports.BusinessError = BusinessError;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVzaW5lc3MuZXJyb3IuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvc2hhcmVkL2FwcGxpY2F0aW9uL2hhbmRsZXIvYnVzaW5lc3MuZXJyb3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMkNBQXlDO0FBRXpDO0lBQW1DLGlDQUFTO0lBQ3hDLHVCQUFtQixJQUFPLEVBQVEsUUFBUTtRQUF2QixxQkFBQSxFQUFBLFNBQU87UUFBMUIsWUFDSSxrQkFBTSxJQUFJLEVBQUMsUUFBUSxDQUFDLFNBQ3ZCO1FBRmtCLFVBQUksR0FBSixJQUFJLENBQUc7UUFBUSxjQUFRLEdBQVIsUUFBUSxDQUFBOztJQUUxQyxDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQUFDLEFBSkQsQ0FBbUMsc0JBQVMsR0FJM0M7QUFKWSxzQ0FBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJhc2VFcnJvciB9IGZyb20gXCIuL2Jhc2UuZXJyb3JcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBCdXNpbmVzc0Vycm9yIGV4dGVuZHMgQmFzZUVycm9ye1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHR5cGU9XCJcIixwdWJsaWMgcmVzcG9uc2Upe1xyXG4gICAgICAgIHN1cGVyKHR5cGUscmVzcG9uc2UpICBcclxuICAgIH1cclxufSJdfQ==