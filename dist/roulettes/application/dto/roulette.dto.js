"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.RouletteDto = void 0;
var base_dto_1 = require("../../../shared/application/base.dto");
var RouletteDto = /** @class */ (function (_super) {
    __extends(RouletteDto, _super);
    function RouletteDto() {
        return _super.call(this) || this;
    }
    RouletteDto.prototype.getDate = function () {
        return this.date;
    };
    RouletteDto.prototype.setDate = function (date) {
        this.date = date;
    };
    RouletteDto.prototype.getStatus = function () {
        return this.status;
    };
    RouletteDto.prototype.setStatus = function (status) {
        this.status = status;
    };
    return RouletteDto;
}(base_dto_1.Dto));
exports.RouletteDto = RouletteDto;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91bGV0dGUuZHRvLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL3JvdWxldHRlcy9hcHBsaWNhdGlvbi9kdG8vcm91bGV0dGUuZHRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGlFQUEyRDtBQUczRDtJQUFpQywrQkFBRztJQUloQztlQUNJLGlCQUFPO0lBQ1gsQ0FBQztJQUVNLDZCQUFPLEdBQWQ7UUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVNLDZCQUFPLEdBQWQsVUFBZSxJQUFZO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0lBQ3JCLENBQUM7SUFFTSwrQkFBUyxHQUFoQjtRQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRU0sK0JBQVMsR0FBaEIsVUFBaUIsTUFBc0I7UUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDekIsQ0FBQztJQUNMLGtCQUFDO0FBQUQsQ0FBQyxBQXZCRCxDQUFpQyxjQUFHLEdBdUJuQztBQXZCWSxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IER0byB9IGZyb20gXCIuLi8uLi8uLi9zaGFyZWQvYXBwbGljYXRpb24vYmFzZS5kdG9cIjtcclxuaW1wb3J0IHsgU3RhdHVzUm91bGV0dGUgfSBmcm9tIFwiLi4vLi4vZG9tYWluL1N0YXR1c1JvdWxldHRlXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgUm91bGV0dGVEdG8gZXh0ZW5kcyBEdG97XHJcbiAgICBwcml2YXRlIGRhdGU6c3RyaW5nXHJcbiAgICBwcml2YXRlIHN0YXR1czpTdGF0dXNSb3VsZXR0ZVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKCl7XHJcbiAgICAgICAgc3VwZXIoKVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXREYXRlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0ZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0RGF0ZShkYXRlOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmRhdGUgPSBkYXRlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRTdGF0dXMoKTogU3RhdHVzUm91bGV0dGUge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnN0YXR1cztcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0U3RhdHVzKHN0YXR1czogU3RhdHVzUm91bGV0dGUpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnN0YXR1cyA9IHN0YXR1cztcclxuICAgIH1cclxufSJdfQ==