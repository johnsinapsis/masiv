import { ExpressServer } from "./server/server";
import sourceMapSupport from 'source-map-support'
sourceMapSupport.install()
class Init {
    server
    rMng

    constructor(){
        this.server = new ExpressServer()
        this.rMng = this.server.getRouteManager()
    }
    
    registerRoutes(){
        this.rMng.bindAny('CreateRouteController')
        this.rMng.bindAny('OpenRouletteController')
        this.rMng.bindAny('BetRouletteController')
        this.rMng.bindAny('CloseRouletteController')
        this.rMng.bindAny('GetRoulettesController')
    }
}

let init = new Init()
init.registerRoutes()
init.server.startup()