import { CommonResponse } from "../../../shared/infrastructure/common.response";

export class BetResponse extends CommonResponse{
    constructor(){
        super()
        this.type = "BetResponse"
        this.response.description = "Se ha registrado la apuesta de manera correcta"
    }
}