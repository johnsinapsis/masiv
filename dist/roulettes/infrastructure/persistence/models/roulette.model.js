"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.RouletteModel = void 0;
var Model_1 = require("../../../../shared/infrastructure/Model");
var RouletteModel = /** @class */ (function (_super) {
    __extends(RouletteModel, _super);
    function RouletteModel() {
        var _this = _super.call(this) || this;
        _this.name = 'roulette';
        _this.collection = {
            _id: '',
            date: ''
        };
        return _this;
    }
    RouletteModel.prototype.mapCollection = function (data) {
        if (data.getId())
            this.collection._id = data.getId();
        if (data.getDate())
            this.collection.date = data.getDate();
        return this.collection;
    };
    return RouletteModel;
}(Model_1.Model));
exports.RouletteModel = RouletteModel;
