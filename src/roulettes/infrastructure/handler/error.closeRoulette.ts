import { BaseErrorController } from "../../../shared/infrastructure/controllers/base.error.controller";

export class ErrorCloseRoulette extends BaseErrorController{
    constructor(){
        super()
        this.type = "ErrorCloseRoulette"
        this.stack = new Error().stack
    }
}