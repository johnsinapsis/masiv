"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExistRouletteCondition = void 0;
var base_condition_1 = require("../../../../shared/infrastructure/rules/base.condition");
var roulette_persistence_1 = require("../../persistences/roulette.persistence");
var ExistRouletteCondition = /** @class */ (function (_super) {
    __extends(ExistRouletteCondition, _super);
    function ExistRouletteCondition() {
        var _this = _super.call(this) || this;
        _this.type = "ExistRouletteCondition";
        _this.dao = new roulette_persistence_1.RoulettePersistence();
        return _this;
    }
    ExistRouletteCondition.prototype.rouletteExist = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var roulette;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.dao.getRouletteById(id)];
                    case 1:
                        roulette = _a.sent();
                        console.log(roulette);
                        if (!roulette)
                            return [2 /*return*/, false];
                        return [2 /*return*/, true];
                }
            });
        });
    };
    ExistRouletteCondition.prototype.exec = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.rouletteExist(id)];
                    case 1:
                        if (!(_a.sent()))
                            this.throwBusiness("El id de la ruleta no existe");
                        return [2 /*return*/];
                }
            });
        });
    };
    return ExistRouletteCondition;
}(base_condition_1.Condition));
exports.ExistRouletteCondition = ExistRouletteCondition;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhpc3RSb3VsZXR0ZS5jb25kaXRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvcm91bGV0dGVzL2luZnJhc3RydWN0dXJlL3J1bGVzL2NvbmRpdGlvbnMvZXhpc3RSb3VsZXR0ZS5jb25kaXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEseUZBQW1GO0FBRW5GLGdGQUE4RTtBQUc5RTtJQUE0QywwQ0FBUztJQUdqRDtRQUFBLFlBQ0ksaUJBQU8sU0FHVjtRQUZHLEtBQUksQ0FBQyxJQUFJLEdBQUcsd0JBQXdCLENBQUE7UUFDcEMsS0FBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLDBDQUFtQixFQUFFLENBQUE7O0lBQ3hDLENBQUM7SUFFWSw4Q0FBYSxHQUExQixVQUEyQixFQUFTOzs7Ozs0QkFDUixxQkFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsRUFBQTs7d0JBQXRELFFBQVEsR0FBWSxTQUFrQzt3QkFDMUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDdEIsSUFBRyxDQUFDLFFBQVE7NEJBQ1Isc0JBQU8sS0FBSyxFQUFBO3dCQUVoQixzQkFBTyxJQUFJLEVBQUE7Ozs7S0FDZDtJQUVZLHFDQUFJLEdBQWpCLFVBQWtCLEVBQVM7Ozs7NEJBQ25CLHFCQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEVBQUE7O3dCQUFoQyxJQUFHLENBQUMsQ0FBQSxTQUE0QixDQUFBOzRCQUM1QixJQUFJLENBQUMsYUFBYSxDQUFDLDhCQUE4QixDQUFDLENBQUE7Ozs7O0tBQ3pEO0lBQ0wsNkJBQUM7QUFBRCxDQUFDLEFBdEJELENBQTRDLDBCQUFTLEdBc0JwRDtBQXRCWSx3REFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSdWxlc0J1c3NpbmVzcyB9IGZyb20gXCIuLi8uLi8uLi8uLi9zaGFyZWQvYXBwbGljYXRpb24vYnVzaW5lc3MvcnVsZXMuYnVzc2luZXNzXCI7XHJcbmltcG9ydCB7IEV4aXN0Um91bGV0dGVCdXNpbmVzc1J1bGUgfSBmcm9tIFwiLi4vLi4vLi4vYXBwbGljYXRpb24vYnVzaW5lc3MucnVsZXMvZXhpc3RSb3VsZXR0ZS5idXNpbmVzcy5ydWxlXCI7XHJcbmltcG9ydCB7IENvbmRpdGlvbiB9IGZyb20gXCIuLi8uLi8uLi8uLi9zaGFyZWQvaW5mcmFzdHJ1Y3R1cmUvcnVsZXMvYmFzZS5jb25kaXRpb25cIjtcclxuaW1wb3J0IHsgUm91bGV0dGUgfSBmcm9tIFwiLi4vLi4vLi4vZG9tYWluL1JvdWxldHRlXCI7XHJcbmltcG9ydCB7IFJvdWxldHRlUGVyc2lzdGVuY2UgfSBmcm9tIFwiLi4vLi4vcGVyc2lzdGVuY2VzL3JvdWxldHRlLnBlcnNpc3RlbmNlXCI7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIEV4aXN0Um91bGV0dGVDb25kaXRpb24gZXh0ZW5kcyBDb25kaXRpb24gaW1wbGVtZW50cyBSdWxlc0J1c3NpbmVzcywgRXhpc3RSb3VsZXR0ZUJ1c2luZXNzUnVsZXtcclxuICAgIHByaXZhdGUgZGFvXHJcblxyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy50eXBlID0gXCJFeGlzdFJvdWxldHRlQ29uZGl0aW9uXCJcclxuICAgICAgICB0aGlzLmRhbyA9IG5ldyBSb3VsZXR0ZVBlcnNpc3RlbmNlKClcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgcm91bGV0dGVFeGlzdChpZDpzdHJpbmcpe1xyXG4gICAgICAgIGxldCByb3VsZXR0ZTpSb3VsZXR0ZSA9IGF3YWl0IHRoaXMuZGFvLmdldFJvdWxldHRlQnlJZChpZClcclxuICAgICAgICBjb25zb2xlLmxvZyhyb3VsZXR0ZSk7XHJcbiAgICAgICAgaWYoIXJvdWxldHRlKVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2VcclxuXHJcbiAgICAgICAgcmV0dXJuIHRydWVcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgZXhlYyhpZDpzdHJpbmcpe1xyXG4gICAgICAgIGlmKCFhd2FpdCB0aGlzLnJvdWxldHRlRXhpc3QoaWQpKVxyXG4gICAgICAgICAgICB0aGlzLnRocm93QnVzaW5lc3MoXCJFbCBpZCBkZSBsYSBydWxldGEgbm8gZXhpc3RlXCIpXHJcbiAgICB9XHJcbn0iXX0=