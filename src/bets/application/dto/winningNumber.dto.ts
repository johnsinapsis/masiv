import { Dto } from "../../../shared/application/base.dto";
import { Color } from "../../domain/Color";

export class WinningNumberDto extends Dto {
    private winningNumber:number
    private winningColor:Color

    public getWinnignNumber(): number {
        return this.winningNumber;
    }

    public setWinningNumber(winningNumber: number): void {
        this.winningNumber = winningNumber;
        winningNumber % 2 ? this.setWinningColor("Black") : this.setWinningColor("Red")
    }

    public getWinnigColor(): Color {
        return this.winningColor;
    }

    private setWinningColor(winnigColor: Color): void {
        this.winningColor = winnigColor;
    }
}