export class EnvConfig {
    static pathSeparator() {
        let os = process.platform
        if (os.startsWith("win")) {
            return "\\"
        } else {
            return "/"
        }
    }
}