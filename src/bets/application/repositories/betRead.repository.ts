import { Bet } from "../../domain/Bet";

export interface BetReadRepository{
    getBetsByRouletteId(id:string): Promise <Array <Bet>>
}