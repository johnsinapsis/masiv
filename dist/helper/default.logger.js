"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
var log4js_1 = require("log4js");
var config_1 = __importDefault(require("../config/config"));
// appenders
(0, log4js_1.configure)({
    appenders: {
        console: { type: 'stdout', layout: { type: 'colored' } },
        dateFile: {
            type: 'dateFile',
            filename: config_1.default.logging.logDir + "/" + config_1.default.logging.logFile,
            layout: { type: 'basic' },
            compress: true,
            daysToKeep: 14,
            keepFileExt: true
        }
    },
    categories: {
        default: { appenders: ['console', 'dateFile'], level: config_1.default.logging.level }
    }
});
// fetch logger and export
exports.logger = (0, log4js_1.getLogger)();
