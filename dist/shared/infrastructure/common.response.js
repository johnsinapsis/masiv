"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommonResponse = void 0;
var constants_1 = require("../../config/constants");
var CommonResponse = /** @class */ (function () {
    function CommonResponse() {
        this.type = "";
        this.response = { result: constants_1.RESULT_SUCCESS, description: '' };
    }
    return CommonResponse;
}());
exports.CommonResponse = CommonResponse;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLnJlc3BvbnNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL3NoYXJlZC9pbmZyYXN0cnVjdHVyZS9jb21tb24ucmVzcG9uc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsb0RBQXdEO0FBRXhEO0lBQUE7UUFDVyxTQUFJLEdBQUcsRUFBRSxDQUFBO1FBQ1QsYUFBUSxHQUFHLEVBQUMsTUFBTSxFQUFDLDBCQUFjLEVBQUMsV0FBVyxFQUFDLEVBQUUsRUFBQyxDQUFBO0lBQzVELENBQUM7SUFBRCxxQkFBQztBQUFELENBQUMsQUFIRCxJQUdDO0FBSFksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSRVNVTFRfU1VDQ0VTUyB9IGZyb20gXCIuLi8uLi9jb25maWcvY29uc3RhbnRzXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgQ29tbW9uUmVzcG9uc2V7XHJcbiAgICBwdWJsaWMgdHlwZSA9IFwiXCJcclxuICAgIHB1YmxpYyByZXNwb25zZSA9IHtyZXN1bHQ6UkVTVUxUX1NVQ0NFU1MsZGVzY3JpcHRpb246Jyd9XHJcbn0iXX0=