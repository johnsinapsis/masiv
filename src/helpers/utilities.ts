import { UNDEF } from "../config/constants" 

export class Utils{
    static isEmpty(text) {
        
        return (Utils.isUndfNull(text) || text.toString() == "")
    }

    static isUndfNull(text) {
        
        return (typeof text == UNDEF || text == null)
    }

    static isNumber(text) {
        if (!Utils.isEmpty(text)) {
            
            return !isNaN(text)
        }
        
        return false
    }
    
    static isSuccessRange(dateStart,dateEnd){
        let f1 = new Date(dateStart)
        let f2 = new Date(dateEnd)
        
        return !(f2<f1)
    }

    static setLocalDate(date){
        let z = date.getTimezoneOffset() * 60 * 1000
        let tLocal = date - z
        let localD = new Date(tLocal)
        
        return localD
    }

    static isBson(id){
        if (!id.match(/^[0-9a-fA-F]{24}$/))
            return false
        return true
    }
}