import { CommonResponse } from "../../../shared/infrastructure/common.response";

export class OpenRouletteResponse extends CommonResponse{
    constructor(){
        super()
        this.type = "OpenRouletteResponse"
        this.response.description = "Se ha abierto la ruleta correctamente"
    }
}