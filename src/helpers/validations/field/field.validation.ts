import { INV_FIELDS,UNDEF } from "../../../config/constants" 

export class FieldValidator{
    public jpExpr
    public failMsg
    public msg

    constructor(jpExpr, failMsg){
        this.jpExpr = jpExpr
        this.failMsg = failMsg
    }

    evaluate(req, res){
        this.msg = this.failMsg
        let isValid = this.isValid(req, res)
        if(!isValid)
            this.addErrorResponse(res, this.msg)
        return isValid
    }

    isValid(req, res){
        return true
    }
    
    addErrorResponse (res , msg){
        if( typeof res[INV_FIELDS] === UNDEF ){
            res.invalid_fields = []
        }
        res.invalid_fields.push(msg)
    }
} 
