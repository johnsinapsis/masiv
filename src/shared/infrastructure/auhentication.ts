import jwt from 'jsonwebtoken'
import config  from '../../config/config'
import { UserDto } from '../../users/application/dto/user.dto'
import { BusinessError } from '../application/handler/business.error'

export class Authentication{
    private apiKey = config.auth.token
    private time = config.auth.time+'d'
    private type = "ErrorAuthentication"

    public sign(user:UserDto){
        return jwt.sign({user},this.apiKey,{expiresIn:this.time});
    }

    public validateToken(token,res){
        let decoded =jwt.verify(token,this.apiKey,(error,decoded)=>{
            if(error){
                this.throwBusiness("Token de autenticación no válido")
            }
            return decoded
        })
        return decoded
    }

    public throwBusiness(msg){
        let response = {description: msg}
        throw new BusinessError(this.type,response)
    }
}