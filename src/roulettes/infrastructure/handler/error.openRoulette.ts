import { BaseErrorController } from "../../../shared/infrastructure/controllers/base.error.controller";

export class ErrorOpenRoulette extends BaseErrorController{
    constructor(){
        super()
        this.type = "ErrorOpenRoulette"
        this.stack = new Error().stack
    }
}