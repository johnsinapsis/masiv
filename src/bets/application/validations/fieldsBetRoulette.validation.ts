import { BaseValidation } from "../../../shared/application/validation/base.validation";
import { START_NUMBER_BET,END_NUMBER_BET,MAX_MOUNT_BET } from "../../../config/constants";

export class FieldsBetRouletteValidation extends BaseValidation{
    constructor(){
        super()
        let emptyId = this.fieldValidationFactory.createInstance('empty','$.rouletteId','El id de ruleta es requerido')
        let emptyMount = this.fieldValidationFactory.createInstance('empty','$.mount','El campo mount es requerido')
        let numberMount = this.fieldValidationFactory.createInstance('number','$.mount','El campo mount debe ser número')
        let msg = `El campo mount debe ser mayor a 0 y menor o igual a ${MAX_MOUNT_BET} USD`
        let rangeMount = this.fieldValidationFactory.createInstance('mount','$.mount',msg)
        let betType = this.fieldValidationFactory.createInstance('bet-type','$','el campo number o color es requerido. Solo es válido uno de los dos')
        let numberNumber = this.fieldValidationFactory.createInstance('number','$.number','El campo number debe ser número')
        msg = `Número no válido en el campo number. Rango válido entre ${START_NUMBER_BET} y ${END_NUMBER_BET}`
        let betNumber = this.fieldValidationFactory.createInstance('number-bet','$.number',msg)
        let betColor = this.fieldValidationFactory.createInstance('color','$.color','valor del campo color es inválido. Valores posibles {Red,Black}')
        this.addValidator(emptyId)
        this.addValidator(emptyMount)
        this.addValidator(numberMount)
        this.addValidator(rangeMount)
        this.addValidator(betType)
        this.addValidator(numberNumber)
        this.addValidator(betNumber)
        this.addValidator(betColor)
    }
}