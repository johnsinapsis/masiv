import { FieldValidator } from "./field/field.validation";
import jp from 'jsonpath'

export class BetTypeValidation extends FieldValidator{
    constructor(jpExpr, failMsg){
        super(jpExpr, failMsg)
        this.jpExpr = jpExpr
        this.failMsg = failMsg
    }

    isValid(req, res){
        let color = jp.query(req.body, this.jpExpr)[0].color
        let number = jp.query(req.body, this.jpExpr)[0].number
        if((!color && !number) || (color && number))
            return false
        return true;
    }
}