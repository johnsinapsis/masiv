"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorOpenRoulette = void 0;
var base_error_controller_1 = require("../../../shared/infrastructure/controllers/base.error.controller");
var ErrorOpenRoulette = /** @class */ (function (_super) {
    __extends(ErrorOpenRoulette, _super);
    function ErrorOpenRoulette() {
        var _this = _super.call(this) || this;
        _this.type = "ErrorOpenRoulette";
        _this.stack = new Error().stack;
        return _this;
    }
    return ErrorOpenRoulette;
}(base_error_controller_1.BaseErrorController));
exports.ErrorOpenRoulette = ErrorOpenRoulette;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3Iub3BlblJvdWxldHRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL3JvdWxldHRlcy9pbmZyYXN0cnVjdHVyZS9oYW5kbGVyL2Vycm9yLm9wZW5Sb3VsZXR0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwwR0FBdUc7QUFFdkc7SUFBdUMscUNBQW1CO0lBQ3REO1FBQUEsWUFDSSxpQkFBTyxTQUdWO1FBRkcsS0FBSSxDQUFDLElBQUksR0FBRyxtQkFBbUIsQ0FBQTtRQUMvQixLQUFJLENBQUMsS0FBSyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFBOztJQUNsQyxDQUFDO0lBQ0wsd0JBQUM7QUFBRCxDQUFDLEFBTkQsQ0FBdUMsMkNBQW1CLEdBTXpEO0FBTlksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmFzZUVycm9yQ29udHJvbGxlciB9IGZyb20gXCIuLi8uLi8uLi9zaGFyZWQvaW5mcmFzdHJ1Y3R1cmUvY29udHJvbGxlcnMvYmFzZS5lcnJvci5jb250cm9sbGVyXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgRXJyb3JPcGVuUm91bGV0dGUgZXh0ZW5kcyBCYXNlRXJyb3JDb250cm9sbGVye1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy50eXBlID0gXCJFcnJvck9wZW5Sb3VsZXR0ZVwiXHJcbiAgICAgICAgdGhpcy5zdGFjayA9IG5ldyBFcnJvcigpLnN0YWNrXHJcbiAgICB9XHJcbn0iXX0=