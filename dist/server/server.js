"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExpressServer = void 0;
var express_1 = __importStar(require("express"));
var config_js_1 = __importDefault(require("../config/config.js"));
var route_js_1 = require("../route/route.js");
var default_logger_js_1 = require("../helpers/default.logger.js");
var ExpressServer = /** @class */ (function () {
    function ExpressServer() {
        this.app = (0, express_1.default)();
        this.router = (0, express_1.Router)();
        this.routeManager = new route_js_1.RouteManager(this.router);
        this.app.use((0, express_1.urlencoded)({ extended: false }));
        this.app.use(express_1.default.json());
        this.app.use(function (req, res, next) {
            var allowedOrigins = ['http://192.168.1.6:3000', 'http://localhost:3000'];
            var Host = req.get('Origin');
            if (Host) {
                allowedOrigins.forEach(function (val, key) {
                    //console.log("linea 19",Host);
                    if (Host.indexOf(val) > -1) {
                        res.setHeader('Access-Control-Allow-Origin', Host);
                    }
                });
            }
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
            next();
        });
        this.app.use(this.router);
    }
    ExpressServer.prototype.startup = function () {
        var port = config_js_1.default.port;
        this.app.listen(port, function () {
            default_logger_js_1.logger.info('Servidor web escuchando en el puerto ' + port);
        });
    };
    ExpressServer.prototype.getApp = function () {
        return this.app;
    };
    ExpressServer.prototype.getRouter = function () {
        return this.router;
    };
    ExpressServer.prototype.getRouteManager = function () {
        return this.routeManager;
    };
    return ExpressServer;
}());
exports.ExpressServer = ExpressServer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL3NlcnZlci9zZXJ2ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGlEQUF5RDtBQUN6RCxrRUFBd0M7QUFDeEMsOENBQWdEO0FBQ2hELGtFQUFzRDtBQUV0RDtJQUtJO1FBSkEsUUFBRyxHQUFHLElBQUEsaUJBQU8sR0FBRSxDQUFDO1FBQ2hCLFdBQU0sR0FBRyxJQUFBLGdCQUFNLEdBQUUsQ0FBQztRQUNsQixpQkFBWSxHQUFHLElBQUksdUJBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7UUFHeEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBQSxvQkFBVSxFQUFDLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxDQUFDLENBQUMsQ0FBQTtRQUMzQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxpQkFBTyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUE7UUFDNUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLElBQUk7WUFDdEIsSUFBSSxjQUFjLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO1lBQzFFLElBQUksSUFBSSxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDN0IsSUFBRyxJQUFJLEVBQUM7Z0JBQ0osY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFTLEdBQUcsRUFBRSxHQUFHO29CQUNwQywrQkFBK0I7b0JBQy9CLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBQzt3QkFDdkIsR0FBRyxDQUFDLFNBQVMsQ0FBQyw2QkFBNkIsRUFBRSxJQUFJLENBQUMsQ0FBQztxQkFDdEQ7Z0JBQ0wsQ0FBQyxDQUFDLENBQUE7YUFDTDtZQUNELEdBQUcsQ0FBQyxTQUFTLENBQUMsOEJBQThCLEVBQUMsK0JBQStCLENBQUMsQ0FBQTtZQUM3RSxJQUFJLEVBQUUsQ0FBQTtRQUNWLENBQUMsQ0FBQyxDQUFBO1FBQ0YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0lBQzdCLENBQUM7SUFFRCwrQkFBTyxHQUFQO1FBQ1csSUFBQSxJQUFJLEdBQUksbUJBQU0sS0FBVixDQUFVO1FBQ3JCLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRTtZQUNsQiwwQkFBTSxDQUFDLElBQUksQ0FBQyx1Q0FBdUMsR0FBRSxJQUFJLENBQUMsQ0FBQTtRQUM5RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw4QkFBTSxHQUFOO1FBQ0ksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFBO0lBQ25CLENBQUM7SUFFRCxpQ0FBUyxHQUFUO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFBO0lBQ3RCLENBQUM7SUFFRCx1Q0FBZSxHQUFmO1FBQ0ksT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFBO0lBQzVCLENBQUM7SUFDTCxvQkFBQztBQUFELENBQUMsQUEzQ0QsSUEyQ0M7QUEzQ1ksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZXhwcmVzcywge1JvdXRlciwgdXJsZW5jb2RlZCwganNvbn0gZnJvbSAnZXhwcmVzcydcclxuaW1wb3J0IGNvbmZpZyBmcm9tICcuLi9jb25maWcvY29uZmlnLmpzJ1xyXG5pbXBvcnQgeyBSb3V0ZU1hbmFnZXIgfSBmcm9tICcuLi9yb3V0ZS9yb3V0ZS5qcydcclxuaW1wb3J0IHsgbG9nZ2VyIH0gZnJvbSAnLi4vaGVscGVycy9kZWZhdWx0LmxvZ2dlci5qcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgRXhwcmVzc1NlcnZlcntcclxuICAgIGFwcCA9IGV4cHJlc3MoKTtcclxuICAgIHJvdXRlciA9IFJvdXRlcigpO1xyXG4gICAgcm91dGVNYW5hZ2VyID0gbmV3IFJvdXRlTWFuYWdlcih0aGlzLnJvdXRlcilcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcigpe1xyXG4gICAgICAgIHRoaXMuYXBwLnVzZSh1cmxlbmNvZGVkKHtleHRlbmRlZDogZmFsc2V9KSlcclxuICAgICAgICB0aGlzLmFwcC51c2UoZXhwcmVzcy5qc29uKCkpXHJcbiAgICAgICAgdGhpcy5hcHAudXNlKChyZXEscmVzLG5leHQpPT57XHJcbiAgICAgICAgICAgIGxldCBhbGxvd2VkT3JpZ2lucyA9IFsnaHR0cDovLzE5Mi4xNjguMS42OjMwMDAnLCAnaHR0cDovL2xvY2FsaG9zdDozMDAwJ107XHJcbiAgICAgICAgICAgIGxldCBIb3N0ID0gcmVxLmdldCgnT3JpZ2luJyk7XHJcbiAgICAgICAgICAgIGlmKEhvc3Qpe1xyXG4gICAgICAgICAgICAgICAgYWxsb3dlZE9yaWdpbnMuZm9yRWFjaChmdW5jdGlvbih2YWwsIGtleSl7XHJcbiAgICAgICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcImxpbmVhIDE5XCIsSG9zdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKEhvc3QuaW5kZXhPZih2YWwpID4gLTEpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXMuc2V0SGVhZGVyKCdBY2Nlc3MtQ29udHJvbC1BbGxvdy1PcmlnaW4nLCBIb3N0KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJlcy5zZXRIZWFkZXIoJ0FjY2Vzcy1Db250cm9sLUFsbG93LUhlYWRlcnMnLCdYLVJlcXVlc3RlZC1XaXRoLGNvbnRlbnQtdHlwZScpXHJcbiAgICAgICAgICAgIG5leHQoKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgdGhpcy5hcHAudXNlKHRoaXMucm91dGVyKVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXJ0dXAoKXtcclxuICAgICAgICBjb25zdCB7cG9ydH0gPSBjb25maWdcclxuICAgICAgICB0aGlzLmFwcC5saXN0ZW4ocG9ydCwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGxvZ2dlci5pbmZvKCdTZXJ2aWRvciB3ZWIgZXNjdWNoYW5kbyBlbiBlbCBwdWVydG8gJysgcG9ydClcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBcHAoKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcHBcclxuICAgIH1cclxuXHJcbiAgICBnZXRSb3V0ZXIoKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5yb3V0ZXJcclxuICAgIH1cclxuXHJcbiAgICBnZXRSb3V0ZU1hbmFnZXIoKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5yb3V0ZU1hbmFnZXIgICAgICAgXHJcbiAgICB9XHJcbn0iXX0=