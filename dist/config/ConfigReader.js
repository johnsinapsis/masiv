"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigReader = void 0;
var jsonpath_1 = __importDefault(require("jsonpath"));
var config_1 = __importDefault(require("./config"));
var ConfigReader = /** @class */ (function () {
    function ConfigReader() {
    }
    ConfigReader.getPathsMap = function (controllerId) {
        return jsonpath_1.default.query(config_1.default, 'ep.' + controllerId)[0];
    };
    return ConfigReader;
}());
exports.ConfigReader = ConfigReader;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnUmVhZGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbmZpZy9Db25maWdSZWFkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsc0RBQXlCO0FBQ3pCLG9EQUE2QjtBQUU3QjtJQUFBO0lBSUEsQ0FBQztJQUhVLHdCQUFXLEdBQWxCLFVBQW1CLFlBQVk7UUFDM0IsT0FBTyxrQkFBRSxDQUFDLEtBQUssQ0FBQyxnQkFBTSxFQUFFLEtBQUssR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNwRCxDQUFDO0lBQ0wsbUJBQUM7QUFBRCxDQUFDLEFBSkQsSUFJQztBQUpZLG9DQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGpwIGZyb20gJ2pzb25wYXRoJ1xyXG5pbXBvcnQgY29uZmlnIGZyb20gJy4vY29uZmlnJ1xyXG5cclxuZXhwb3J0IGNsYXNzIENvbmZpZ1JlYWRlciB7XHJcbiAgICBzdGF0aWMgZ2V0UGF0aHNNYXAoY29udHJvbGxlcklkKSB7XHJcbiAgICAgICAgcmV0dXJuIGpwLnF1ZXJ5KGNvbmZpZywgJ2VwLicgKyBjb250cm9sbGVySWQpWzBdXHJcbiAgICB9XHJcbn0iXX0=