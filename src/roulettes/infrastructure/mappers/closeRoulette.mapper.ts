import { BetDto } from "../../../bets/application/dto/bet.dto"; 
import { Bet } from "../../../bets/domain/Bet";
import { CommonResponse } from "../../../shared/infrastructure/common.response";
import { CloseRouletteResponse } from "../responses/closeRoulette.response";
import { ResponseMapper } from "../../../shared/application/mappers/response.mapper";
import { DtoMapper } from "../../../shared/application/mappers/dto.mapper"
import { Dto } from "../../../shared/application/base.dto";
import { WinningNumberDto } from "../../../bets/application/dto/winningNumber.dto";

export class CloseRouletteMapper implements DtoMapper{
    toDto(req: any): BetDto {
        let dto = new BetDto()
        if(req.id)
            dto.setId(req.id)
        if(req.date)
            dto.setDate(req.date)
        if(req.rouletteId)
            dto.setRouletteId(req.rouletteId)
        if(req.userId)
            dto.setUserId(req.userId)
        if(req.mount)
            dto.setMount(req.mount)
        if(req.color)
            dto.setColor(req.color)
        if(req.number)
            dto.setNumber(req.number)
        if(req.prize)
            dto.setPrize(req.prize)
        if(req.result)
            dto.setResult(req.result)

        return dto
    }
    
    toResponseWithListBetDto(bets:Array<Bet>,winningNumber:WinningNumberDto){
        let response = new CloseRouletteResponse()
        let betsList = []
        for(let bet of bets){
            let dto = this.toDto(bet)
            betsList.push(dto)
        }
        response.response["winning_number"] = winningNumber
        response.response["bets"] = betsList
        
        return response
    }
}