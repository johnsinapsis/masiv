"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoulettePersistence = void 0;
var roulette_model_1 = require("./models/roulette.model");
var base_persistence_1 = require("../../../shared/infrastructure/persistences/base.persistence");
var RoulettePersistence = /** @class */ (function (_super) {
    __extends(RoulettePersistence, _super);
    function RoulettePersistence() {
        var _this = _super.call(this) || this;
        _this.model = new roulette_model_1.RouletteModel();
        return _this;
    }
    RoulettePersistence.prototype.createRoulette = function (dto) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.setDBAndCollection()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.collection.insertOne(this.model.mapCollection(dto))];
                    case 2:
                        result = _a.sent();
                        this.driver.disconnection();
                        if (result) {
                            if (result.insertedId)
                                dto.setId(result.insertedId);
                        }
                        this.model.clearCollection();
                        return [2 /*return*/, dto];
                }
            });
        });
    };
    RoulettePersistence.prototype.getRouletteById = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _id, data, roulette;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _id = this.getObjectId(id);
                        if (!_id)
                            return [2 /*return*/, null];
                        return [4 /*yield*/, this.setDBAndCollection()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.collection.findOne({ _id: _id })];
                    case 2:
                        data = _a.sent();
                        this.driver.disconnection();
                        if (!data)
                            return [2 /*return*/, null];
                        roulette = this.model.mapEntity(data);
                        return [2 /*return*/, roulette];
                }
            });
        });
    };
    RoulettePersistence.prototype.getAllRoulettes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, dataList, rouletteList, _i, dataList_1, row, roulette;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.setDBAndCollection()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.collection.find({})];
                    case 2:
                        data = _a.sent();
                        return [4 /*yield*/, data.toArray()];
                    case 3:
                        dataList = _a.sent();
                        this.driver.disconnection();
                        rouletteList = [];
                        if (!dataList)
                            return [2 /*return*/, null];
                        for (_i = 0, dataList_1 = dataList; _i < dataList_1.length; _i++) {
                            row = dataList_1[_i];
                            roulette = this.model.mapEntity(row);
                            rouletteList.push(roulette);
                        }
                        return [2 /*return*/, rouletteList];
                }
            });
        });
    };
    return RoulettePersistence;
}(base_persistence_1.Persistence));
exports.RoulettePersistence = RoulettePersistence;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91bGV0dGUucGVyc2lzdGVuY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvcm91bGV0dGVzL2luZnJhc3RydWN0dXJlL3BlcnNpc3RlbmNlcy9yb3VsZXR0ZS5wZXJzaXN0ZW5jZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHQSwwREFBd0Q7QUFDeEQsaUdBQTJGO0FBRzNGO0lBQXlDLHVDQUFXO0lBQ2hEO1FBQUEsWUFDSSxpQkFBTyxTQUVWO1FBREcsS0FBSSxDQUFDLEtBQUssR0FBRyxJQUFJLDhCQUFhLEVBQUUsQ0FBQTs7SUFDcEMsQ0FBQztJQUVLLDRDQUFjLEdBQXBCLFVBQXFCLEdBQWdCOzs7Ozs0QkFDakMscUJBQU0sSUFBSSxDQUFDLGtCQUFrQixFQUFFLEVBQUE7O3dCQUEvQixTQUErQixDQUFBO3dCQUNsQixxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFBOzt3QkFBdkUsTUFBTSxHQUFHLFNBQThEO3dCQUMzRSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFBO3dCQUMzQixJQUFHLE1BQU0sRUFBQzs0QkFDTixJQUFHLE1BQU0sQ0FBQyxVQUFVO2dDQUNoQixHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQTt5QkFDbkM7d0JBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQTt3QkFFNUIsc0JBQU8sR0FBRyxFQUFBOzs7O0tBQ2I7SUFFSyw2Q0FBZSxHQUFyQixVQUFzQixFQUFTOzs7Ozs7d0JBQ3ZCLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFBO3dCQUM5QixJQUFHLENBQUMsR0FBRzs0QkFDSCxzQkFBTyxJQUFJLEVBQUE7d0JBQ2YscUJBQU0sSUFBSSxDQUFDLGtCQUFrQixFQUFFLEVBQUE7O3dCQUEvQixTQUErQixDQUFBO3dCQUNwQixxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFDLEdBQUcsS0FBQSxFQUFDLENBQUMsRUFBQTs7d0JBQTNDLElBQUksR0FBRyxTQUFvQzt3QkFDL0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQTt3QkFDM0IsSUFBRyxDQUFDLElBQUk7NEJBQ0osc0JBQU8sSUFBSSxFQUFBO3dCQUNYLFFBQVEsR0FBWSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQTt3QkFFbEQsc0JBQU8sUUFBUSxFQUFBOzs7O0tBQ2xCO0lBRUssNkNBQWUsR0FBckI7Ozs7OzRCQUNJLHFCQUFNLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxFQUFBOzt3QkFBL0IsU0FBK0IsQ0FBQTt3QkFDcEIscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUE7O3dCQUFyQyxJQUFJLEdBQUcsU0FBOEI7d0JBQzFCLHFCQUFNLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBQTs7d0JBQS9CLFFBQVEsR0FBRyxTQUFvQjt3QkFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQTt3QkFDdkIsWUFBWSxHQUFHLEVBQUUsQ0FBQTt3QkFDckIsSUFBRyxDQUFDLFFBQVE7NEJBQ1Isc0JBQU8sSUFBSSxFQUFBO3dCQUNmLFdBQXVCLEVBQVIscUJBQVEsRUFBUixzQkFBUSxFQUFSLElBQVEsRUFBQzs0QkFBaEIsR0FBRzs0QkFDSCxRQUFRLEdBQVksSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUE7NEJBQ2pELFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7eUJBQzlCO3dCQUVELHNCQUFPLFlBQVksRUFBQTs7OztLQUN0QjtJQUNMLDBCQUFDO0FBQUQsQ0FBQyxBQWhERCxDQUF5Qyw4QkFBVyxHQWdEbkQ7QUFoRFksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91bGV0dGVEdG8gfSBmcm9tIFwiLi4vLi4vYXBwbGljYXRpb24vZHRvL3JvdWxldHRlLmR0b1wiO1xyXG5pbXBvcnQgeyBSb3VsZXR0ZSB9IGZyb20gXCIuLi8uLi9kb21haW4vUm91bGV0dGVcIjtcclxuaW1wb3J0IHsgUm91bGV0dGVSZXBvc2l0b3J5IH0gZnJvbSBcIi4uLy4uL2FwcGxpY2F0aW9uL3JlcG9zaXRvcmllcy9yb3VsZXR0ZS5yZXBvc2l0b3J5XCI7XHJcbmltcG9ydCB7IFJvdWxldHRlTW9kZWwgfSBmcm9tIFwiLi9tb2RlbHMvcm91bGV0dGUubW9kZWxcIjtcclxuaW1wb3J0IHsgUGVyc2lzdGVuY2UgfSBmcm9tIFwiLi4vLi4vLi4vc2hhcmVkL2luZnJhc3RydWN0dXJlL3BlcnNpc3RlbmNlcy9iYXNlLnBlcnNpc3RlbmNlXCI7XHJcbmltcG9ydCB7IFJvdWxldHRlUmVhZFJlcG9zaXRvcnkgfSBmcm9tIFwiLi4vLi4vYXBwbGljYXRpb24vcmVwb3NpdG9yaWVzL3JvdWxldHRlUmVhZC5yZXBvc2l0b3J5XCI7XHJcblxyXG5leHBvcnQgY2xhc3MgUm91bGV0dGVQZXJzaXN0ZW5jZSBleHRlbmRzIFBlcnNpc3RlbmNlIGltcGxlbWVudHMgUm91bGV0dGVSZXBvc2l0b3J5LCBSb3VsZXR0ZVJlYWRSZXBvc2l0b3J5e1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy5tb2RlbCA9IG5ldyBSb3VsZXR0ZU1vZGVsKClcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBjcmVhdGVSb3VsZXR0ZShkdG86IFJvdWxldHRlRHRvKTogUHJvbWlzZTxSb3VsZXR0ZUR0bz4ge1xyXG4gICAgICAgIGF3YWl0IHRoaXMuc2V0REJBbmRDb2xsZWN0aW9uKClcclxuICAgICAgICBsZXQgcmVzdWx0ID0gYXdhaXQgdGhpcy5jb2xsZWN0aW9uLmluc2VydE9uZSh0aGlzLm1vZGVsLm1hcENvbGxlY3Rpb24oZHRvKSlcclxuICAgICAgICB0aGlzLmRyaXZlci5kaXNjb25uZWN0aW9uKClcclxuICAgICAgICBpZihyZXN1bHQpe1xyXG4gICAgICAgICAgICBpZihyZXN1bHQuaW5zZXJ0ZWRJZClcclxuICAgICAgICAgICAgICAgIGR0by5zZXRJZChyZXN1bHQuaW5zZXJ0ZWRJZClcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5tb2RlbC5jbGVhckNvbGxlY3Rpb24oKVxyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiBkdG9cclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBnZXRSb3VsZXR0ZUJ5SWQoaWQ6c3RyaW5nKTpQcm9taXNlIDxSb3VsZXR0ZT57XHJcbiAgICAgICAgbGV0IF9pZCA9IHRoaXMuZ2V0T2JqZWN0SWQoaWQpXHJcbiAgICAgICAgaWYoIV9pZClcclxuICAgICAgICAgICAgcmV0dXJuIG51bGxcclxuICAgICAgICBhd2FpdCB0aGlzLnNldERCQW5kQ29sbGVjdGlvbigpXHJcbiAgICAgICAgbGV0IGRhdGEgPSBhd2FpdCB0aGlzLmNvbGxlY3Rpb24uZmluZE9uZSh7X2lkfSlcclxuICAgICAgICB0aGlzLmRyaXZlci5kaXNjb25uZWN0aW9uKClcclxuICAgICAgICBpZighZGF0YSlcclxuICAgICAgICAgICAgcmV0dXJuIG51bGxcclxuICAgICAgICBsZXQgcm91bGV0dGU6Um91bGV0dGUgPSB0aGlzLm1vZGVsLm1hcEVudGl0eShkYXRhKVxyXG5cclxuICAgICAgICByZXR1cm4gcm91bGV0dGUgXHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgZ2V0QWxsUm91bGV0dGVzKCk6IFByb21pc2UgPEFycmF5PFJvdWxldHRlPj57XHJcbiAgICAgICAgYXdhaXQgdGhpcy5zZXREQkFuZENvbGxlY3Rpb24oKVxyXG4gICAgICAgIGxldCBkYXRhID0gYXdhaXQgdGhpcy5jb2xsZWN0aW9uLmZpbmQoe30pXHJcbiAgICAgICAgbGV0IGRhdGFMaXN0ID0gYXdhaXQgZGF0YS50b0FycmF5KClcclxuICAgICAgICB0aGlzLmRyaXZlci5kaXNjb25uZWN0aW9uKClcclxuICAgICAgICBsZXQgcm91bGV0dGVMaXN0ID0gW11cclxuICAgICAgICBpZighZGF0YUxpc3QpXHJcbiAgICAgICAgICAgIHJldHVybiBudWxsXHJcbiAgICAgICAgZm9yKGxldCByb3cgb2YgZGF0YUxpc3Qpe1xyXG4gICAgICAgICAgICBsZXQgcm91bGV0dGU6Um91bGV0dGUgPSB0aGlzLm1vZGVsLm1hcEVudGl0eShyb3cpXHJcbiAgICAgICAgICAgIHJvdWxldHRlTGlzdC5wdXNoKHJvdWxldHRlKVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJvdWxldHRlTGlzdFxyXG4gICAgfVxyXG59Il19