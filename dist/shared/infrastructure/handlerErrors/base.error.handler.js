"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseErrorHandler = void 0;
var business_error_handler_1 = require("./business.error.handler");
var stackParser_handler_1 = require("./stackParser.handler");
var business_error_1 = require("../../application/handler/business.error");
var BaseErrorHandler = /** @class */ (function () {
    function BaseErrorHandler() {
    }
    BaseErrorHandler.handle = function (res, e) {
        if (e instanceof business_error_1.BusinessError) {
            var properties = this.parseStack(e.response);
            this.clear(e);
            return res.status(200).json(e);
        }
        else {
            return res.status(400).json({ error: "Excepción no controlada del sistema" });
        }
    };
    BaseErrorHandler.parseStack = function (e) {
        var parser = new stackParser_handler_1.StackParser(e.stack);
        var line = parser.getTopLine();
        var file = parser.getTopFile();
        return { line: line, file: file };
    };
    BaseErrorHandler.clear = function (e) {
        delete e.code;
        delete e.response.stack;
    };
    BaseErrorHandler.beh = new business_error_handler_1.BusinessErrorHandler();
    return BaseErrorHandler;
}());
exports.BaseErrorHandler = BaseErrorHandler;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5lcnJvci5oYW5kbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL3NoYXJlZC9pbmZyYXN0cnVjdHVyZS9oYW5kbGVyRXJyb3JzL2Jhc2UuZXJyb3IuaGFuZGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxtRUFBK0Q7QUFDL0QsNkRBQW1EO0FBQ25ELDJFQUF3RTtBQUV4RTtJQUFBO0lBeUJBLENBQUM7SUF0QlUsdUJBQU0sR0FBYixVQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxZQUFZLDhCQUFhLEVBQUU7WUFDNUIsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUE7WUFDNUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUNiLE9BQU8sR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDakM7YUFDRztZQUNBLE9BQU8sR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBQyxLQUFLLEVBQUMscUNBQXFDLEVBQUMsQ0FBQyxDQUFBO1NBQzdFO0lBQ0wsQ0FBQztJQUVNLDJCQUFVLEdBQWpCLFVBQWtCLENBQUM7UUFDZixJQUFJLE1BQU0sR0FBRyxJQUFJLGlDQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3JDLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQTtRQUM5QixJQUFJLElBQUksR0FBRyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUE7UUFDOUIsT0FBTyxFQUFDLElBQUksTUFBQSxFQUFDLElBQUksTUFBQSxFQUFDLENBQUE7SUFDdEIsQ0FBQztJQUVNLHNCQUFLLEdBQVosVUFBYSxDQUFDO1FBQ1YsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFBO1FBQ2IsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQTtJQUMzQixDQUFDO0lBdkJNLG9CQUFHLEdBQUcsSUFBSSw2Q0FBb0IsRUFBRSxDQUFBO0lBd0IzQyx1QkFBQztDQUFBLEFBekJELElBeUJDO0FBekJZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJ1c2luZXNzRXJyb3JIYW5kbGVyIH0gZnJvbSBcIi4vYnVzaW5lc3MuZXJyb3IuaGFuZGxlclwiXHJcbmltcG9ydCB7IFN0YWNrUGFyc2VyIH0gZnJvbSBcIi4vc3RhY2tQYXJzZXIuaGFuZGxlclwiXHJcbmltcG9ydCB7IEJ1c2luZXNzRXJyb3IgfSBmcm9tIFwiLi4vLi4vYXBwbGljYXRpb24vaGFuZGxlci9idXNpbmVzcy5lcnJvclwiXHJcblxyXG5leHBvcnQgY2xhc3MgQmFzZUVycm9ySGFuZGxlciB7XHJcbiAgICBzdGF0aWMgYmVoID0gbmV3IEJ1c2luZXNzRXJyb3JIYW5kbGVyKClcclxuXHJcbiAgICBzdGF0aWMgaGFuZGxlKHJlcywgZSkge1xyXG4gICAgICAgIGlmIChlIGluc3RhbmNlb2YgQnVzaW5lc3NFcnJvcikge1xyXG4gICAgICAgICAgICBsZXQgcHJvcGVydGllcyA9IHRoaXMucGFyc2VTdGFjayhlLnJlc3BvbnNlKVxyXG4gICAgICAgICAgICB0aGlzLmNsZWFyKGUpXHJcbiAgICAgICAgICAgIHJldHVybiByZXMuc3RhdHVzKDIwMCkuanNvbihlKVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzLnN0YXR1cyg0MDApLmpzb24oe2Vycm9yOlwiRXhjZXBjacOzbiBubyBjb250cm9sYWRhIGRlbCBzaXN0ZW1hXCJ9KVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgcGFyc2VTdGFjayhlKSB7XHJcbiAgICAgICAgbGV0IHBhcnNlciA9IG5ldyBTdGFja1BhcnNlcihlLnN0YWNrKVxyXG4gICAgICAgIGxldCBsaW5lID0gcGFyc2VyLmdldFRvcExpbmUoKVxyXG4gICAgICAgIGxldCBmaWxlID0gcGFyc2VyLmdldFRvcEZpbGUoKVxyXG4gICAgICAgIHJldHVybiB7bGluZSxmaWxlfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBjbGVhcihlKXtcclxuICAgICAgICBkZWxldGUgZS5jb2RlXHJcbiAgICAgICAgZGVsZXRlIGUucmVzcG9uc2Uuc3RhY2tcclxuICAgIH1cclxufVxyXG4iXX0=