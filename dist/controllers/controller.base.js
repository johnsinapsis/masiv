"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controller = void 0;
var config_1 = require("../config/config");
var Controller = /** @class */ (function () {
    function Controller() {
        this.cr = config_1.ConfigReader;
    }
    Controller.prototype.getPathsMap = function (controllerConfigId) {
        return this.cr.getPathsMap(controllerConfigId);
    };
    Controller.prototype.build400Response = function (res) {
        return {
            "code": 400,
            "invalid_fields": res.invalid_fields
        };
    };
    return Controller;
}());
exports.Controller = Controller;
