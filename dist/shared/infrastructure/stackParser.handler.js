"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StackParser = void 0;
var config_1 = require("../../config/config");
var StackParser = /** @class */ (function () {
    function StackParser(stack) {
        this.frames = [];
        this.config = { "skip_js": true,
            "max_line": 120,
            "source_path": true };
        this.load(stack);
    }
    StackParser.prototype.load = function (stack) {
        this.frames = [];
        if (stack) {
            for (var _i = 0, _a = stack.split("\n"); _i < _a.length; _i++) {
                var frameTxt = _a[_i];
                var idx = frameTxt.lastIndexOf(config_1.EnvConfig.pathSeparator());
                if (idx > 0) {
                    var frame = {};
                    frame.textLine = frameTxt;
                    var parts = frame.textLine.split('(');
                    if (parts.length == 2) {
                        frame.classPath = parts[0];
                        frame.sourcePath = parts[1].replace(')', '');
                    }
                    frameTxt = frameTxt.substring(idx);
                    parts = frameTxt.split(':');
                    if (parts.length == 3) {
                        frame.lineNumber = parts[1];
                        frame.lineColumn = parts[2].replace(')', '');
                        frame.file = parts[0].replace('/', '');
                    }
                    this.frames.push(frame);
                }
            }
        }
        return this;
    };
    StackParser.prototype.loggerProxy = function () {
        return { 'err': console.log };
    };
    StackParser.prototype.printStack = function (loggerP) {
        var logger = loggerP ? loggerP : this.loggerProxy();
        if (this.config.skip_js) {
            var max = this.config.max_line;
            for (var _i = 0, _a = this.frames; _i < _a.length; _i++) {
                var frame = _a[_i];
                if (frame.file.endsWith('.ts') && !this.config.source_path) {
                    var truncated = frame.textLine.length > max ? frame.textLine.substring(frame.textLine.length - max) : frame.textLine;
                    logger.err(truncated);
                }
                else if (frame.file.endsWith('.ts')) {
                    logger.err(frame.sourcePath);
                }
            }
        }
    };
    StackParser.prototype.printFrame = function (frame) {
        console.log(frame.file + '( ' + frame.lineNumber + ' )' + ', ( ' + frame.lineColumn + ' )');
    };
    StackParser.prototype.getTopLine = function () {
        return this.frames[0] ? this.frames[0].lineNumber : '';
    };
    StackParser.prototype.getTopFile = function () {
        return this.frames[0] ? this.frames[0].file : '';
    };
    return StackParser;
}());
exports.StackParser = StackParser;
