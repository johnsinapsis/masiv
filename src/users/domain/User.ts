import { Entity } from "../../shared/domain/base.entity"

export class User extends Entity{
    id:string
    nickname: string 
}