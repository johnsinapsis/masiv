"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseValidation = void 0;
var constants_1 = require("../../../config/constants");
var validation_factory_1 = require("./validation.factory");
var BaseValidation = /** @class */ (function () {
    function BaseValidation() {
        this.fieldValidators = [];
        this.fieldValidationFactory = validation_factory_1.FieldValidationFactory;
    }
    BaseValidation.prototype.isValid = function (req, res) {
        for (var _i = 0, _a = this.fieldValidators; _i < _a.length; _i++) {
            var validator = _a[_i];
            var isValid = validator.evaluate(req, res);
            if (!isValid) {
                return false;
            }
        }
        if (typeof res[constants_1.INV_FIELDS] != constants_1.UNDEF && res[constants_1.INV_FIELDS].length > 0) {
            return false;
        }
        return true;
    };
    BaseValidation.prototype.addValidator = function (validator) {
        this.fieldValidators.push(validator);
    };
    return BaseValidation;
}());
exports.BaseValidation = BaseValidation;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS52YWxpZGF0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL3NoYXJlZC9hcHBsaWNhdGlvbi92YWxpZGF0aW9uL2Jhc2UudmFsaWRhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSx1REFBNkQ7QUFDN0QsMkRBQThEO0FBRTlEO0lBSUk7UUFDSSxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQTtRQUN6QixJQUFJLENBQUMsc0JBQXNCLEdBQUcsMkNBQXNCLENBQUE7SUFDeEQsQ0FBQztJQUVELGdDQUFPLEdBQVAsVUFBUSxHQUFHLEVBQUUsR0FBRztRQUNaLEtBQXNCLFVBQW9CLEVBQXBCLEtBQUEsSUFBSSxDQUFDLGVBQWUsRUFBcEIsY0FBb0IsRUFBcEIsSUFBb0IsRUFBRTtZQUF2QyxJQUFJLFNBQVMsU0FBQTtZQUNkLElBQUksT0FBTyxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFBO1lBQzFDLElBQUcsQ0FBQyxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxLQUFLLENBQUE7YUFDZjtTQUNKO1FBQ0QsSUFBSSxPQUFPLEdBQUcsQ0FBQyxzQkFBVSxDQUFDLElBQUksaUJBQUssSUFBSSxHQUFHLENBQUMsc0JBQVUsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDL0QsT0FBTyxLQUFLLENBQUE7U0FDZjtRQUNELE9BQU8sSUFBSSxDQUFBO0lBQ2YsQ0FBQztJQUVELHFDQUFZLEdBQVosVUFBYSxTQUFTO1FBQ2xCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO0lBQ3hDLENBQUM7SUFDTCxxQkFBQztBQUFELENBQUMsQUF6QkQsSUF5QkM7QUF6Qlksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBVTkRFRixJTlZfRklFTERTIH0gZnJvbSBcIi4uLy4uLy4uL2NvbmZpZy9jb25zdGFudHNcIjtcclxuaW1wb3J0IHsgRmllbGRWYWxpZGF0aW9uRmFjdG9yeSB9IGZyb20gXCIuL3ZhbGlkYXRpb24uZmFjdG9yeVwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIEJhc2VWYWxpZGF0aW9ue1xyXG4gICAgcHJvdGVjdGVkIGZpZWxkVmFsaWRhdG9yc1xyXG4gICAgcHJvdGVjdGVkIGZpZWxkVmFsaWRhdGlvbkZhY3RvcnlcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcigpe1xyXG4gICAgICAgIHRoaXMuZmllbGRWYWxpZGF0b3JzID0gW11cclxuICAgICAgICB0aGlzLmZpZWxkVmFsaWRhdGlvbkZhY3RvcnkgPSBGaWVsZFZhbGlkYXRpb25GYWN0b3J5XHJcbiAgICB9XHJcblxyXG4gICAgaXNWYWxpZChyZXEsIHJlcyl7ICBcclxuICAgICAgICBmb3IoIGxldCB2YWxpZGF0b3Igb2YgdGhpcy5maWVsZFZhbGlkYXRvcnMgKXsgICBcclxuICAgICAgICAgICAgbGV0IGlzVmFsaWQgPSB2YWxpZGF0b3IuZXZhbHVhdGUocmVxLCByZXMpXHJcbiAgICAgICAgICAgIGlmKCFpc1ZhbGlkICl7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2VcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiggdHlwZW9mIHJlc1tJTlZfRklFTERTXSAhPSBVTkRFRiAmJiByZXNbSU5WX0ZJRUxEU10ubGVuZ3RoID4gMCApe1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2VcclxuICAgICAgICB9IFxyXG4gICAgICAgIHJldHVybiB0cnVlXHJcbiAgICB9XHJcblxyXG4gICAgYWRkVmFsaWRhdG9yKHZhbGlkYXRvcil7XHJcbiAgICAgICAgdGhpcy5maWVsZFZhbGlkYXRvcnMucHVzaCh2YWxpZGF0b3IpXHJcbiAgICB9XHJcbn0iXX0=