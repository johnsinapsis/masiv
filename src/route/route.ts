import { ControllerFactory } from "../shared/infrastructure/controllers/controller.factory";

export class RouteManager {
    constructor( protected router ){
        this.router = router
    }

    bindAny(controlName) {
        let instance = ControllerFactory.createInstance(controlName)
        let pathsMap = instance.getPathsMap(instance.getConfigId())
        for (let route in pathsMap) {
            if (route.startsWith("get")){
                this.router.get(pathsMap[route], (req, res) => instance[route](req, res))
            }
            if (route.startsWith("post")) {

                this.router.post(pathsMap[route], (req, res) => instance[route](req, res))
            }
            if (route.startsWith("put")) {

                this.router.put(pathsMap[route], (req, res) => instance[route](req, res))
            }

        }
    }
}