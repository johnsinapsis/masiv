"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.FieldsOpenRouletteValidation = void 0;
var base_validation_1 = require("../../../shared/application/validation/base.validation");
var FieldsOpenRouletteValidation = /** @class */ (function (_super) {
    __extends(FieldsOpenRouletteValidation, _super);
    function FieldsOpenRouletteValidation() {
        var _this = _super.call(this) || this;
        var emptyId = _this.fieldValidationFactory.createInstance('empty', '$.id', 'El id de ruleta es requerido');
        _this.addValidator(emptyId);
        return _this;
    }
    return FieldsOpenRouletteValidation;
}(base_validation_1.BaseValidation));
exports.FieldsOpenRouletteValidation = FieldsOpenRouletteValidation;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGRzT3BlblJvdWxldHRlLnZhbGlkYXRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvcm91bGV0dGVzL2FwcGxpY2F0aW9uL3ZhbGlkYXRpb25zL2ZpZWxkc09wZW5Sb3VsZXR0ZS52YWxpZGF0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDBGQUF3RjtBQUV4RjtJQUFrRCxnREFBYztJQUM1RDtRQUFBLFlBQ0ksaUJBQU8sU0FHVjtRQUZHLElBQUksT0FBTyxHQUFHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFDLE1BQU0sRUFBQyw4QkFBOEIsQ0FBQyxDQUFBO1FBQ3ZHLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUE7O0lBQzlCLENBQUM7SUFDTCxtQ0FBQztBQUFELENBQUMsQUFORCxDQUFrRCxnQ0FBYyxHQU0vRDtBQU5ZLG9FQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJhc2VWYWxpZGF0aW9uIH0gZnJvbSBcIi4uLy4uLy4uL3NoYXJlZC9hcHBsaWNhdGlvbi92YWxpZGF0aW9uL2Jhc2UudmFsaWRhdGlvblwiO1xyXG5cclxuZXhwb3J0IGNsYXNzIEZpZWxkc09wZW5Sb3VsZXR0ZVZhbGlkYXRpb24gZXh0ZW5kcyBCYXNlVmFsaWRhdGlvbntcclxuICAgIGNvbnN0cnVjdG9yKCl7XHJcbiAgICAgICAgc3VwZXIoKVxyXG4gICAgICAgIGxldCBlbXB0eUlkID0gdGhpcy5maWVsZFZhbGlkYXRpb25GYWN0b3J5LmNyZWF0ZUluc3RhbmNlKCdlbXB0eScsJyQuaWQnLCdFbCBpZCBkZSBydWxldGEgZXMgcmVxdWVyaWRvJylcclxuICAgICAgICB0aGlzLmFkZFZhbGlkYXRvcihlbXB0eUlkKVxyXG4gICAgfVxyXG59Il19