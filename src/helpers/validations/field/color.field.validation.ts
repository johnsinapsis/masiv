import { FieldValidator } from './field.validation'
import jp from 'jsonpath'
import { INV_FIELDS, UNDEF } from '../../../config/constants'
import { color ,Color } from '../../../bets/domain/Color'

const isColor = (col:any): col is Color => color.includes(col)

export class ColorFieldValidation extends FieldValidator{
    constructor(jpExpr, failMsg){
        super(jpExpr, failMsg)
        this.jpExpr = jpExpr
        this.failMsg = failMsg
    }

    isValid(req, res){
        let field = jp.query(req.body, this.jpExpr)[0]
        if(!field)
            return true
        if(!isColor(field))
            return false;

        return true;
    }

    addErrorResponse( res, msg ){        
        if( typeof res[INV_FIELDS] === UNDEF ){
            res.invalid_fields = []
        }
        res.invalid_fields.push(msg)
    }
}