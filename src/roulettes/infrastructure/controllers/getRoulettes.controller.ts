import { Controller } from "../../../shared/infrastructure/controllers/controller.base";
import { logger } from "../../../helpers/default.logger";
import { BaseErrorHandler } from "../../../shared/infrastructure/handlerErrors/base.error.handler";
import { ConfigControllerIdentity } from "../../../shared/infrastructure/controllers/config.controller.identity";
import { GetRoulettesMapper } from "../mappers/getRoulettes.mapper";
import { RoulettePersistence } from "../persistences/roulette.persistence";
import { Roulette } from "../../domain/Roulette";

export class GetRoulettesController extends Controller implements ConfigControllerIdentity{
    private mapper = new GetRoulettesMapper()
    private service = new RoulettePersistence()

    constructor(){
        super()
    }

    getConfigId(){
        return 'getRoulettes'
    }

    async getRoulettes(req,res){
        try{
            let listRoulettes:Array<Roulette> = await this.service.getAllRoulettes() 
            let response = this.mapper.toResponseWithListRouletteDto(listRoulettes)

            return res.json(response)
        }
        catch(e){
            logger.error(e)

            return BaseErrorHandler.handle(res,e)
        }
    }
}