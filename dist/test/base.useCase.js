"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseUseCase = void 0;
var config_1 = __importDefault(require("../config/config"));
var ConfigReader_1 = require("../config/ConfigReader");
var BaseUseCase = /** @class */ (function () {
    function BaseUseCase() {
        this.cr = ConfigReader_1.ConfigReader;
        this.url = 'http://localhost:' + config_1.default.port;
    }
    BaseUseCase.prototype.getRandomInt = function (min, max) {
        min = Math.ceil(min);
        max = Math.ceil(max);
        return Math.floor(Math.random() * (max - min) + min);
    };
    return BaseUseCase;
}());
exports.BaseUseCase = BaseUseCase;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS51c2VDYXNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL3Rlc3QvYmFzZS51c2VDYXNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLDREQUFxQztBQUNyQyx1REFBcUQ7QUFFckQ7SUFHSTtRQUNJLElBQUksQ0FBQyxFQUFFLEdBQUcsMkJBQVksQ0FBQTtRQUN0QixJQUFJLENBQUMsR0FBRyxHQUFHLG1CQUFtQixHQUFDLGdCQUFNLENBQUMsSUFBSSxDQUFBO0lBQzlDLENBQUM7SUFFUyxrQ0FBWSxHQUF0QixVQUF1QixHQUFVLEVBQUMsR0FBVTtRQUN4QyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQixHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFBO0lBQ3hELENBQUM7SUFDTCxrQkFBQztBQUFELENBQUMsQUFiRCxJQWFDO0FBYlksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgY29uZmlnIGZyb20gXCIuLi9jb25maWcvY29uZmlnXCJcclxuaW1wb3J0IHsgQ29uZmlnUmVhZGVyIH0gZnJvbSBcIi4uL2NvbmZpZy9Db25maWdSZWFkZXJcIlxyXG5cclxuZXhwb3J0IGNsYXNzIEJhc2VVc2VDYXNle1xyXG4gICAgcHJvdGVjdGVkIGNyIFxyXG4gICAgcHJvdGVjdGVkIHVybFxyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICB0aGlzLmNyID0gQ29uZmlnUmVhZGVyXHJcbiAgICAgICAgdGhpcy51cmwgPSAnaHR0cDovL2xvY2FsaG9zdDonK2NvbmZpZy5wb3J0XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIGdldFJhbmRvbUludChtaW46bnVtYmVyLG1heDpudW1iZXIpe1xyXG4gICAgICAgIG1pbiA9IE1hdGguY2VpbChtaW4pO1xyXG4gICAgICAgIG1heCA9IE1hdGguY2VpbChtYXgpO1xyXG4gICAgICAgIHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobWF4IC0gbWluKSArIG1pbilcclxuICAgIH1cclxufVxyXG5cclxuIl19