import { BusinessError } from "../../application/handler/business.error"
import { RESULT_ERROR } from "../../../config/constants"

export class Condition {
    public type

    public throwBusiness(msg){
        let response = {result:RESULT_ERROR ,description: msg}
        throw new BusinessError(this.type,response)
    }
}