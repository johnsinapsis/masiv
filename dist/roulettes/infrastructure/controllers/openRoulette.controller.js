"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenRouletteController = void 0;
var controller_base_1 = require("../../../shared/infrastructure/controllers/controller.base");
var default_logger_1 = require("../../../helpers/default.logger");
var base_error_handler_1 = require("../../../shared/infrastructure/handlerErrors/base.error.handler");
var roulette_dto_1 = require("../../application/dto/roulette.dto");
var status_roulette_persistence_1 = require("../persistences/status.roulette.persistence");
var fieldsOpenRoulette_validation_1 = require("../../application/validations/fieldsOpenRoulette.validation");
var error_openRoulette_1 = require("../handler/error.openRoulette");
var openRoulette_rules_1 = require("../rules/openRoulette.rules");
var openRoulette_mapper_1 = require("../mappers/openRoulette.mapper");
var OpenRouletteController = /** @class */ (function (_super) {
    __extends(OpenRouletteController, _super);
    function OpenRouletteController() {
        var _this = _super.call(this) || this;
        _this.mapper = new openRoulette_mapper_1.OpenRouletteMapper();
        _this.service = new status_roulette_persistence_1.StatusRoulettePersistence();
        _this.validateFields = new fieldsOpenRoulette_validation_1.FieldsOpenRouletteValidation();
        _this.rules = new openRoulette_rules_1.OpenRouletteRules();
        return _this;
    }
    OpenRouletteController.prototype.getConfigId = function () {
        return 'openRoulette';
    };
    OpenRouletteController.prototype.putOpenRoulette = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var update, response, error, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        if (!this.validateFields.isValid(req, res)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.rules.exec(req.body.id)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.service.setOpenStatusRoulette(req.body.id)];
                    case 2:
                        update = _a.sent();
                        if (!update)
                            return [2 /*return*/, base_error_handler_1.BaseErrorHandler.handle(res, "Error inesperado al abrir la ruleta")];
                        response = this.mapper.toResponse(new roulette_dto_1.RouletteDto());
                        return [2 /*return*/, res.json(response)];
                    case 3:
                        error = new error_openRoulette_1.ErrorOpenRoulette();
                        return [2 /*return*/, res.json(error.buildResponse(res))];
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        default_logger_1.logger.error(e_1);
                        return [2 /*return*/, base_error_handler_1.BaseErrorHandler.handle(res, e_1)];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    return OpenRouletteController;
}(controller_base_1.Controller));
exports.OpenRouletteController = OpenRouletteController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlblJvdWxldHRlLmNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvcm91bGV0dGVzL2luZnJhc3RydWN0dXJlL2NvbnRyb2xsZXJzL29wZW5Sb3VsZXR0ZS5jb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDhGQUF3RjtBQUN4RixrRUFBeUQ7QUFDekQsc0dBQW1HO0FBRW5HLG1FQUFpRTtBQUNqRSwyRkFBd0Y7QUFDeEYsNkdBQTJHO0FBQzNHLG9FQUFrRTtBQUNsRSxrRUFBZ0U7QUFDaEUsc0VBQW9FO0FBRXBFO0lBQTRDLDBDQUFVO0lBTWxEO1FBQUEsWUFDSSxpQkFBTyxTQUdWO1FBUE8sWUFBTSxHQUFHLElBQUksd0NBQWtCLEVBQUUsQ0FBQTtRQUNqQyxhQUFPLEdBQUcsSUFBSSx1REFBeUIsRUFBRSxDQUFBO1FBSTdDLEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSw0REFBNEIsRUFBRSxDQUFBO1FBQ3hELEtBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxzQ0FBaUIsRUFBRSxDQUFBOztJQUN4QyxDQUFDO0lBRUQsNENBQVcsR0FBWDtRQUNJLE9BQU8sY0FBYyxDQUFBO0lBQ3pCLENBQUM7SUFFSyxnREFBZSxHQUFyQixVQUFzQixHQUFHLEVBQUMsR0FBRzs7Ozs7Ozs2QkFFbEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFDLEdBQUcsQ0FBQyxFQUFwQyx3QkFBb0M7d0JBQ25DLHFCQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUE7O3dCQUFsQyxTQUFrQyxDQUFBO3dCQUNyQixxQkFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUE7O3dCQUE5RCxNQUFNLEdBQUcsU0FBcUQ7d0JBQ2xFLElBQUcsQ0FBQyxNQUFNOzRCQUNOLHNCQUFPLHFDQUFnQixDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUMscUNBQXFDLENBQUMsRUFBQTt3QkFDekUsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksMEJBQVcsRUFBRSxDQUFDLENBQUE7d0JBRXhELHNCQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUE7O3dCQUdyQixLQUFLLEdBQUcsSUFBSSxzQ0FBaUIsRUFBRSxDQUFBO3dCQUVuQyxzQkFBTyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBQTs7Ozt3QkFJN0MsdUJBQU0sQ0FBQyxLQUFLLENBQUMsR0FBQyxDQUFDLENBQUE7d0JBRWYsc0JBQU8scUNBQWdCLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBQyxHQUFDLENBQUMsRUFBQTs7Ozs7S0FFNUM7SUFDTCw2QkFBQztBQUFELENBQUMsQUF2Q0QsQ0FBNEMsNEJBQVUsR0F1Q3JEO0FBdkNZLHdEQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbnRyb2xsZXIgfSBmcm9tIFwiLi4vLi4vLi4vc2hhcmVkL2luZnJhc3RydWN0dXJlL2NvbnRyb2xsZXJzL2NvbnRyb2xsZXIuYmFzZVwiO1xyXG5pbXBvcnQgeyBsb2dnZXIgfSBmcm9tIFwiLi4vLi4vLi4vaGVscGVycy9kZWZhdWx0LmxvZ2dlclwiO1xyXG5pbXBvcnQgeyBCYXNlRXJyb3JIYW5kbGVyIH0gZnJvbSBcIi4uLy4uLy4uL3NoYXJlZC9pbmZyYXN0cnVjdHVyZS9oYW5kbGVyRXJyb3JzL2Jhc2UuZXJyb3IuaGFuZGxlclwiO1xyXG5pbXBvcnQgeyBDb25maWdDb250cm9sbGVySWRlbnRpdHkgfSBmcm9tIFwiLi4vLi4vLi4vc2hhcmVkL2luZnJhc3RydWN0dXJlL2NvbnRyb2xsZXJzL2NvbmZpZy5jb250cm9sbGVyLmlkZW50aXR5XCI7XHJcbmltcG9ydCB7IFJvdWxldHRlRHRvIH0gZnJvbSBcIi4uLy4uL2FwcGxpY2F0aW9uL2R0by9yb3VsZXR0ZS5kdG9cIjtcclxuaW1wb3J0IHsgU3RhdHVzUm91bGV0dGVQZXJzaXN0ZW5jZSB9IGZyb20gXCIuLi9wZXJzaXN0ZW5jZXMvc3RhdHVzLnJvdWxldHRlLnBlcnNpc3RlbmNlXCI7XHJcbmltcG9ydCB7IEZpZWxkc09wZW5Sb3VsZXR0ZVZhbGlkYXRpb24gfSBmcm9tIFwiLi4vLi4vYXBwbGljYXRpb24vdmFsaWRhdGlvbnMvZmllbGRzT3BlblJvdWxldHRlLnZhbGlkYXRpb25cIjtcclxuaW1wb3J0IHsgRXJyb3JPcGVuUm91bGV0dGUgfSBmcm9tIFwiLi4vaGFuZGxlci9lcnJvci5vcGVuUm91bGV0dGVcIjtcclxuaW1wb3J0IHsgT3BlblJvdWxldHRlUnVsZXMgfSBmcm9tIFwiLi4vcnVsZXMvb3BlblJvdWxldHRlLnJ1bGVzXCI7XHJcbmltcG9ydCB7IE9wZW5Sb3VsZXR0ZU1hcHBlciB9IGZyb20gXCIuLi9tYXBwZXJzL29wZW5Sb3VsZXR0ZS5tYXBwZXJcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBPcGVuUm91bGV0dGVDb250cm9sbGVyIGV4dGVuZHMgQ29udHJvbGxlciBpbXBsZW1lbnRzIENvbmZpZ0NvbnRyb2xsZXJJZGVudGl0eXtcclxuICAgIHByaXZhdGUgdmFsaWRhdGVGaWVsZHNcclxuICAgIHByaXZhdGUgcnVsZXNcclxuICAgIHByaXZhdGUgbWFwcGVyID0gbmV3IE9wZW5Sb3VsZXR0ZU1hcHBlcigpXHJcbiAgICBwcml2YXRlIHNlcnZpY2UgPSBuZXcgU3RhdHVzUm91bGV0dGVQZXJzaXN0ZW5jZSgpXHJcblxyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy52YWxpZGF0ZUZpZWxkcyA9IG5ldyBGaWVsZHNPcGVuUm91bGV0dGVWYWxpZGF0aW9uKClcclxuICAgICAgICB0aGlzLnJ1bGVzID0gbmV3IE9wZW5Sb3VsZXR0ZVJ1bGVzKClcclxuICAgIH1cclxuXHJcbiAgICBnZXRDb25maWdJZCgpe1xyXG4gICAgICAgIHJldHVybiAnb3BlblJvdWxldHRlJ1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIHB1dE9wZW5Sb3VsZXR0ZShyZXEscmVzKXtcclxuICAgICAgICB0cnl7XHJcbiAgICAgICAgICAgIGlmKHRoaXMudmFsaWRhdGVGaWVsZHMuaXNWYWxpZChyZXEscmVzKSl7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLnJ1bGVzLmV4ZWMocmVxLmJvZHkuaWQpXHJcbiAgICAgICAgICAgICAgICBsZXQgdXBkYXRlID0gYXdhaXQgdGhpcy5zZXJ2aWNlLnNldE9wZW5TdGF0dXNSb3VsZXR0ZShyZXEuYm9keS5pZClcclxuICAgICAgICAgICAgICAgIGlmKCF1cGRhdGUpXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIEJhc2VFcnJvckhhbmRsZXIuaGFuZGxlKHJlcyxcIkVycm9yIGluZXNwZXJhZG8gYWwgYWJyaXIgbGEgcnVsZXRhXCIpXHJcbiAgICAgICAgICAgICAgICBsZXQgcmVzcG9uc2UgPSB0aGlzLm1hcHBlci50b1Jlc3BvbnNlKG5ldyBSb3VsZXR0ZUR0bygpKVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiByZXMuanNvbihyZXNwb25zZSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgbGV0IGVycm9yID0gbmV3IEVycm9yT3BlblJvdWxldHRlKClcclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVzLmpzb24oZXJyb3IuYnVpbGRSZXNwb25zZShyZXMpKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNhdGNoKGUpe1xyXG4gICAgICAgICAgICBsb2dnZXIuZXJyb3IoZSlcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHJldHVybiBCYXNlRXJyb3JIYW5kbGVyLmhhbmRsZShyZXMsZSlcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iXX0=