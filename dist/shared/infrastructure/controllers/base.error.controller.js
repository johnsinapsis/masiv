"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseErrorController = void 0;
var constants_1 = require("../../../config/constants");
var stackParser_handler_1 = require("../handlerErrors/stackParser.handler");
var default_logger_1 = require("../../../helpers/default.logger");
var BaseErrorController = /** @class */ (function () {
    function BaseErrorController() {
    }
    BaseErrorController.prototype.buildResponse = function (res) {
        var response = { result: constants_1.RESULT_ERROR, description: '', line: '', file: '' };
        return this.toResponse(res, response);
    };
    BaseErrorController.prototype.toResponse = function (res, response) {
        var newRes = this.map2response(response, res.invalid_fields[0]);
        var error = this.parseStack();
        newRes.line = error.line;
        newRes.file = error.file.substring(1);
        default_logger_1.logger.error("ErrorType: " + this.type + ", file: " + newRes.file + ", line: " + newRes.line + " ");
        return {
            type: this.type,
            response: newRes
        };
    };
    BaseErrorController.prototype.map2response = function (response, msg) {
        response.description = msg;
        return response;
    };
    BaseErrorController.prototype.parseStack = function () {
        var parser = new stackParser_handler_1.StackParser(this.stack);
        var line = parser.getTopLine();
        var file = parser.getTopFile();
        return { line: line, file: file };
    };
    return BaseErrorController;
}());
exports.BaseErrorController = BaseErrorController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5lcnJvci5jb250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL3NoYXJlZC9pbmZyYXN0cnVjdHVyZS9jb250cm9sbGVycy9iYXNlLmVycm9yLmNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsdURBQXdEO0FBQ3hELDRFQUFrRTtBQUNsRSxrRUFBd0Q7QUFFeEQ7SUFBQTtJQW1DQSxDQUFDO0lBL0JHLDJDQUFhLEdBQWIsVUFBYyxHQUFHO1FBQ2IsSUFBSSxRQUFRLEdBQUcsRUFBQyxNQUFNLEVBQUMsd0JBQVksRUFBRSxXQUFXLEVBQUMsRUFBRSxFQUFDLElBQUksRUFBQyxFQUFFLEVBQUUsSUFBSSxFQUFDLEVBQUUsRUFBRSxDQUFBO1FBQ3RFLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUE7SUFDekMsQ0FBQztJQUVNLHdDQUFVLEdBQWpCLFVBQWtCLEdBQUcsRUFBQyxRQUFRO1FBQzFCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUM5RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7UUFDN0IsTUFBTSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFBO1FBQ3hCLE1BQU0sQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDckMsdUJBQU0sQ0FBQyxLQUFLLENBQUMsZ0JBQWMsSUFBSSxDQUFDLElBQUksZ0JBQVcsTUFBTSxDQUFDLElBQUksZ0JBQVcsTUFBTSxDQUFDLElBQUksTUFBRyxDQUFDLENBQUE7UUFFcEYsT0FBTztZQUNILElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtZQUNmLFFBQVEsRUFBQyxNQUFNO1NBQ2xCLENBQUE7SUFDTCxDQUFDO0lBRVMsMENBQVksR0FBdEIsVUFBdUIsUUFBUSxFQUFDLEdBQUc7UUFDL0IsUUFBUSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUE7UUFFMUIsT0FBTyxRQUFRLENBQUE7SUFDbkIsQ0FBQztJQUVPLHdDQUFVLEdBQWxCO1FBQ0ksSUFBSSxNQUFNLEdBQUcsSUFBSSxpQ0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUN4QyxJQUFJLElBQUksR0FBRyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUE7UUFDOUIsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFBO1FBRTlCLE9BQU8sRUFBQyxJQUFJLE1BQUEsRUFBQyxJQUFJLE1BQUEsRUFBQyxDQUFBO0lBQ3RCLENBQUM7SUFDTCwwQkFBQztBQUFELENBQUMsQUFuQ0QsSUFtQ0M7QUFuQ1ksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUkVTVUxUX0VSUk9SIH0gZnJvbSBcIi4uLy4uLy4uL2NvbmZpZy9jb25zdGFudHNcIlxyXG5pbXBvcnQgeyBTdGFja1BhcnNlciB9IGZyb20gXCIuLi9oYW5kbGVyRXJyb3JzL3N0YWNrUGFyc2VyLmhhbmRsZXJcIiBcclxuaW1wb3J0IHsgbG9nZ2VyIH0gZnJvbSBcIi4uLy4uLy4uL2hlbHBlcnMvZGVmYXVsdC5sb2dnZXJcIlxyXG5cclxuZXhwb3J0IGNsYXNzIEJhc2VFcnJvckNvbnRyb2xsZXJ7XHJcbiAgICBwcm90ZWN0ZWQgdHlwZVxyXG4gICAgcHJvdGVjdGVkIHN0YWNrXHJcbiAgICBcclxuICAgIGJ1aWxkUmVzcG9uc2UocmVzKXtcclxuICAgICAgICBsZXQgcmVzcG9uc2UgPSB7cmVzdWx0OlJFU1VMVF9FUlJPUiAsZGVzY3JpcHRpb246JycsbGluZTonJywgZmlsZTonJyB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudG9SZXNwb25zZShyZXMsIHJlc3BvbnNlKVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB0b1Jlc3BvbnNlKHJlcyxyZXNwb25zZSl7XHJcbiAgICAgICAgbGV0IG5ld1JlcyA9IHRoaXMubWFwMnJlc3BvbnNlKHJlc3BvbnNlLHJlcy5pbnZhbGlkX2ZpZWxkc1swXSlcclxuICAgICAgICBsZXQgZXJyb3IgPSB0aGlzLnBhcnNlU3RhY2soKVxyXG4gICAgICAgIG5ld1Jlcy5saW5lID0gZXJyb3IubGluZVxyXG4gICAgICAgIG5ld1Jlcy5maWxlID0gZXJyb3IuZmlsZS5zdWJzdHJpbmcoMSlcclxuICAgICAgICBsb2dnZXIuZXJyb3IoYEVycm9yVHlwZTogJHt0aGlzLnR5cGV9LCBmaWxlOiAke25ld1Jlcy5maWxlfSwgbGluZTogJHtuZXdSZXMubGluZX0gYClcclxuXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgdHlwZTogdGhpcy50eXBlLFxyXG4gICAgICAgICAgICByZXNwb25zZTpuZXdSZXNcclxuICAgICAgICB9IFxyXG4gICAgfVxyXG5cclxuICAgIHByb3RlY3RlZCBtYXAycmVzcG9uc2UocmVzcG9uc2UsbXNnKXtcclxuICAgICAgICByZXNwb25zZS5kZXNjcmlwdGlvbiA9IG1zZ1xyXG5cclxuICAgICAgICByZXR1cm4gcmVzcG9uc2VcclxuICAgIH1cclxuICAgIFxyXG4gICAgcHJpdmF0ZSBwYXJzZVN0YWNrKCkge1xyXG4gICAgICAgIGxldCBwYXJzZXIgPSBuZXcgU3RhY2tQYXJzZXIodGhpcy5zdGFjaylcclxuICAgICAgICBsZXQgbGluZSA9IHBhcnNlci5nZXRUb3BMaW5lKClcclxuICAgICAgICBsZXQgZmlsZSA9IHBhcnNlci5nZXRUb3BGaWxlKClcclxuXHJcbiAgICAgICAgcmV0dXJuIHtsaW5lLGZpbGV9XHJcbiAgICB9XHJcbn0iXX0=