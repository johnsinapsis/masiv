import { RulesBussiness } from "../../../shared/application/business/rules.bussiness";
import { ExistRouletteCondition } from "../../../roulettes/infrastructure/rules/conditions/existRoulette.condition";
import { OpenRouletteCondition } from "./conditions/openRoulette.condition";
import { BetDto } from "../../application/dto/bet.dto";

export class BetRouletteRules implements RulesBussiness{
    type: string;
    private existRoulettecondition
    private openRouletteCondition

    constructor(){
        this.type = "OpenRouletteRules"
        this.existRoulettecondition = new ExistRouletteCondition()
        this.openRouletteCondition = new OpenRouletteCondition()
    }

    public async exec(dto: BetDto) {
        await this.existRoulettecondition.exec(dto.getRouletteId())
        await this.openRouletteCondition.exec(dto.getRouletteId())
    }
}