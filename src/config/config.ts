import dotenv from 'dotenv'
dotenv.config()

const config = {
    port: process.env.node_port || 3002,
    ep:{
        createRoulette:{
            postCreateRoulette:'/roulette'
        },
        openRoulette:{
            putOpenRoulette:'/roulette/open'
        },
        bet:{
            postBet:'/roulette/bet'
        },
        closeRoulette:{
            putCloseRoulette:'/roulette/close'
        },
        getRoulettes:{
            getRoulettes:'/roulettes'
        }
    },
    db:{
        motor: process.env.db_motor+'' || 'mongodb',
        host: process.env.db_host || 'localhost',
        port: process.env.db_port || 3306,
        database: process.env.db_database || 'admin',
        user: process.env.db_user || 'root',
        pass: process.env.db_pass || ''
    },
    auth:{
        token:process.env.api_key || 'johnsinapsis',
        time: process.env.time_token || 30
    },
    logging: {
        nodeEnv: process.env.ENV || process.env.NODE_ENV,
        level: process.env.LOGGING_LVL || "error",
        logDir: process.env.LOG_DIR || 'logs',
        logFile: process.env.LOG_FILE || 'app.log'
    }
}
export default config




