import { CreateRouteController } from "../../../roulettes/infrastructure/controllers/createRoulette.controller";
import { OpenRouletteController } from "../../../roulettes/infrastructure/controllers/openRoulette.controller";
import { BetRouletteController } from "../../../bets/infrastructure/controllers/betRoulette.controller";
import { CloseRouletteController } from "../../../roulettes/infrastructure/controllers/closeRoulette.controller";
import { GetRoulettesController } from "../../../roulettes/infrastructure/controllers/getRoulettes.controller";

const CONTROLLERS = {
    CreateRouteController,
    OpenRouletteController,
    BetRouletteController,
    CloseRouletteController,
    GetRoulettesController
};

export class ControllerFactory {
    static createInstance(name) {        
        const cConstructor = CONTROLLERS[name];
        
        return cConstructor ? new cConstructor(name) : null;        
    }
}