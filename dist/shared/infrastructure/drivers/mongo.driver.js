"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongoDriver = void 0;
var mongodb_1 = require("mongodb");
var config_1 = __importDefault(require("../../../config/config"));
var default_logger_1 = require("../../../helpers/default.logger");
var MongoDriver = /** @class */ (function () {
    function MongoDriver() {
        this.db = config_1.default.db.database;
        this.engine = config_1.default.db.motor;
        this.host = config_1.default.db.host;
        this.port = config_1.default.db.port;
        this.user = config_1.default.db.user;
        this.password = config_1.default.db.pass;
        this.url = this.engine + '://' + this.user + ':' + this.password +
            '@' + this.host + ':' + this.port;
        this.client = new mongodb_1.MongoClient(this.url);
    }
    MongoDriver.prototype.connection = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.client.connect()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.client.db("admin").command({ ping: 1 })];
                    case 2:
                        _a.sent();
                        default_logger_1.logger.info("Connected successfully to Mongo server");
                        return [2 /*return*/, this.client.db(this.db)];
                    case 3:
                        e_1 = _a.sent();
                        default_logger_1.logger.error(e_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MongoDriver.prototype.disconnection = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.client.close()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return MongoDriver;
}());
exports.MongoDriver = MongoDriver;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9uZ28uZHJpdmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL3NoYXJlZC9pbmZyYXN0cnVjdHVyZS9kcml2ZXJzL21vbmdvLmRyaXZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxtQ0FBc0M7QUFDdEMsa0VBQTRDO0FBQzVDLGtFQUF5RDtBQUV6RDtJQVVJO1FBVE8sT0FBRSxHQUFHLGdCQUFNLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQTtRQUN0QixXQUFNLEdBQUcsZ0JBQU0sQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFBO1FBQ3hCLFNBQUksR0FBRyxnQkFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUE7UUFDckIsU0FBSSxHQUFHLGdCQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQTtRQUNyQixTQUFJLEdBQUcsZ0JBQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFBO1FBQ3JCLGFBQVEsR0FBRyxnQkFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUE7UUFLN0IsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFDLEtBQUssR0FBQyxJQUFJLENBQUMsSUFBSSxHQUFDLEdBQUcsR0FBQyxJQUFJLENBQUMsUUFBUTtZQUNoRCxHQUFHLEdBQUMsSUFBSSxDQUFDLElBQUksR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLElBQUksQ0FBQTtRQUNuQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUkscUJBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7SUFDM0MsQ0FBQztJQUVZLGdDQUFVLEdBQXZCOzs7Ozs7O3dCQUVRLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLEVBQUE7O3dCQUEzQixTQUEyQixDQUFBO3dCQUMzQixxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBQTs7d0JBQWxELFNBQWtELENBQUM7d0JBQ25ELHVCQUFNLENBQUMsSUFBSSxDQUFDLHdDQUF3QyxDQUFDLENBQUM7d0JBQ3RELHNCQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBQTs7O3dCQUc5Qix1QkFBTSxDQUFDLEtBQUssQ0FBQyxHQUFDLENBQUMsQ0FBQTs7Ozs7O0tBRXRCO0lBRVksbUNBQWEsR0FBMUI7Ozs7NEJBQ0kscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsRUFBQTs7d0JBQXpCLFNBQXlCLENBQUE7Ozs7O0tBQzVCO0lBQ0wsa0JBQUM7QUFBRCxDQUFDLEFBL0JELElBK0JDO0FBL0JZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTW9uZ29DbGllbnQgfSBmcm9tIFwibW9uZ29kYlwiO1xyXG5pbXBvcnQgY29uZmlnIGZyb20gXCIuLi8uLi8uLi9jb25maWcvY29uZmlnXCI7XHJcbmltcG9ydCB7IGxvZ2dlciB9IGZyb20gXCIuLi8uLi8uLi9oZWxwZXJzL2RlZmF1bHQubG9nZ2VyXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgTW9uZ29Ecml2ZXJ7XHJcbiAgICBwdWJsaWMgZGIgPSBjb25maWcuZGIuZGF0YWJhc2VcclxuICAgIHByaXZhdGUgZW5naW5lID0gY29uZmlnLmRiLm1vdG9yXHJcbiAgICBwcml2YXRlIGhvc3QgPSBjb25maWcuZGIuaG9zdFxyXG4gICAgcHJpdmF0ZSBwb3J0ID0gY29uZmlnLmRiLnBvcnRcclxuICAgIHByaXZhdGUgdXNlciA9IGNvbmZpZy5kYi51c2VyXHJcbiAgICBwcml2YXRlIHBhc3N3b3JkID0gY29uZmlnLmRiLnBhc3NcclxuICAgIHByaXZhdGUgdXJsOnN0cmluZ1xyXG4gICAgcHVibGljIGNsaWVudDpNb25nb0NsaWVudFxyXG5cclxuICAgIGNvbnN0cnVjdG9yKCl7XHJcbiAgICAgICAgdGhpcy51cmwgPSB0aGlzLmVuZ2luZSsnOi8vJyt0aGlzLnVzZXIrJzonK3RoaXMucGFzc3dvcmQrXHJcbiAgICAgICAgICAgICAgICAnQCcrdGhpcy5ob3N0Kyc6Jyt0aGlzLnBvcnRcclxuICAgICAgICB0aGlzLmNsaWVudCA9IG5ldyBNb25nb0NsaWVudCh0aGlzLnVybClcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYXN5bmMgY29ubmVjdGlvbigpe1xyXG4gICAgICAgIHRyeXtcclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5jbGllbnQuY29ubmVjdCgpXHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuY2xpZW50LmRiKFwiYWRtaW5cIikuY29tbWFuZCh7IHBpbmc6IDEgfSk7XHJcbiAgICAgICAgICAgIGxvZ2dlci5pbmZvKFwiQ29ubmVjdGVkIHN1Y2Nlc3NmdWxseSB0byBNb25nbyBzZXJ2ZXJcIik7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNsaWVudC5kYih0aGlzLmRiKVxyXG4gICAgICAgIH1cclxuICAgICAgICBjYXRjaChlKXtcclxuICAgICAgICAgICAgbG9nZ2VyLmVycm9yKGUpXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBhc3luYyBkaXNjb25uZWN0aW9uKCl7XHJcbiAgICAgICAgYXdhaXQgdGhpcy5jbGllbnQuY2xvc2UoKVxyXG4gICAgfVxyXG59XHJcbiJdfQ==