import { FieldValidator } from './field.validation' 
import { Utils } from '../../utilities' 
import jp from 'jsonpath'
import { INV_FIELDS, UNDEF } from '../../../config/constants'

export class EmptyFieldValidation extends FieldValidator {
    constructor(jpExpr, failMsg){
        super(jpExpr, failMsg)
        this.jpExpr = jpExpr
        this.failMsg = failMsg
    }

    isValid(req, res){
        let field = jp.query(req.body, this.jpExpr)[0]
        return !Utils.isEmpty( field )
    }

    addErrorResponse( res, msg ){        
        if( typeof res[INV_FIELDS] === UNDEF ){
            res.invalid_fields = []
        }
        res.invalid_fields.push(msg)
    }
}