import { RouletteDto } from "../dto/roulette.dto";

export interface StatusRouletteRepository{
    setOpenStatusRoulette(id: any): Promise <boolean>;
    setCloseStatusRoulette(id: any): Promise <boolean>;
}