"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
var config = {
    port: process.env.node_port || 3002,
    ep: {
        createRoulette: {
            postCreateRoulette: '/roulette'
        },
        openRoulette: {
            putOpenRoulette: '/roulette/open'
        },
        bet: {
            postBet: '/roulette/bet'
        },
        closeRoulette: {
            putCloseRoulette: '/roulette/close'
        },
        getRoulettes: {
            getRoulettes: '/roulettes'
        }
    },
    db: {
        motor: process.env.db_motor + '' || 'mongodb',
        host: process.env.db_host || 'localhost',
        port: process.env.db_port || 3306,
        database: process.env.db_database || 'admin',
        user: process.env.db_user || 'root',
        pass: process.env.db_pass || ''
    },
    auth: {
        token: process.env.api_key || 'johnsinapsis',
        time: process.env.time_token || 30
    },
    logging: {
        nodeEnv: process.env.ENV || process.env.NODE_ENV,
        level: process.env.LOGGING_LVL || "error",
        logDir: process.env.LOG_DIR || 'logs',
        logFile: process.env.LOG_FILE || 'app.log'
    }
};
exports.default = config;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbmZpZy9jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxrREFBMkI7QUFDM0IsZ0JBQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQTtBQUVmLElBQU0sTUFBTSxHQUFHO0lBQ1gsSUFBSSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxJQUFJLElBQUk7SUFDbkMsRUFBRSxFQUFDO1FBQ0MsY0FBYyxFQUFDO1lBQ1gsa0JBQWtCLEVBQUMsV0FBVztTQUNqQztRQUNELFlBQVksRUFBQztZQUNULGVBQWUsRUFBQyxnQkFBZ0I7U0FDbkM7UUFDRCxHQUFHLEVBQUM7WUFDQSxPQUFPLEVBQUMsZUFBZTtTQUMxQjtRQUNELGFBQWEsRUFBQztZQUNWLGdCQUFnQixFQUFDLGlCQUFpQjtTQUNyQztRQUNELFlBQVksRUFBQztZQUNULFlBQVksRUFBQyxZQUFZO1NBQzVCO0tBQ0o7SUFDRCxFQUFFLEVBQUM7UUFDQyxLQUFLLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUMsRUFBRSxJQUFJLFNBQVM7UUFDM0MsSUFBSSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxJQUFJLFdBQVc7UUFDeEMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxJQUFJLElBQUk7UUFDakMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxJQUFJLE9BQU87UUFDNUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxJQUFJLE1BQU07UUFDbkMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxJQUFJLEVBQUU7S0FDbEM7SUFDRCxJQUFJLEVBQUM7UUFDRCxLQUFLLEVBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLElBQUksY0FBYztRQUMzQyxJQUFJLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLElBQUksRUFBRTtLQUNyQztJQUNELE9BQU8sRUFBRTtRQUNMLE9BQU8sRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVE7UUFDaEQsS0FBSyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxJQUFJLE9BQU87UUFDekMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxJQUFJLE1BQU07UUFDckMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxJQUFJLFNBQVM7S0FDN0M7Q0FDSixDQUFBO0FBQ0Qsa0JBQWUsTUFBTSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGRvdGVudiBmcm9tICdkb3RlbnYnXHJcbmRvdGVudi5jb25maWcoKVxyXG5cclxuY29uc3QgY29uZmlnID0ge1xyXG4gICAgcG9ydDogcHJvY2Vzcy5lbnYubm9kZV9wb3J0IHx8IDMwMDIsXHJcbiAgICBlcDp7XHJcbiAgICAgICAgY3JlYXRlUm91bGV0dGU6e1xyXG4gICAgICAgICAgICBwb3N0Q3JlYXRlUm91bGV0dGU6Jy9yb3VsZXR0ZSdcclxuICAgICAgICB9LFxyXG4gICAgICAgIG9wZW5Sb3VsZXR0ZTp7XHJcbiAgICAgICAgICAgIHB1dE9wZW5Sb3VsZXR0ZTonL3JvdWxldHRlL29wZW4nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBiZXQ6e1xyXG4gICAgICAgICAgICBwb3N0QmV0Oicvcm91bGV0dGUvYmV0J1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2xvc2VSb3VsZXR0ZTp7XHJcbiAgICAgICAgICAgIHB1dENsb3NlUm91bGV0dGU6Jy9yb3VsZXR0ZS9jbG9zZSdcclxuICAgICAgICB9LFxyXG4gICAgICAgIGdldFJvdWxldHRlczp7XHJcbiAgICAgICAgICAgIGdldFJvdWxldHRlczonL3JvdWxldHRlcydcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGI6e1xyXG4gICAgICAgIG1vdG9yOiBwcm9jZXNzLmVudi5kYl9tb3RvcisnJyB8fCAnbW9uZ29kYicsXHJcbiAgICAgICAgaG9zdDogcHJvY2Vzcy5lbnYuZGJfaG9zdCB8fCAnbG9jYWxob3N0JyxcclxuICAgICAgICBwb3J0OiBwcm9jZXNzLmVudi5kYl9wb3J0IHx8IDMzMDYsXHJcbiAgICAgICAgZGF0YWJhc2U6IHByb2Nlc3MuZW52LmRiX2RhdGFiYXNlIHx8ICdhZG1pbicsXHJcbiAgICAgICAgdXNlcjogcHJvY2Vzcy5lbnYuZGJfdXNlciB8fCAncm9vdCcsXHJcbiAgICAgICAgcGFzczogcHJvY2Vzcy5lbnYuZGJfcGFzcyB8fCAnJ1xyXG4gICAgfSxcclxuICAgIGF1dGg6e1xyXG4gICAgICAgIHRva2VuOnByb2Nlc3MuZW52LmFwaV9rZXkgfHwgJ2pvaG5zaW5hcHNpcycsXHJcbiAgICAgICAgdGltZTogcHJvY2Vzcy5lbnYudGltZV90b2tlbiB8fCAzMFxyXG4gICAgfSxcclxuICAgIGxvZ2dpbmc6IHtcclxuICAgICAgICBub2RlRW52OiBwcm9jZXNzLmVudi5FTlYgfHwgcHJvY2Vzcy5lbnYuTk9ERV9FTlYsXHJcbiAgICAgICAgbGV2ZWw6IHByb2Nlc3MuZW52LkxPR0dJTkdfTFZMIHx8IFwiZXJyb3JcIixcclxuICAgICAgICBsb2dEaXI6IHByb2Nlc3MuZW52LkxPR19ESVIgfHwgJ2xvZ3MnLFxyXG4gICAgICAgIGxvZ0ZpbGU6IHByb2Nlc3MuZW52LkxPR19GSUxFIHx8ICdhcHAubG9nJ1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IGNvbmZpZ1xyXG5cclxuXHJcblxyXG5cclxuIl19