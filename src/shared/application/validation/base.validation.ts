import { UNDEF,INV_FIELDS } from "../../../config/constants";
import { FieldValidationFactory } from "./validation.factory";

export class BaseValidation{
    protected fieldValidators
    protected fieldValidationFactory

    constructor(){
        this.fieldValidators = []
        this.fieldValidationFactory = FieldValidationFactory
    }

    isValid(req, res){  
        for( let validator of this.fieldValidators ){   
            let isValid = validator.evaluate(req, res)
            if(!isValid ){
                return false
            }
        }
        if( typeof res[INV_FIELDS] != UNDEF && res[INV_FIELDS].length > 0 ){
            return false
        } 
        return true
    }

    addValidator(validator){
        this.fieldValidators.push(validator)
    }
}